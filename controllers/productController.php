<?php

/**
 * Class productController | controllers/productController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * PRODUCT - handles requests to retrieve product info.
 *  
 */
class productController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_model($pk_car, $pk_lng) {
        $this->_view->recordSet['pk_car'] = $pk_car;
        $data = $this->loadModel('model');
        $model_results = $data->getname($pk_car, $pk_lng);
        return $this->_view->recordSet['car_name'] = $model_results['car_name'];
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    public function product($argumentos) {
        $pk_car = $argumentos[0];
        $pk_ctg = $argumentos[1];
        $pk_prd = $argumentos[2];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $this->c_model($pk_car, $pk_lng);

        $objprd= $this->loadmodel('product');
        $prd_info= $objprd->prd_info($pk_car,$pk_lng,$pk_prd);
        $obj= $this->loadmodel('category');
        $bc_= $obj->bc_builder($pk_car,$pk_ctg,$pk_lng);
        $bc= $obj->bc_assigntype($pk_car,$bc_);
        $objdesp= $this->loadModel('despiece');
        $despiece= $objdesp->getDespieces($pk_prd,$pk_car,$pk_lng);
        $this->_view->recordSet['sidebar_ctgs']= $obj->get_mainctgs($pk_car,$pk_lng);
        $this->_view->recordSet['category_data']['bc_elements']= $bc;
        $this->_view->recordSet['prd_despiece']= $despiece;
        $this->_view->recordSet['prd_info']= $prd_info;
        //reasign 5 to call 'getCardRelations' procedure in DDBB
        if ($pk_lng == null) { $pk_lng = 5; }
        $prd_cross = $objprd->getCrossselling($pk_car, $pk_prd, $pk_lng);
        $cross = $objprd->cross_urlBuilder($prd_cross);
        $this->_view->recordSet['cross_selling'] = $cross;
        $this->_view->render(HEADER, 'product', FOOTER);
    }

}
