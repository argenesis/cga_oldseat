<?php

/**
 * Class mapController | controllers/mapController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * MAP - handles requests(mostly AJAX on "public/js/map_output.js")from DDBB to fetch locations.
 * 
 * @todo [mapsearch()] Make the AJAX send the QUERY TYPE and the parameters.
 * 
 */
class mapController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    /** To load GoogleMaps.
     * @version ? */
    public function map() {
        $this->_view->recordSet['map'] = 'true';
        $this->_view->render(MAP_HEADER, 'map', FOOTER);
    }

    /** To load SEAT iframe for map.
     * @version 1.0 */
    public function mapa() {
        $this->_view->recordSet['map'] = 'true';
        $this->_view->render(MAP_HEADER, 'mapa', FOOTER);
    }

    /** To search locations under type of search and parameters provided.
     * @version 2.0 */
    public function mapsearch() {
        $dcity = filter_input(INPUT_POST, 'city');
        $dcity_lat = filter_input(INPUT_POST, 'city_lat');
        $dcity_lng = filter_input(INPUT_POST, 'city_lng');
        $dradius = filter_input(INPUT_POST, 'radius');

        header("Content-type: application/json; charset=utf-8");
        $obj = $this->loadModel('map');
        $res = $obj->searchby_radius($dradius, $dcity_lat, $dcity_lng);
        echo json_encode($res);
    }

}
