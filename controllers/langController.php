<?php

/**
 * Class langController | controllers/langController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * LANG - handles/set internal state for queries(through $_SESSION and 
 * "public/js/commonviews.js") and language related values/tags.
 * 
 * @todo Build an abstraction layer to separate LANG from COUNTRY and MARKET.
 * @revision Argenis Urribarri<argenis@snconsultors.es>
 * 
 */
class langController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /** Authorized role of user that can access this controller */
    protected function set_autRole() {
        $this->_autRole = 'USER';
    }

    public function get_autRole() {
        return $this->_autRole;
    }

    public function index() {
        
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    /** Sets the new lang in _SESSION through AJAX - commonviews.js. */
    public function changelang() {
        $new_lng = filter_input(INPUT_POST, 'newlang');
        $_SESSION['user_lang'] = $new_lng;
    }
}
