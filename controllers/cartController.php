<?php
require_once INCLUDES_REPO.'wishlist/recaptcha-master/src/autoload.php';

/**
 * Class cartController | controllers/cartController.php
 *
 * @package     controllers
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * CART - handles interactions with items for/on the user cart.
 * 
 * @uses app/Cart
 * @property-read Cart $_iCart Property from this controller, which builds a singleton of a cart/wishlist. 
 * @property-write Cart $_iCart Property from this controller, which builds a singleton of a cart/wishlist.
 * @revision Argenis Urribarri<argenis@snconsultors.es>
 *  
 */
class cartController extends Controller {

    protected $_iCart;

    public function __construct() {
        parent::__construct();
        $this->_iCart = Cart::singleton();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_model($pk_car, $pk_lng) {
        $this->_view->recordSet['pk_car'] = $pk_car;
        $data = $this->loadModel('model');
        $model_results = $data->getname($pk_car, $pk_lng);
        return $this->_view->recordSet['car_name'] = $model_results['car_name'];
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    public function cart() {
        $this->_view->recordSet['iCart'] = $this->_iCart->getItems();
        $this->_view->render(SIMPLE_HEADER, 'cart', FOOTER);
    }

    public function add() {
        $prd = $_POST['prd_data']; //passing array through POST
        $this->_iCart->addItem($prd);
    }

    public function emptycart() {
        $this->_iCart->deleteAllItem();
    }

    public function delete() {
        $uid = filter_input(INPUT_POST, 'uid');
        $this->_iCart->deleteItem($uid);
    }

    public function update() {
        $updatedata = $_POST['updatedata'];
        $this->_iCart->updateQuantity($updatedata);
    }
    
    //--------------------------------------------//
    /** Verificacion de uso de reCAPTCHA
     * 
     * @author Ricard Beyloc. */
    public function testRecaptcha() { 
        // require ReCaptcha class        
        $recaptchaSecret = constant('recaptchaSecret');
        $type = 'danger';
        try {
            $errorMessage = constant('cform_POSTerror');
            if (!empty($_POST)) {
                $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret, new \ReCaptcha\RequestMethod\CurlPost());
                $response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
                if (!$response->isSuccess()) {
                    $errorMessage = constant('cform_CAPTCHAerror');
                    throw new \Exception('ReCaptcha was not validated.');
                } else {
                    if($this->sendMailManager()){ //1: true(enviados)
                       
                    $errorMessage = constant('cform_SUCCESS');                   
                        $type = 'success';
                        throw new \Exception('ReCaptcha ok.');
                    }
                }
            }
        } catch (\Exception $e) {
            $responseArray = array('type' => $type, 'message' => $errorMessage);
        }        
        
        //si hemos recibido una petición desde Ajax 'enviamos' la respuesta al Ajax codificado en JSON y sino simplemente el message del array de respuesta
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $encoded = json_encode($responseArray);
            header('Content-Type: application/json');
            echo $encoded;
        } else {
            echo json_encode($responseArray);
        }
    }
    
    /** Gestor de envio de correos. */
    private function sendMailManager() {
        $host_mail= constant('mailORIGIN');//Config.php
        $conc_email= 'ricard@snconsultors.es';//filter_input(INPUT_POST, 'mail_conc');
        $user_email= filter_input(INPUT_POST, 'email');
        $formValues= array('dealer_name' => filter_input(INPUT_POST, 'nombre_conc'),
            'dealer_email' => $conc_email,
            'dealer_adress' => filter_input(INPUT_POST, 'direcc_conc'),
            'dealer_phone' => null,
            'user_name' => filter_input(INPUT_POST, 'name'),
            'user_surname' => filter_input(INPUT_POST, 'surname'),
            'user_email' => $user_email,
            'user_phone' => filter_input(INPUT_POST, 'phone'),
            'user_message' => filter_input(INPUT_POST, 'message'));
        
        $iMail = $this->loadModel('mailing');
        $wlContent= $this->_iCart->getItems();
        
        if($iMail->toConcessionaire_mail($host_mail,$conc_email,constant('mailCC'),constant('mailBLIND'),$formValues,$wlContent)) {
            return $iMail->toClient_mail($host_mail,$user_email,constant('mailCC'),constant('mailBLIND'),$formValues,$wlContent);
        }
    }
}