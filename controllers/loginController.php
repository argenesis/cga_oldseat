<?php

/**
 * Class loginController | controllers/loginController.php | [ignored]
 *
 * @package     controllers
 * @author      SNCONSULTORS
 * @version     ?
 */

/**
 * LOGIN - Description goes here.
 * 
 * @todo Mirror this feature from OWA when requested by SEAT.
 * @revision Argenis Urribarri<argenis@snconsultors.es>
 * 
 */
class loginController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        $this->_view->render('index');
    }

    public function logout() {   //logout redirects to the 'login'-index
        Login::singleton()->logout();
        $this->_view->render('logout');
    }

    public function valid() {   //validUser SENDS to 'valid' view, which sends to 'home';invalidUser SENDS to 'login' 
        $values = array();
        $user = $this->loadModel('user');
        $login_user = filter_input(INPUT_POST, 'login_user');
        $login_password = filter_input(INPUT_POST, 'login_password');

        if ($obj->isValid($login_user, $login_password)) {
            /* $obj->getUserData($user, $pass, &$data);
              $this->_view->render('valid'); */
            //$current_login = Login::singleton();// -> catalogue-dev
            $current_login->assign($values);
            $this->_view->render('valid');
        } else {
            $this->_view->redirect('login');
        }
    }

}
