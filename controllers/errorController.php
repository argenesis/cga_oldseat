<?php

/**
 * Class errorController | controllers/errorController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * ERROR - builds a view to output the corresponding error message.
 * 
 * Display message content and output; 'err_msg' serves as a tag(see config_xx.php)
 * @param String View::$recordSet['error_log'] The tag and message to feed the cosntructor.
 *  
 */
class errorController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    public function error() {
        $msg = filter_input(INPUT_POST, 'err_msg');
        $this->_view->recordSet['error_log'] = $msg;
        $this->_view->render(HEADER, 'error', FOOTER);
    }

}
