<?php

/**
 * Class modelController | controllers/modelController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * MODEL - handles requests in single VEHICLE MODEL info.
 *  
 */
class modelController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    private function c_model($pk_car, $pk_lng) {
        $this->_view->recordSet['pk_car'] = $pk_car;
        $data = $this->loadModel('model');
        $model_results = $data->getname($pk_car, $pk_lng);
        return $this->_view->recordSet['car_name'] = $model_results['car_name'];
    }

    private function check_model($pk_car, $pk_lng) {
        $this->_view->recordSet['pk_car'] = $pk_car;
        $data = $this->loadModel('model');
        $model_results = $data->getname($pk_car, $pk_lng);
        return $model_results['car_name'];
    }
    
    /** Enable/disable the parameter; it maps main collections that 
     * must implement footer_video and enable videoModal.
     * 
     * @revision 17-10-2017 The Collection with id=100005 has to show a video.
     * @param int $pk_coll The ID of the MAIN collection. */
    private function has_video($pk_car) {
        $footer = FOOTER;
        switch ($pk_car) {
            case 45: //Arona
                $this->_view->recordSet['has_video'] = true;
                $this->_view->recordSet['video_name'] = 'SEAT_Arona_Video.mp4';
                $this->_view->recordSet['video_text'] = 'SEAT Arona Video';
                $this->_view->recordSet['video_margin_top'] = '330px';
                $this->_view->recordSet['video_margin_right'] = '160px';
                $this->_view->recordSet['video_text_color'] = 'seat-white';
                $footer = FOOTER_VIDEO;
                break;
            case 44: //Nuevo Ibiza
                $this->_view->recordSet['has_video'] = true;
                $this->_view->recordSet['video_name'] = 'SEAT_Ibiza_Video.mp4';
                $this->_view->recordSet['video_text'] = 'SEAT Ibiza Video';
                $this->_view->recordSet['video_margin_top'] = '20px';
                $this->_view->recordSet['video_margin_right'] = '50px';
                $this->_view->recordSet['video_text_color'] = 'seat-black-text';
                $footer = FOOTER_VIDEO;
                break;
            case 40: //Ateca
                $this->_view->recordSet['has_video'] = true;
                $this->_view->recordSet['video_name'] = 'SEAT_Ateca_Video.mp4';
                $this->_view->recordSet['video_text'] = 'SEAT Ateca Video';
                $this->_view->recordSet['video_margin_top'] = '340px';
                $this->_view->recordSet['video_margin_right'] = '55px';
                $this->_view->recordSet['video_text_color'] = 'seat-white';
                $footer = FOOTER_VIDEO;
                break;
        }
        return $footer;
    }

    /** Single VEHICLE MODEL rendering; info on common main categories */
    public function model($parameters) {
        $pk_car = $parameters[0];
        $pk_ctg = NULL; //main ctg
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $this->c_model($pk_car, $pk_lng);

        $obj = $this->loadModel('category');
        $this->_view->recordSet['sidebar_ctgs'] = $obj->get_mainctgs($pk_car, $pk_lng);
        $img_beauty = $obj->get_imgbeauty($pk_car, $pk_ctg);
        $this->_view->recordSet['category_data']['img_beauty'] = $img_beauty;
        $this->_view->recordSet['category_data']['bc_elements'] = $pk_car; //top level(=1)
        $footer = $this->has_video($pk_car);
        $this->_view->render(HEADER, 'model', $footer);
    }

    /** Validates if the VEHICLE MODEL exists in target market/language(AJAX related)->commonviews.js */
    public function checkurl_onmodelchange() {
        $pk_car = filter_input(INPUT_POST, 'model_pkcar');
        //example:26,27(doesn't exists in MEXICO)
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $res = $this->check_model($pk_car, $pk_lng);
        $valid = 'false';
        //Check if model name exists for this mrkt...
        if ($res != null) {
            $valid = 'true';
        }
        print $valid;
    }

}
