<?php

/**
 * Class collectionController | controllers/collectionController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * COLLECTION - handles request that call to a collection(category-like).
 * 
 * The selection implies:(1)Selecting a COLLECTION;(2)Selecting a COLLECTION CATEGORY.
 * It also performs a SHORTCUT functionality. For example: a category having 
 * only one product must go to the product view; a category having one 
 * subcategory that is parent to another category must go straight to the last one...
 * 
 * @property-read Model $main_model Set as a property to re-use "collection-data" method and avoid
 * duplicity in the code and instanciating the model again.
 * @todo The shortcuts applying through collections and collection categories need to be
 * chunked into smaller pieces of code [Recipe-like coding](see "categoryController.php").
 *  
 */
class collectionController extends Controller {

    private $main_model; //to re-use "collection_data" method        

    private function set_mM($mModel) { //set main-model
        $this->main_model = $mModel;
    }

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Loops through child categories (size=1) and updates the value for the current "working" category. */
    private function sc_loop($haschild, &$pk_ctg) {   //Shortcut through ctg having only 1 child
        if (sizeof($haschild->rows) == 1) {
            $shortcut_toctg = true;
        }
        $curr_ctg = $pk_ctg;
        while (!$shortcut_toctg == false) {
            $_haschild = $this->main_model->collection_haschild($curr_ctg);
            if ((sizeof($_haschild->rows) > 1) || ($_haschild->rows[0]['haschild'] == null)) {
                $pk_ctg = $curr_ctg; //dead point reached...
                $shortcut_toctg = false;
            } else {
                $curr_ctg = $_haschild->rows[0]['haschild']; //assignment size=1
            }
        }//end of shortcut
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Validates the type [list/products/category]. */
    private function sc_checkType(&$items, $list, $ctg_prds, $pk_ctg) { // isList/ctg_prds
        if ($list['is_list'] === 'true') {
            $ctg_list = $this->main_model->assign_type($ctg_prds);
            $items['list_img'] = $list['img'];
            $items['list_name'] = $list['name'];
            $items['ctg_list'] = $ctg_list;
            header('Location: ' . BASE_URL . "collection/collctg_list/$pk_ctg");
            exit;
        } else {
            //Shortcut through ctg_final having only 1 prd
            if (sizeof($ctg_prds) == 1) {
                $curr_ctg = $ctg_prds[0]['pk_ctgfinal'];
                $curr_prd = $ctg_prds[0]['pk_prd'];
                header('Location: ' . BASE_URL . "collectionprd/collectionprd/$curr_ctg/$curr_prd");
                exit;
            } elseif (sizeof($ctg_prds) > 1) {
                $ctgprds = $this->main_model->assign_type($ctg_prds);
                $items['ctg_prds'] = $ctgprds;
                header('Location: ' . BASE_URL . "collection/collctg_prds/$pk_ctg");
                exit;
            }
        }
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Validates the type [list]. */
    private function sc_isList(&$items, $list, $ctg_prds) {   //Check if isList
        $ctg_list = $this->main_model->assign_type($ctg_prds);
        $items['list_img'] = $list['img'];
        $items['list_name'] = $list['name'];
        $items['ctg_list'] = $ctg_list;
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Goes straight to the product if (size=1). */
    private function sc_only1Prd(&$items, $ctg_prds) {   //Shortcut through ctg_final having only 1 prd
        if (sizeof($ctg_prds) == 1) {
            $curr_ctg = $ctg_prds[0]['pk_ctgfinal'];
            $curr_prd = $ctg_prds[0]['pk_prd'];
            header('Location: ' . BASE_URL . "collectionprd/collectionprd/$curr_ctg/$curr_prd");
            exit;
        }
        $ctgprds = $this->main_model->assign_type($ctg_prds);
        $items['ctg_prds'] = $ctgprds;
    }

    /** MAIN TRIGGER/SELECTOR FOR 'sc_xxx' functions. */
    private function collection_data($pk_ctg, $pk_lng) {
        $items = array();
        $haschild = $this->main_model->collection_haschild($pk_ctg);
        $this->sc_loop($haschild, $pk_ctg);
        //***Get true/false for ctg_islist || check if ctg has products
        $list = $this->main_model->is_list($pk_ctg, $pk_lng);
        $ctg_prds = $this->main_model->get_ctg_prds($pk_ctg, $pk_lng);
        //***Cases for URL build
        if (($ctg_prds) == null) {
            $manager = $this->main_model->coll_manager($pk_lng, $pk_ctg);
            $items['coll_relations'] = $manager;
        } elseif (sizeof($haschild->rows) == 1) {
            $this->sc_checkType($items, $list, $ctg_prds, $pk_ctg);
        } else {
            if ($list['is_list'] === 'true') {
                $this->sc_isList($items, $list, $ctg_prds);
            } else {
                $this->sc_only1Prd($items, $ctg_prds);
            }
        } // - Default items
        $bc = $this->main_model->bc_builder($pk_ctg, $pk_lng);
        $items['bc_elements'] = $bc;
        $items['jumbo'] = $this->main_model->jumbo($bc[0]['bc_ctg']);
        return $items;
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    /** Enable/disable the parameter; it maps main collections that 
     * must implement footer_video and enable videoModal.
     * 
     * @revision 17-10-2017 The Collection with id=100005 has to show a video.
     * @param int $pk_coll The ID of the MAIN collection. */
    private function has_video($pk_coll) {
        $footer = FOOTER;
        switch ($pk_coll) {
            case 100005: //Mediterranean
                $this->_view->recordSet['has_video'] = true;
                $this->_view->recordSet['video_name'] = 'MASTER MAKING OF_COLLECTION SEAT.mp4';
                $this->_view->recordSet['video_text'] = constant('vidmodal_TITLE');
                $this->_view->recordSet['video_margin_top'] = '-25px';
                $this->_view->recordSet['video_margin_right'] = '-100px';
                $this->_view->recordSet['video_text_color'] = 'seat-black-text';
                $footer = FOOTER_VIDEO;
                break;
            case 100001 || 100002 || 100003 || 100004: //Mediterranean
                $this->_view->recordSet['has_video'] = true;
                $this->_view->recordSet['video_name'] = 'SEAT_Collection_Video.mp4';
                $this->_view->recordSet['video_text'] = 'SEAT Collection Video';
                $this->_view->recordSet['video_margin_top'] = '260px';
                $this->_view->recordSet['video_margin_right'] = '-75px';
                $this->_view->recordSet['video_text_color'] = 'seat-white';
                $footer = FOOTER_VIDEO;
                break;
        }
        return $footer;
    }

    /** Is the equivalent to 'Main Categories' for Accessories. */
    public function collectionselector() {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);

        $obj = $this->loadModel('collection');
        $this->set_mM($obj);
        $main_collections = $this->main_model->collection_selector($pk_lng);
        $this->_view->recordSet['coll_selector'] = $main_collections;
        $this->_view->render(HEADER, 'collection_selector', FOOTER);
    }

    /** A TRANSITIONAL collection-category containing collection-categories. */
    public function collctg($argumentos) {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $pk_coll = $argumentos[0]; //pk_coll=pk_ctg

        $obj = $this->loadModel('collection');
        $this->set_mM($obj);
        $coll_selected = $this->collection_data($pk_coll, $pk_lng);
        $this->_view->recordSet['coll_data'] = $coll_selected;
        $main_coll = $coll_selected['bc_elements'][0]['bc_ctg'];
        $footer = $this->has_video($main_coll);
        $this->_view->render(HEADER, 'collctg', $footer);
    }

    /** A collection-category containing a LIST OF PRODUCTS. */
    public function collctg_list($argumentos) {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $pk_coll = $argumentos[0]; //pk_coll=pk_ctg

        $obj = $this->loadModel('collection');
        $this->set_mM($obj);
        $coll_selected = $this->collection_data($pk_coll, $pk_lng);
        $this->_view->recordSet['coll_data'] = $coll_selected;
        $sb_prntctg = $coll_selected['bc_elements'][0]['bc_ctg'];
        $footer = $this->has_video($sb_prntctg);
        $sidebar = $this->main_model->sidebar_manager($pk_lng, $sb_prntctg);
        $this->_view->recordSet['sidebar_ctgs'] = $sidebar;
        $this->_view->render(HEADER, 'collctg_list', $footer);
    }

    /** A collection-category containing PRODUCTS. */
    public function collctg_prds($argumentos) {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $pk_coll = $argumentos[0]; //pk_coll=pk_ctg        

        $obj = $this->loadModel('collection');
        $this->set_mM($obj);
        $coll_selected = $this->collection_data($pk_coll, $pk_lng);
        $main_coll = $coll_selected['bc_elements'][0]['bc_ctg'];
        $footer = $this->has_video($main_coll);
        $this->_view->recordSet['coll_data'] = $coll_selected;
        $this->_view->render(HEADER, 'collctg_prds', $footer);
    }

}
