<?php

/**
 * Class indexController | controllers/indexController.php
 *
 * @package     controllers
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * INDEX - handles request performed in any variation of "index.php".
 * 
 * @todo Define behaviour if "login" is enabled for the project.
 * @revision Argenis Urribarri<argenis@snconsultors.es>
 * 
 */
class indexController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /** Authorized role of user that can access this controller */
    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        $obj = $this->loadModel('index');
        $this->_view->recordSet['hc_indexdata'] = $obj->get_hc_indexdata();
        $this->_view->render(HEADER, 'index', FOOTER);
    }

}
