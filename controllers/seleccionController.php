<?php

/**
 * Class seleccionController | controllers/seleccionController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * SELECCION - handles user's navigation requests through the main page for COLLECTION/ACCESORIES.
 *  
 */
class seleccionController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'USER';
    }

    public function index() {
        
    }

    public function seleccion() {
        $obj_data = $this->loadModel('seleccion');
        $currentmodels = $obj_data->get_hc_currentmodels();
        $this->_view->recordSet['currentmodels'] = $currentmodels;
        $this->_view->render(HEADER, 'seleccion', FOOTER);
    }

    public function anteriores() {
        $obj_data = $this->loadModel('seleccion');
        $othermodels = $obj_data->get_othermodels();
        $this->_view->recordSet['othermodels'] = $othermodels;
        $this->_view->render(HEADER, 'anteriores', FOOTER);
    }

}
