<?php

/**
 * Class searchresultsController | controllers/searchresultsController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * SEARCHRESULTS - handles requests to search for a product(accessories/collection).
 *  
 */
class searchresultsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    /** Performs the actual search.
     * This is also called from those products on "cross-selling"; that is
     * why there's a checkout on where are the parameters comming (url/post).
     * This controller calls to a method on the model fetching pk_prd from a SKU
     * and then triggers necessary actions.
     * 
     * @param string $prd_sku The Stock-keeping unit value; it has a corresponding ID on ddbb.
     * @param int $pk_lng Necessary to switch on markets(see the Model for further info). */
    public function searchresults($parameters) {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        if ($parameters[0] == null) {
            $prd_sku = filter_input(INPUT_POST, 'prd_sku');
        } else {
            $prd_sku = $parameters[0];
        }
        $items = array(); // Elements
        $obj = $this->loadModel('searchresults');
        $fetched = $obj->fetch_pk($prd_sku, $pk_lng);
        $prd_pk = $fetched[0]['pk_prd'];
        $type = $fetched[0]['type'];

        if ($prd_pk != null) {// SKU=>PK EXISTS for catalogue OR collection...
            switch ($type) {
                case 'cat': // catalogue
                    $prd = $obj->is_unassigned($prd_pk);
                    if ($prd['is_unassigned'] != null) {
                        $items['unassigned'] = $sku;
                    } else {
                        $cm = $obj->getfor_currentmodels($prd_pk);
                        $items['currentmodels'] = $cm;
                        $om = $obj->getfor_othermodels($prd_pk);
                        $items['othermodels'] = $om;
                    }
                    break;
                case 'coll':// collection
                    $coll = $obj->is_collunassigned($prd_pk);
                    if ($coll['is_unassigned'] != null) {
                        $items['unassigned'] = $sku;
                    } else {
                        $collm = $obj->getfor_collection($prd_pk);
                        $items['collection'] = $collm;
                    }
                    break;
            }
        } // SKU doesn't exists for catalogue NOR for collection
        else {
            $items = null;
        }
        $this->_view->recordSet['searchresults'] = $items;
        $this->_view->recordSet['prd_sku'] = $prd_sku;
        $this->_view->render(HEADER, 'searchresults', FOOTER);
    }
    
    public function autocomplete($parameters) {
        $term = $parameters[0];
        $obj = $this->loadModel('searchresults');
        $result = $obj->autocomplete_search($term);
        echo json_encode($result);
    }

}
