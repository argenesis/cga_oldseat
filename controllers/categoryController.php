<?php

/**
 * Class categoryController | controllers/categoryController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * CATEGORY - handles requests to get a category(accessories).
 * 
 * The structure of categories differs from model to model; the common categories are
 * the level=1/main categories, so the code first asks for the CHILD category/categories.
 * The main categories are set as NULL because they have no parents.
 * This class also performs a SHORTCUT functionality (prefix 'sc_'). For example: 
 * a category having only one product must go to the product view; a category having one 
 * subcategory that is parent to another category must go straight to the last one...
 * 
 * @property-read Model $main_model Set as a property to re-use "category-data" method and avoid
 * duplicity in the code and instanciating the model again.
 *      
 */
class categoryController extends Controller {

    private $main_model;

    private function set_mM($mModel) {
        $this->main_model = $mModel;
    }

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function get_autRole() {
        return $this->_autRole;
    }

    public function index() {
        
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Loops through child categories (size=1) and updates the value for the current "working" category. */
    private function sc_loop($haschild, $pk_car, &$pk_ctg) {   //Shortcut through ctg having only 1 child
        if (sizeof($haschild->rows) == 1) {
            $shortcut_toctg = true;
        }
        $curr_ctg = $pk_ctg;
        while ((!$shortcut_toctg == false)) {
            $_haschild = $this->main_model->haschildlevel($pk_car, $curr_ctg);
            if ((sizeof($_haschild->rows) > 1) || (($_haschild->rows == null))) {
                $pk_ctg = $curr_ctg; //dead point reached...
                $shortcut_toctg = false;
            } else {
                $curr_ctg = $_haschild->rows[0]['hrc_child']; //assignment when size=1
            }
        }//end of shortcut
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Validates the type [list/products/category]. */
    private function sc_checkType(&$items, $list, $pk_car, $ctg_prds, $pk_ctg) { // isList/ctg_prds
        if ($list['is_list'] === 'true') {
            $ctg_list = $this->main_model->assign_type($pk_car, $ctg_prds->rows);
            $items['list_img'] = $list['img'];
            $items['list_name'] = $list['name'];
            $items['ctg_list'] = $ctg_list;
            header('Location: ' . BASE_URL . "category/ctg_list/$pk_car/$pk_ctg");
            exit;
        } else {
            //Shortcut through ctg_final having only 1 prd
            if (sizeof($ctg_prds->rows) == 1) {
                $curr_ctg = $ctg_prds->rows[0]['pk_ctgfinal'];
                $curr_prd = $ctg_prds->rows[0]['pk_prd'];
                header('Location: ' . BASE_URL . "product/product/$pk_car/$curr_ctg/$curr_prd");
                exit;
            } elseif (sizeof($ctg_prds->rows) > 1) {
                $ctgprds = $this->main_model->assign_type($pk_car, $ctg_prds->rows);
                $items['ctg_prds'] = $ctgprds;
                header('Location: ' . BASE_URL . "category/ctg_prds/$pk_car/$pk_ctg");
                exit;
            }
        }
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Validates the type [list]. */
    private function sc_isList(&$items, $list, $pk_car, $ctg_prds) {   //Check if isList
        $ctg_list = $this->main_model->assign_type($pk_car, $ctg_prds->rows);
        $items['list_img'] = $list['img'];
        $items['list_name'] = $list['name'];
        $items['ctg_list'] = $ctg_list;
    }

    /** PREFIX "sc_": shortcut related functions. 
     * Goes straight to the product if (size=1). */
    private function sc_only1Prd(&$items, $ctg_prds, $pk_car) {   //Shortcut through ctg_final having only 1 prd
        if (sizeof($ctg_prds->rows) == 1) {
            $curr_ctg = $ctg_prds->rows[0]['pk_ctgfinal'];
            $curr_prd = $ctg_prds->rows[0]['pk_prd'];
            header('Location: ' . BASE_URL . "product/product/$pk_car/$curr_ctg/$curr_prd");
            exit;
        }
        $ctgprds = $this->main_model->assign_type($pk_car, $ctg_prds->rows);
        $items['ctg_prds'] = $ctgprds;
    }

    /** MAIN TRIGGER/SELECTOR FOR 'sc_xxx' functions. */
    private function category_data($pk_car, $pk_ctg, $pk_lng) {
        $items = array();
        $haschild = $this->main_model->haschildlevel($pk_car, $pk_ctg);
        $this->sc_loop($haschild, $pk_car, $pk_ctg);
        // - Check if ctg isList or has products
        $list = $this->main_model->is_list($pk_car, $pk_ctg, $pk_lng);
        $ctg_prds = $this->main_model->get_ctg_prds($pk_car, $pk_ctg, $pk_lng);
        // - Cases for URL build: http://beta.seataccesoriescatalogue.com/es/category/category/45/48
        if (($ctg_prds->rows == null)) {
            $manager = $this->main_model->manager($pk_car, $pk_ctg, $pk_lng);
            $haschild = $this->main_model->url_selector($pk_car, $manager->rows);
            $items['ctg_relations'] = $haschild;
        } elseif (sizeof($haschild->rows) == 1) {
            $this->sc_checkType($items, $list, $pk_car, $ctg_prds, $pk_ctg);
        } elseif (sizeof($haschild->rows) > 1) {
            $manager = $this->main_model->manager($pk_car, $pk_ctg, $pk_lng);
            $haschild = $this->main_model->url_selector($pk_car, $manager->rows);
            $items['ctg_relations'] = $haschild;
        } else {
            if ($list['is_list'] === 'true') {
                $this->sc_isList($items, $list, $pk_car, $ctg_prds);
            } else {
                $this->sc_only1Prd($items, $ctg_prds, $pk_car);
            }
        } // - Default items
        $bc = $this->main_model->bc_builder($pk_car, $pk_ctg, $pk_lng);
        $items['bc_elements'] = $bc;
        $items['img_beauty'] = $this->main_model->get_imgbeauty($pk_car, $bc[0]['bc_ctg']);
        return $items;
    }

    //------------------------------------------------------------------------------------// 

    private function c_model($pk_car, $pk_lng) {
        $this->_view->recordSet['pk_car'] = $pk_car;
        $data = $this->loadModel('model');
        $model_results = $data->getname($pk_car, $pk_lng);
        return $this->_view->recordSet['car_name'] = $model_results['car_name'];
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    public function category($parameters) {   //Renders categories view [1 level]
        $pk_car = $parameters[0];
        $pk_ctg = $parameters[1];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $this->c_model($pk_car, $pk_lng);
        $obj = $this->loadModel('category');
        $this->set_mM($obj);
        $this->_view->recordSet['sidebar_ctgs'] = $this->main_model->get_mainctgs($pk_car, $pk_lng);
        $category_data = $this->category_data($pk_car, $pk_ctg, $pk_lng);
        $this->_view->recordSet['category_data'] = $category_data;
        $this->_view->render(HEADER, 'ctg', FOOTER);
    }

    public function ctg_list($parameters) {   // Renders list categories
        $pk_car = $parameters[0];
        $pk_ctg = $parameters[1];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $this->c_model($pk_car, $pk_lng);

        $obj = $this->loadModel('category');
        $this->set_mM($obj);
        $this->_view->recordSet['sidebar_ctgs'] = $this->main_model->get_mainctgs($pk_car, $pk_lng);
        $category_data = $this->category_data($pk_car, $pk_ctg, $pk_lng);
        $this->_view->recordSet['category_data'] = $category_data;
        $this->_view->render(HEADER, 'ctg_list', FOOTER);
    }

    public function ctg_prds($parameters) {   //Renders view 'ctg_prds'|| final category products
        $pk_car = $parameters[0];
        $pk_ctg = $parameters[1];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $this->c_model($pk_car, $pk_lng);

        $obj = $this->loadModel('category');
        $this->set_mM($obj);
        $this->_view->recordSet['sidebar_ctgs'] = $this->main_model->get_mainctgs($pk_car, $pk_lng);
        $category_data = $this->category_data($pk_car, $pk_ctg, $pk_lng);
        $this->_view->recordSet['category_data'] = $category_data;
        $this->_view->render(HEADER, 'ctg_prds', FOOTER);
    }

}
