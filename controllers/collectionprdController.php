<?php

/**
 * Class collectionprdController | controllers/collectionprdController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * COLLECTIONPRD - handles request related to collection product.
 *
 */
class collectionprdController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    public function collectionprd($argumentos) {
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);
        $pk_coll = $argumentos[0]; //pk_coll=pk_ctg
        $pk_prd = $argumentos[1];

        $objctg = $this->loadModel('collection');
        $bc = $objctg->bc_builder($pk_coll, $pk_lng);

        $obj = $this->loadModel('collectionprd');
        $prd_data = $obj->collection_prd_info($pk_prd, $pk_lng);
        $this->_view->recordSet['prd_info'] = $prd_data;

        $sb_prntctg = $bc[0]['bc_ctg'];
        $this->_view->recordSet['sidebar_ctgs'] = $objctg->sidebar_manager($pk_lng, $sb_prntctg);
        $this->_view->recordSet['coll_data']['bc_elements'] = $bc;
        $this->_view->render(HEADER, 'collectionprd', FOOTER);
    }

}
