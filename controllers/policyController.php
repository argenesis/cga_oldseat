<?php

/**
 * Class policyController | controllers/policyController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * POLICY - handles cookie policy view.
 * 
 * The cookies policy terms view are set as individual views(not a single view
 * with fields to populate) because of large text content. That is why there is
 * no intantiation of models in this controller.
 * 
 */
class policyController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /** Authorized role of user that can access this controller */
    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    public function policy() {
        if (constant('has_policy') === true) {
            $view = 'policy';
            $this->_view->render(HEADER, $view, FOOTER);
        } else {
            header('Location: ' . BASE_URL . "error/error/");
        }
    }

}
