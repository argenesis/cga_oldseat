<?php

/**
 * Class printController | controllers/printController.php
 *
 * @package     controllers
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * PRINT - handles requests for printing output(accessories/collection/wishlist).
 * 
 * @uses app/Cart
 * @property-read Cart $_iCart Property from this controller, which builds a singleton of a cart/wishlist. 
 *  
 */
class printController extends Controller {

    protected $_iCart;

    public function __construct() {
        parent::__construct();
        $this->_iCart = Cart::singleton();
    }

    protected function set_autRole() {
        $this->_autRole = 'ALL';
    }

    public function index() {
        
    }

    private function c_lng($lng_tag) {
        $lng_data = $this->loadModel('lang');
        $lngpk = $lng_data->getLngPK($lng_tag);
        $pk_lng = $lngpk['lng_pk'];
        return $pk_lng;
    }

    public function print_prd($argumentos) {
        $pk_car = $argumentos[0];
        $pk_prd = $argumentos[2];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);

        $objmodel = $this->loadModel('model');
        $model_results = $objmodel->getname($pk_car, $pk_lng);

        $objprd = $this->loadmodel('product');
        $prd_info = $objprd->prd_info($pk_car, $pk_lng, $pk_prd);

        $this->_view->recordSet['car_name'] = $model_results['car_name'];
        $this->_view->recordSet['prd_info'] = $prd_info;        
        $this->_view->render(PRINTING_HEADER, 'print_prd', NULL);
    }

    public function print_collprd($parameters) {
        $pk_prd = $parameters[1];
        $pk_lng = $this->c_lng($_SESSION['lang_tag']);

        $obj = $this->loadModel('collectionprd');
        $prd_info = $obj->collection_prd_info($pk_prd, $pk_lng);
        $this->_view->recordSet['prd_info'] = $prd_info;
        $this->_view->render(PRINTING_HEADER, 'print_collprd', NULL);
    }

    public function print_wishlist() {
        $this->_view->recordSet['iCart'] = $this->_iCart->getItems();
        $this->_view->render(SIMPLE_HEADER, 'print_wishlist', NULL);
    }
    
    /** TODO: servicio que genere una salida de impresion HTML de la wishlist
    pero utilizando como argumento de lang el idioma por defecto del
    mercado.*/

}
