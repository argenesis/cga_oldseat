<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', substr(realpath(dirname(__FILE__)), 0, -3) . DS);
define('APP_PATH', ROOT . 'app' . DS);
define('_COUNTRY', 'it');
define('DEFAULT_LANG', 'it');
include APP_PATH . 'Launcher.php';
?>