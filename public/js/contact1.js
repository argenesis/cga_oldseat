$(function () {

    /*$('#contact-form').validator(); */
    
    $('#contact-form').on('submit', function (e) {

        if (!e.isDefaultPrevented()) {
            var newurl = "testRecaptcha";

            $.ajax({
                type: "POST",
                url: newurl,
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    if (messageAlert && messageText) {
                        $('#contact-form').find('.messages').html(alertBox);
                        //$('#contact-form')[0].reset();
                        grecaptcha.reset();
                    }
                }
            });
            return false; 
        }
    })
});