/* !
 * COMMONVIEWS.JS: contains JS functionalities that are commonly required from views
 * > changeLang: it ALWAYS set's the new selected language AND THEN if data is valid.
 */

function get_baseurl() {
    //Set return server/localhost for development
    var absURL = window.location.href;
    var n = absURL.indexOf(".com");//     
    return absURL.substr(0, n + 8);
}

function changeLang(set_lng) {
    $.ajax({
        type: 'POST',
        url: get_baseurl() + 'lang/changelang',
        data: {newlang: set_lng},
        async: false,
        success: function () {
            location.reload();
        }
    });
}