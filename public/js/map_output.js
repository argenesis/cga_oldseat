/* !
 * MAP_OUTPUT.JS: contains JS functionalities that are commonly required to paint/fill the map
 * with data comming from the DDBB as geoJSON(JSON).
 * > initmap: default data to paint map on document
 */
map;
infowindow;

function mapLoader(){
    $(window).load(function () {
        initMap();
    });
};

function reloadMap() {
    setTimeout(function () {
         initMap();
    }, 1000);
};

function initMap() {   
    $('.spin-loader').show(); // Wheel spinner
    // Create a map object and specify the DOM element for display.
    var mapCanvas= document.getElementById('map-canvas');
    var mapOptions= {
        center: {lat: centerMapOnLAT, lng: centerMapOnLNG},  
        zoom: centerMapZOOM,
        streetViewControl: false,
        mapTypeControl: false
    };
    map = new google.maps.Map(mapCanvas, mapOptions);   
    
    // Listener for feature(Feature.Point.InfoWindow)
    infowindow = new google.maps.InfoWindow();

    // Gets data when event click occurs and send this data to the infowindow to be displayed (RBR)
    map.data.addListener('click', function (event) {
        var dealerName = event.feature.getProperty("name");
        var dealerAddress = event.feature.getProperty("address");
        var dealerMail = event.feature.getProperty("email");
        infowindow.setContent(" <div style='width:250px; text-align: center;'>" +
                "     <p>" + dealerName + "</p>" +
                "     <p>" + dealerAddress + "</p>" +
                "     <div style='margin-left: auto; margin-right: auto;'>" +
                "         <button type='button' class='btn seat_button_ficha bg-red' data-dismiss='modal' >" + textChoseAndClose +
                "         </button>" +
                "     </div>" +
                " </div>");
        infowindow.setPosition(event.feature.getGeometry().get());
        infowindow.setOptions({pixelOffset: new google.maps.Size(0, -30)});
        infowindow.open(map);
        $("#dealerName").val(dealerName);
        $("#dealerAddress").val(dealerAddress);
        $("#dealerMail").val(dealerMail);
        $("#wishlist").removeClass("in active");
        $("#formulari").addClass("in active");
        $("#wishlisttab").removeClass("active");
        $("#formularitab").addClass("active");
    });

    $('#mapsearchform').on('submit', function (event) {
        submit_map_form();
        event.preventDefault();
    });
    $('.spin-loader').hide(); // Wheel spinner
}

function submit_map_form() {
    $('.spin-loader').show(); // Wheel spinner
    //Remove previous data from data layer
    
    map.data.forEach(function (feature) {
        map.data.remove(feature);
        infowindow.close(map);// In order to close the infowindow of a previous search and selection (RBR)
    });
    // Current search by user - form values
    var city = $("#city").val();    
    var new_zoom = 6;
    setTimeout(function () {
        var geocoder = new google.maps.Geocoder; // Search coordinates of city, center by default
        if (city != '') {
            geocoder.geocode({'address': city}, function (results, status) {
                city_coordinates = results[0].geometry.location;
                city_lat = city_coordinates.lat();
                city_lng = city_coordinates.lng();
                radius = 1.1;
                new_zoom = 9;
            });
        } else {
            city_lat = 40.430814;
            city_lng = -3.701518;
            radius = 20;
            new_zoom = 6;
        }
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: get_baseurl() + 'map/mapsearch',
                data: {city: city, city_lat: city_lat, city_lng: city_lng, radius: radius},
                dataType: "json",
                success: function (data) {
                    $.each(data, function (key, feature) {
                        map.data.addGeoJson(feature); // add each one as JSON Object                
                    });
                    map.data.setStyle(function() {
                     return {icon:'http://gestor.seataccesoriescatalogue.com/imgs/find_dealer_logo.png'};//find_dealer_logo.png
                     });
                    var geocoder = new google.maps.Geocoder; 
                    geocoder.geocode({'address': city}, function (results, status) {// center and zoom view on the selection area
                        map.setCenter(results[0].geometry.location);
                        map.setZoom(new_zoom);
                    });
                } // success: END
            });
            $('.spin-loader').hide(); // Wheel spinner
        }, 1000);
    }, 1000);
}