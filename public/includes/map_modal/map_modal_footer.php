<footer>
    <div class="container-fluid">
        <div class="row footer-text">
            <div class="col-xs-12 col-sm-12 col-md-8">
                <div class="container-fluid">
                    <div class="col-xs-12 text-primary text-center">
                        <p class="seat-white seat-footer-little"><?= constant('Footer1'); ?><strong><?= constant('Footer2'); ?></strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
    //Parsing de valores para asignar centro de mapa(map_modal_footer.php)
    var centerMapOnLAT = <?php echo json_encode($_SESSION['center_map_on_LAT']);?>;
    var centerMapOnLNG = <?php echo json_encode($_SESSION['center_map_on_LNG']);?>;
    var centerMapZOOM = <?php echo json_encode($_SESSION['center_map_ZOOM']);?>;
    var textChoseAndClose = "<?= constant('mpCHOSE_btn'); ?>";
</script>
<script src="<?= JS_REPO ?>map_output.js"></script>
<?= constant('googlemaps');?>
</body>
</html>