<!-- Content -->
<div class="findDealer">
    <div class="cont-module">
        <div class="mapContainer container-fluid">
            <div id="map-canvas" style="position: relative; overflow: hidden; width: 100%; height: 700px; background-color: grey"></div>
            <div class="container-fluid">
                <div class="mapLeftRail row" style="width: 390px; height: 80px; margin-top: -50px; margin-left: -15px; z-index: 10;">
                    <div class="mapForm clearfix col-xs-12 col-sm-12">
                        <form id="mapsearchform" method="post">
                            <div class="pull-left">
                                <label for="city"><?= constant('mpPROV');?></label></br>
                                <select id="city" name="city">
                        <?php foreach ($_SESSION['country_provinces'] as $key => $value) { ?>                                    
                                    <option value='<?= $key;?>'><?= $value;?></option>                                    
                        <?php } ?>
                                </select>
                            </div>
                            <div class="pull-left" style="margin-left: 30px; margin-top: 5px">
                                <input type="button" value="<?= constant('mpSEARCH_btn');?>" onclick="javascript:submit_map_form()" class="btn seat_button_ficha bg-red">
                            </div>
                            <div class="loading-overlay">
                                <div class="spin-loader" style="display: none;"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= "</div></br>"; ?>
<!--end of content-->