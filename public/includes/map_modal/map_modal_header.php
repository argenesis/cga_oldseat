<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html>
<html lang="<?= $_SESSION['lang_tag']; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Catálogo General de Accesorios SEAT">
        <meta name="author" content="SN Consultors">
        <link rel="icon" type="image/vnd.microsoft.icon" href="imgs/favicon.ico">
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="imgs/favicon.ico">
        <title><?= constant('Head1'); ?></title> 
        <link href="<?= CSS_REPO ?>overlaySpinner.css" rel="stylesheet">
    </head>
    <body> 
        <!-- HEADER & CONTENT -->
        <?= "<div id=\"wrap\">"; // wrapping to fix footer at the bottom: +1 </div> on each view ?>
        <div class="container-fluid brdr-h-bottom" id="head_cga">
            <!-- Head -->
            <div class="row">
                <!--Logo-->
                <div class="col-xs-6 no-h-padding">
                    <a href="<?= BASE_URL . 'index'; ?>" style="padding-left: 5px;">
                        <img class="" src="<?= IMG_REPO . 'seat-logo.png'; ?>" height="80" alt="Seat logo">
                    </a>
                </div>
                <!--Title space-->
                <div class="col-xs-6" id="seat-catalogue-title">
                    <div class="row center-block">
                        <div class="pull-right">
                            <p class="seat-dark-grey-text meta-pro-thin fs-55 mrgn-bottom-0" style="padding-left:10px; line-height:50px;">SEAT</p>
                        </div>
                        <div style="text-align:right;">
                            <span class="seat-dark-grey-text meta-pro-light fs-20 text-right"><?= constant('Head2'); ?></span></br>
                            <span class="seat-red-text meta-pro-light fs-15 text-right"><?= constant('Head3'); ?></span>
                        </div>
                    </div>
                </div>
            </div>      
        </div><!-- end of Head-->  