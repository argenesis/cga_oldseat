<!-- Video modal -->
<div id="myVideoModal" class="modal fade in" role="dialog">
    <div class="modal-dialog" style="width: 80%; height: 80%;">
        <!-- Modal content-->
        <div class="modal-content" style="width: 100%; height: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><?= $this->recordSet['video_text']; ?></h3>
            </div>
            <div class="modal-body center-block text-center embed-responsive" style="width: 100%; height: 100%;">
                <video class="embed-responsive-item" controls>
                    <source src="<?= MEDIA_REPO . $this->recordSet['video_name']; ?>" type="video/mp4">
                    <?= constant('err_videoplayer'); ?>
                </video>
            </div>
            <?php /* <div class="modal-footer bg-inverse text-white" style="width: 100%;">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              </div> */ ?>
        </div>
    </div>
</div>
<!--Pause video on modal closing; resume playing when opening-->
<script type="text/javascript">
    $('#myVideoModal').modal({
        show: false
    }).on('shown.bs.modal', function () {
        $(this).find('video')[0].play();
    });
    $('#myVideoModal').modal({
        show: false
    }).on('hidden.bs.modal', function () {
        $(this).find('video')[0].pause();
    });
</script>