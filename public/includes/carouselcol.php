<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="6000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
        <li data-target="#myCarousel" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection1.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>
        <div class="item">
            <img class="second-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection2.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="third-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection3.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="fourth-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection4.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="fifth-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection5.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="sixth-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection6.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="seventh-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection7.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <div class="item">
            <img class="seventh-slide img-responsive" src="<?= IMG_REPO . 'slider_collection/slide_collection8.jpg'; ?>" alt="SEAT COLLECTION">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

    </div>      
</div>