<div class="col-xs-12 meta-pro-normal fs-12"> 
    <h3 class="pull-left"><?= constant('cformTITLE');?></h3>
    <a role="button" class="seat_button_ficha bg-black pull-right" data-toggle="modal" data-target="#myModal1" style="margin-top: 20px;" onclick="reloadMap();">
        <span class="seat-white-text glyphicon glyphicon-map-marker"></span> <?= constant('cformSELDEALER_btn');?>
    </a>
    <div class="clearfix"></div>
    <form id="contact-form" method="post" action="" role="form">
        <div class="messages"></div>
        <div class="controls">
            <div class="row brdr-h-bottom">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="dealerName"><?= constant('cformDEALERNAME');?></label>
                        <input id="dealerName" type="text" name="nombre_conc" class="form-control" placeholder="<?= constant('cformDEALERNAME_hint');?>" required="required" data-error="<?= constant('cformDEALERNAME_error');?>" readonly='readonly'/>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="dealerAddress"><?= constant('cformDEALERADRESS');?></label>
                        <input id="dealerAddress" type="text" name="direcc_conc" class="form-control" placeholder="<?= constant('cformDEALERADRESS_hint');?>" required="required" data-error="<?= constant('cformDEALERADRESS_error');?>" readonly='readonly' />
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="dealerMail"><?= constant('cformDEALERMAIL');?></label>
                        <input id="dealerMail" type="email" name="mail_conc" class="form-control" placeholder="<?= constant('cformDEALERMAIL_hint');?>" required="required" data-error="<?= constant('cformDEALERMAIL_error');?>" readonly='readonly' />
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="form_name"><?= constant('cformNAME');?></label>
                        <input id="form_name" type="text" name="name" class="form-control" placeholder="<?= constant('cformNAME_hint');?>" required="required" data-error="<?= constant('cformNAME_error');?>">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="form_lastname"><?= constant('cformSURNAME');?></label>
                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="<?= constant('cformSURNAME_hint');?>" required="required" data-error="<?= constant('cformSURNAME_error');?>">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="form_email"><?= constant('cformMAIL');?></label>
                        <input id="form_email" type="email" name="email" class="form-control" placeholder="<?= constant('cformMAIL_hint');?>" required="required" data-error="<?= constant('cformMAIL_error');?>">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="form_phone"><?= constant('cformPHONE');?></label>
                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="<?= constant('cformPHONE_hint');?>">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="form_message"><?= constant('cformMSG');?></label>
                        <textarea id="form_message" name="message" class="form-control" placeholder="<?= constant('cformMSG_hint');?>" rows="4"></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-sm-6"> 
                    <div class="form-group">
                        <?= constant('recaptcha');?>                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <input type="submit" class="btn seat_button_ficha bg-red btn-send pull-right" onclick="validateConc();" value="<?= constant('cformSEND_btn');?>" id="submit_contact">
                    <p class="text-muted"><strong>*</strong> <?= constant('cformNOTICE');?></p> 
                </div>
            </div>
        </div>         
    </form>    
</div> 
