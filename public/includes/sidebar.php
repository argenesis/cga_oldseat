<!--Sidebar Menu-->
<div id="menu_categories">
    <ul class="nav nav-sidebar meta-pro-normal">
        <!--Categories menu-->
        <?php
        foreach ($this->recordSet['sidebar_ctgs'] as $key => $value) {
            $ctgname = $value['ctg_name'];
            $ctgurl = $value['url'];
            echo "<li><a class='panel-body' href=\"$ctgurl\">$ctgname</a></li>";
        }
        ?>
    </ul>
</div>
<!--[END]Sidebar-->