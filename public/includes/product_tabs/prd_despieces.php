<!--Tab despieces-->
<div id="despieces_producto" class="tab-pane fade" style='height: 300px; overflow-y: auto;'>
    <br/>
    <table id="table_despieces" class="seat-deep-grey-text fs-14 meta-pro-normal">
        <tbody>
            <?php foreach ($this->recordSet['prd_despiece'] as $value) { ?>
                <tr>
                    <td style="border-bottom: 1px solid #ddd" class="remomended3" width="100px" height="25px">
                        <label>
                            <span class="taula seat-deep-grey-text meta-pro-normal fs-14">
                                <?= $value['desp_sku']?>
                            </span>
                        </label>
                    </td>
                <?php if(constant('has_prices')) { ?>
                    <td style="border-bottom: 1px solid #ddd" class="remomended3" width="350px" height="25px">
                        <label>
                            <span class="taula seat-deep-grey-text meta-pro-normal fs-14">
                                <?= $value['desp_text']?>
                            </span>
                        </label>
                    </td>
                    <td style="border-bottom: 1px solid #ddd;" class="remomended3 text-right" width="110px" height="25px">
                        <label>
                            <span class="taula seat-deep-grey-text meta-pro-normal fs-14">
                                <?= $value['desp_price']?> <?= constant('lCURR'); ?>
                            </span>
                        </label>
                    </td>
                <?php } else { ?>
                     <td style="border-bottom: 1px solid #ddd" class="remomended3" width="460px" height="25px">
                        <label>
                            <span class="taula seat-deep-grey-text meta-pro-normal fs-14">
                                <?= $value['desp_text']?>
                            </span>
                        </label>
                    </td>
                <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>































</table>
</div>