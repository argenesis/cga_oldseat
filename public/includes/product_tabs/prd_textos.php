<!-- Contenido Tab sección derecha con el textos_producto-->
<div id="textos_producto" class="tab-pane fade in active">
    <br>
    <!--Description-->
    <div class="meta-pro-normal productos_txt productos-txt-short" style="height: 180px;"><?= nl2br($this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_notes']); ?></div>
    <br>
    <!--MARKET PROPERTIES-->
    <?php
    $components = explode("/", $_SERVER['REQUEST_URI']);
    $car = $components[4];
    $ctg = $components[5];
    $prd = $components[6];
    $print_url = BASE_URL . "print/print_prd/$car/$ctg/$prd";
    include INCLUDES_REPO . 'wishlist/wl_product.php';
    ?>
    <!-- CART/WISHLIST -->                
    <?php
    //If prices are set but not empty, whether on [view/config_xx]...
    if (constant('has_prices')==true) {
        include INCLUDES_REPO . 'wishlist/wl_wishlist.php';
    }
    ?>
</div>