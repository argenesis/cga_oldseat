<?php //To set visibility for 'despiece' tab.
    $show_despiece= false;
    if($this->recordSet['prd_despiece']!= null) { 
        $show_despiece= true;
    } 
?>
<!--Description-->
<ul class="nav nav-tabs meta-pro-normal ">    
    <li id="textos_producto_tab" class="wishlisttabs active"><a data-toggle="tab" href="#textos_producto"><?= constant('tabTXT'); ?></a></li>
    <?php if($show_despiece) { ?>
    <li id="despieces_producto_tab" class="wishlisttabs"><a data-toggle="tab" href="#despieces_producto"><?= constant('tabDESP'); ?></a></li>
    <?php } ?>
</ul>
<div class="tab-content">
    <?php //
        include INCLUDES_REPO . 'product_tabs/prd_textos.php';
    if($show_despiece) {
        include INCLUDES_REPO . 'product_tabs/prd_despieces.php';        
        } 
    ?>
</div>
<script>
/** Script de gestión de dos grupos de tabs distintos.*/
$(document).onready(function(){
    $('.nav-tabs a').click(function(e){
    e.preventDefault();
    var tabIndex = $('.nav-tabs a').index(this);
        $(this).parent().siblings().removeClass("active");
        $(this).parent().addClass("active");
        $('.tab-pane:eq( '+tabIndex+' )').siblings().removeClass("in active");
        $('.tab-pane:eq( '+tabIndex+' )').addClass("in active");
    });
});
</script>