<script src="/views/cart/cart.js"></script>
<?php
//Parse PHP array[recordSet['prd_info']] into JSON Object[prd]
echo '<script type="text/javascript">';
echo 'var prd = ' . json_encode($this->recordSet['prd_info']) . ';';
echo 'var msg = \'' . constant('wlADD') . '\';';
echo '</script>';
?>
<div class="row mrgn-v-15">
    <a role="button" class="seat_button_ficha bg-red" href="javascript:addProduct(prd,msg);">
        <span class="glyphicon glyphicon-plus seat-white-text" aria-hidden="true"></span><?= constant('add_WL'); ?></a>

    <a role="button" class="seat_button_ficha bg-red" href="javascript:openCart();">
        <span class="glyphicon glyphicon-share-alt seat-white-text" aria-hidden="true"></span><?= constant('view_WL') ?></a>

    <a role="button" class="seat_button_ficha bg-grey" target="_blank"
       href="<?= $print_url; //$print_url is builded in the view that calls this file...     ?>">
        <span class="glyphicon glyphicon-print seat-white-text"></span><?= constant('lPRNT') ?></a>
</div>
<!--Disclaimer-->
<p class=" seat-deep-grey-text fs-10 mrgn-top-20"><?= constant('lDIS'); ?></p>