<?php //UT and PRINT button for product     ?>
<div class="row meta-pro-light datos-producto-padding seat-red-text ">
    <div class="col-xs-5">
        <span class="glyphicon glyphicon-info-sign pull-left">&nbsp;</span>
        <span class="meta-pro-normal pull-left"><?= constant('lUT'); ?></span>
    </div>
    <div class="col-xs-4 seat-deep-grey-text">
        <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_ut']; ?></strong></span>
    </div>
    <div class="col-xs-3 seat-deep-grey-text">
        <a role="button" class="seat_button_ficha bg-grey" target="_blank"
           href="<?= $print_url; //$print_url is builded in the view that calls this file...     ?>">
            <span class="glyphicon glyphicon-print seat-white-text"></span><?= constant('lPRNT') ?></a>
    </div>
</div>