<?php
//If prices are set but not empty, whether on [view/config_xx]...
if (constant('has_prices')==true) {
    ?>
    <!--Price-->
    <div class="row meta-pro-light datos-producto-padding seat-red-text ">
        <div class="col-xs-5">
            <span class="glyphicon glyphicon-info-sign pull-left"><strong>&nbsp</strong></span>
            <span class="meta-pro-normal pull-left"><strong><?= constant('lPRC'); ?></strong></span>
        </div>
        <div class="col-xs-4 seat-deep-grey-text">
            <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_price']; ?> <?= constant('lCURR'); ?></strong></span>
        </div>
        <div class="col-xs-3 seat-deep-grey-text">
        </div>
    </div>
    <?php if (constant('show_ut')) { ?>
    <!--UT-->
    <div class="row meta-pro-light datos-producto-padding seat-red-text ">
        <div class="col-xs-5">
            <span class="glyphicon glyphicon-info-sign pull-left"><strong>&nbsp</strong></span>
            <span class="meta-pro-normal pull-left"><strong><?= constant('lUT'); ?></strong></span>
        </div>
        <div class="col-xs-4 seat-deep-grey-text">
            <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_ut']; ?></strong></span>
        </div>
    </div>
    <?php } ?>
    <?php
} else { ?>
    <br/><br/><br/> 
    <?php
    include INCLUDES_REPO . 'wishlist/wl_onlyprint.php';
}
?>              