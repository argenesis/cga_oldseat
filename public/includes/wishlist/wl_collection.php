<?php if ($this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_size_apl'] == 1) { ?>
    <!--Size-->
    <!--Apply only to those not being "U" size-->
    <div class="row meta-pro-normal datos-producto-padding seat-red-text mrgn-top-20">
        <div class="col-xs-5">
            <span class="glyphicon glyphicon-info-sign pull-left">&nbsp</span>
            <span class="meta-pro-normal pull-left"><?= constant('lSIZ'); ?></span>
        </div>
        <div class="col-xs-4 seat-deep-grey-text">
            <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_size']; ?></strong></span>
        </div>
        <div class="col-xs-3 seat-deep-grey-text">
        </div>
    </div>
<?php } ?>
<!--Colour-->
<div class="row meta-pro-normal datos-producto-padding seat-red-text ">
    <div class="col-xs-5">
        <span class="glyphicon glyphicon-info-sign pull-left">&nbsp</span>
        <span class="meta-pro-normal pull-left"><?= constant('lCOL'); ?></span>
    </div>
    <div class="col-xs-4 seat-deep-grey-text">
        <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_color']; ?></strong></span>
    </div>
    <div class="col-xs-3 seat-deep-grey-text">
    </div>
</div>
<!--Price-->
<div class="row meta-pro-normal datos-producto-padding seat-red-text ">
    <?php
    //Show PRICE and UT depending on the market(there's a price for those to be shown)...
    //[config_xx]...
    if (constant('has_prices')==true) {
        ?>
        <div class="col-xs-5">
            <span class="glyphicon glyphicon-info-sign pull-left">&nbsp</span>
            <span class="meta-pro-normal pull-left"><?= constant('lPRC'); ?></span>
        </div>
        <div class="col-xs-4 seat-deep-grey-text">
            <span class="pull-left seat-helvetica"><strong><?= $this->recordSet['prd_info'][$_SESSION['lang_tag']]['prd_price']; ?> <?= constant('lCURR'); ?></strong></span>
        </div>
    <?php } else { ?>
        <div class="col-xs-5">                  
            <span class="meta-pro-normal pull-left"></span>
        </div>
        <div class="col-xs-4 seat-deep-grey-text">
            <span class="pull-left seat-helvetica"></span>
        </div>
        <div class="col-xs-3 seat-deep-grey-text">
            <a role="button" class="seat_button_ficha bg-grey" target="_blank"
               href="<?= $print_url; //$print_url is builded in the view that calls this file...     ?>">
                <span class="glyphicon glyphicon-print seat-white-text"></span><?= constant('lPRNT') ?></a>
        </div>                
    <?php } ?>
</div>