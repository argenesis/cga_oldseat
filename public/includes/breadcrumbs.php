<!--Breadcrumbs-->
<div class="container-fluid no-h-padding">
    <div class="row clearboth center-it no-h-padding">
        <div class="hidden-xs col-sm-12 no-h-padding">
            <ol class="breadcrumb meta-pro-normal">

                <?php if ($this->recordSet['category_data'] != null) { ?>          
                    <li><a class='seat-white meta-pro-normal fs-14' href="<?= BASE_URL . 'model/model/' . $this->recordSet['pk_car']; ?>"><?= $this->recordSet['car_name'] ?></a></li>          
                    <?php
                    foreach ($this->recordSet['category_data']['bc_elements'] as $key => $value) {
                        $bc_ctg = $value['bc_ctg_name'];
                        $bc_url = $value['bc_url'];
                        ?>
                        <li><a class='seat-white meta-pro-normal fs-14' href="<?= $value['bc_url']; ?>"><?= $value['bc_ctg_name']; ?></a></li>
                        <?php
                    }
                } elseif ($this->recordSet['coll_data'] != null) {
                    ?>
                    <li><a class='seat-white meta-pro-normal fs-14' href="<?= BASE_URL . 'collection/collectionselector' ?>">SEAT COLLECTION</a></li>
                    <?php
                    foreach ($this->recordSet['coll_data']['bc_elements'] as $key => $value) {
                        $bc_ctg = $value['bc_ctg_name'];
                        $bc_url = $value['bc_url'];
                        ?>
                        <li><a class='seat-white meta-pro-normal fs-14' href="<?= $bc_url; ?>"><?= $bc_ctg; ?></a></li>
                            <?php
                        }
                    }
                    ?>
            </ol>
        </div>
    </div>
</div><!--end of Breadcrumbs-->     