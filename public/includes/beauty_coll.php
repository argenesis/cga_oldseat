<!--Row beauty-->
<div class="col-xs-12 mrgn-bottom-20 jumbotron jumbotron-template no-h-padding" id="jumbotron_category_collection" style="border-radius: 0px;">    
    <div class="container">
        <?php
        //Link to videoModal: the html for the modal is included after the footer. 
        if (isset($this->recordSet['has_video'])) {
            ?>
            <div class="pull-right" style="margin-top: <?= $this->recordSet['video_margin_top']; ?>; margin-right: <?= $this->recordSet['video_margin_right']; ?>;">
                <div class="pull-right">
                    <span class="<?= $this->recordSet['video_text_color']; ?> meta-pro-bold fs-20">&nbsp;&nbsp;<?= $this->recordSet['video_text']; ?></span>
                </div>
                <div class="pull-right" style="margin-top: -10px;">
                    <a href="#" data-toggle="modal" data-target="#myVideoModal">
                        <span class="seat-red-text fs-50 glyphicon glyphicon-play-circle"></span>
                    </a>
                </div>
            </div>
            <div class="clearfix"><br></div>
        <?php } //End of Link to videoModal: the html for the modal is included after the footer.   ?>
        <div class="hero-titles">
            <span class="seat-white meta-pro-bold fs-55">SEAT</span></br>
            <span class="seat-white meta-pro-bold fs-50">COLLECTION</span>
            </br>
            <span class="pull-right seat-white meta-pro-bold fs-45" ><?= $this->recordSet['coll_data']['bc_elements'][0]['bc_ctg_name']; ?></span>
        </div>
    </div> 
</div>
<script type="text/javascript">
        img_url = '<?= $this->recordSet['coll_data']['jumbo']; ?>'; 
        document.getElementById("jumbotron_category_collection").style.background = '#000 url(' + img_url + ') no-repeat center right';  
</script>
<!--[END]Row-->