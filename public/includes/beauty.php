<?php 

function get_browser_name($user_agent) {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/'))
        return 'Opera';
    elseif (strpos($user_agent, 'Edge'))
        return 'Edge';
    elseif (strpos($user_agent, 'Chrome'))
        return 'Chrome';
    elseif (strpos($user_agent, 'Safari'))
        return 'Safari';
    elseif (strpos($user_agent, 'Firefox'))
        return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7'))
        return 'Internet Explorer';
    return 'Other';
}
?>
<!-- row cat beauty-->
<div class="row">
    <div class="col-xs-12 hidden-sm hidden-md hidden-lg no-h-padding" style="background-color: #333;">
        <!-- solo beauty en coches nuevos, en modelos anteriores beauty del coche --> 
        <img class='img-responsive' src='<?= $this->recordSet['category_data']['img_beauty']; ?>'/>
        </br>		
    </div>
</div>
<div class="row">
    <div class="col-xs-12" id="modelo-car">
        <div class="row" style="display: flex; flex-direction: row; align-items: stretch;">
            <div id="categories" class="col-xs-12 col-sm-2 sidebar" style="background-color: #333;">
                <!-- Sidebar Menu insertion -->
                <?php include INCLUDES_REPO . 'sidebar.php'; ?>
                <!-- End of Sidebar insertion-->
                <h2><?= $this->recordSet['car_name']; ?></h2>
            </div>
            <?php
//Safari has a bug when scaling through CSS...
            if (get_browser_name($_SERVER['HTTP_USER_AGENT']) == 'Safari') {
                ?>
                <div class="hidden-xs col-sm-10 no-h-padding">
                    <img class="img-responsive" src="<?= $this->recordSet['category_data']['img_beauty']; ?>" style="background-size: cover; background-position: left; background-repeat: no-repeat; background-color:#fff;">
                <?php } else { ?>
                    <div class="hidden-xs col-sm-10 no-h-padding" style="background-image: url('<?= $this->recordSet['category_data']['img_beauty']; ?>'); background-size: cover; background-position: left; background-repeat: no-repeat; background-color:#fff;">
                    <?php if (isset($this->recordSet['has_video'])) {  //Link to videoModal: the html for the modal is included after the footer. ?>
                        <div class="pull-right" style="margin-top: <?= $this->recordSet['video_margin_top']; ?>; margin-right: <?= $this->recordSet['video_margin_right']; ?>;">
                            <div class="pull-right">
                                <span class="<?= $this->recordSet['video_text_color']; ?> meta-pro-bold fs-20">&nbsp;&nbsp;<?= $this->recordSet['video_text']; ?></span>
                            </div>
                            <div class="pull-right" style="margin-top: -10px;">
                                <a href="#" data-toggle="modal" data-target="#myVideoModal">
                                    <span class="seat-red-text fs-50 glyphicon glyphicon-play-circle"></span>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"><br></div>
                    <?php } //End of Link to videoModal: the html for the modal is included after the footer.   ?>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- fin de la row-->