<!-- Franja del buscador-->
<div id="menu_ruta">
    <form class="buscador navbar-form" role="search" method="post" action="<?= BASE_URL . 'searchresults/searchresults'; ?>" style="margin:4px;">
        <div class="input-group add-on">
            <input class="form-control input-sm" placeholder="<?= constant('buscador'); ?>" name="prd_sku" id="prd_sku"  type="text">
            <div class="input-group-btn">
                <button class="btn btn-default input-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
    </form>
</div>
<!-- fin de la franja del buscador-->