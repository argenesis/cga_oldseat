<?php

//************************ Navigation bar models per market ****************************// 
$ctlr = 'model/model/'; //controller
$_SESSION['ul_navbar'] = array(
    'Arona' => array('Arona' => BASE_URL . $ctlr . '45'),
    'Ateca' => array('Ateca' => BASE_URL . $ctlr . '40'),
    'Alhambra' => array('Alhambra GP' => BASE_URL . $ctlr . '39'),
    'Ibiza' => array('Novo Ibiza' => BASE_URL . $ctlr . '44'),
    'Mii' => array('Mii<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' => BASE_URL . $ctlr . '26',
        'Mii 5P' => BASE_URL . $ctlr . '27'),
    'León' => array('X-PERIENCE' => BASE_URL . $ctlr . '35',
        'Leon ST PA' => BASE_URL . $ctlr . '43',
        'Leon 5P PA' => BASE_URL . $ctlr . '41',
        'Leon SC PA' => BASE_URL . $ctlr . '42'),
    'Toledo' => array('Toledo' => BASE_URL . $ctlr . '33')
);
//****map_modal_body*****// 0: all provinces, [option_value => label]
$_SESSION['country_provinces'] = array(
    ''=>'Todas',
    'Horta, Açores, Portugal'=>'Açores',
    'Aveiro, Aveiro, Portugal'=>'Aveiro',
    'Beja, Beja, Portugal'=>'Beja',
    'Braga, Braga, Portugal'=>'Braga',
    'Macedo de Cavaleiros , Bragança, Portugal'=>'Bragança',
    'Fundao, Castelo Branco, Portugal'=>'Castelo Branco',
    'Coimbra, Coimbra, Portugal'=>'Coimbra',
    'Évora, Évora, Portugal'=>'Évora',
    'Almodovar, Faro, Portugal'=>'Faro', 
    'Guarda, Guarda, Portugal'=>'Guarda', 
    'Leiria, Leiria, Portugal'=>'Leiria',
    'Lisboa, Lisboa, Portugal'=>'Lisboa',
    'Machico, Madeira, Portugal'=>'Madeira',
    'Portalegre, Portalegre, Portugal'=>'Portalegre',
    'Sao Tome, Porto, Portugal'=>'Porto',
    'Santarém, Santarém, Portugal'=>'Santarém',
    'Setúbal, Setúbal, Portugal'=>'Setúbal',
    'Arcos de Valdevez, Viana do Castelo, Portugal'=>'Viana do Castelo',
    'Murca, Vila Real, Portugal'=>'Vila Real',
    'Viseu, Viseu, Portugal'=>'Viseu'
);
//****map_modal_footer*****//
$_SESSION['center_map_on_LAT']= 39.832607;
$_SESSION['center_map_on_LNG']= -8.298442;
$_SESSION['center_map_ZOOM']= 6;

//************************ MARKET PARAMETERS ***************************//
$_SESSION['mrkt_prds'] = '(prd_local=1)'; //its a filter on itself in DDBB
$_SESSION['colmrkt_prds'] = '(prd_local=1)';
$_SESSION['mrkt_ign_models'] = '(2, 3, 4, 6, 8, 9, 100)'; //ignored models

//************************COOKIE-POLICY****************************//
define('has_policy', true);


//************************PRICES-AND-WISHLIST****************************//
define('has_prices', true);// TRUE: shows wishlist and prices
define('lCURR', '€');

?>	