<?php

//************************ Navigation bar models per market ****************************// 
$ctlr = 'model/model/'; //controller
$_SESSION['ul_navbar'] = array(
    'Arona' => array('Arona' => BASE_URL . $ctlr . '45'),
    'Ateca' => array('Ateca' => BASE_URL . $ctlr . '40'),
    'Alhambra' => array('Alhambra GP' => BASE_URL . $ctlr . '39'),
    'Ibiza' => array('Nuevo Ibiza' => BASE_URL . $ctlr . '44'),
    'Mii' => array('Mii<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' => BASE_URL . $ctlr . '26',
        'Mii 5P' => BASE_URL . $ctlr . '27'),
    'León' => array('X-PERIENCE' => BASE_URL . $ctlr . '35',
        'Leon ST PA' => BASE_URL . $ctlr . '43',
        'Leon 5P PA' => BASE_URL . $ctlr . '41',
        'Leon SC PA' => BASE_URL . $ctlr . '42'),
    'Toledo' => array('Toledo' => BASE_URL . $ctlr . '33')
);
//****map_modal_body*****// 0: all provinces, [option_value => label]
$_SESSION['country_provinces'] = array(
    ''=>'Todas',
    'Vitoria-Gasteiz, Arava, España'=>'Álava',
    'Albacete, Albacete, España'=>'Albacete',
    'Alicante, Alicante, España'=>'Alicante/Alacant',
    'Almeria, Almeria, España'=>'Almería',
    'Oviedo, Asturias, España'=>'Asturias',
    'Avila, Avila, España'=>'Ávila',
    'Merida, Badajoz, España'=>'Badajoz',
    'Barcelona, Barcelona, España'=>'Barcelona',
    'Burgos, Burgos, España'=>'Burgos',
    'Caceres, Caceres, España'=>'Cáceres',
    'Jerez de la Frontera, Cadiz, España'=>'Cádiz',
    'Santander, Cantabria, España'=>'Cantabria',
    'Castellon de la Plana, Castellon, España'=>'Castellón/Castelló',
    'Ceuta, Ceuta, España'=>'Ceuta',
    'Ciudad Real, Ciudad Real, España'=>'Ciudad Real',
    'Cordoba, Cordoba, España'=>'Córdoba',
    'Cuenca, Cuenca, España'=>'Cuenca',
    'Girona, Girona, España'=>'Girona',
    'Las Palmas de Gran Canaria, Las Palmas, España'=>'Las Palmas',
    'Granada, Granada, España'=>'Granada',
    'Guadalajara, Guadalajara, España'=>'Guadalajara',
    'Donosti, Guipuzcoa, España'=>'Guipúzcoa',
    'Huelva, Huelva, España'=>'Huelva',
    'Huesca, Huesca, España'=>'Huesca',
    'Palma de Mallorca, Illes Balears, España'=>'Illes Balears',
    'Jaen, Jaen, España'=>'Jaén',
    'Coruña, Acoruña, España'=>'A Coruña',
    'Logroño, La Rioja, España'=>'La Rioja',
    'Leon, Leon, España'=>'León',
    'Lleida, Lleida, España'=>'Lleida',
    'Lugo, Lugo, España'=>'Lugo',
    'Madrid, Madrid, España'=>'Madrid',
    'Malaga, Malaga, España'=>'Málaga',
    'Melilla, Melilla, España'=>'Melilla',
    'Murcia, Murcia, España'=>'Murcia',
    'Pamplona, Navarra, España'=>'Navarra',
    'Orense, Ourense, España'=>'Ourense',
    'Palencia, Palencia, España'=>'Palencia',
    'Pontevedra, Pontevedra, España'=>'Pontevedra',
    'Salamanca, Salamanca, España'=>'Salamanca',
    'Segovia, Segovia, España'=>'Segovia',
    'Sevilla, Sevilla, España'=>'Sevilla',
    'Soria, Soria, España'=>'Soria',
    'Tarragona, Tarragona, España'=>'Tarragona',
    'Santa Cruz de Tenerife, Tenerife, España'=>'Tenerife',
    'Teruel, Teruel, España'=>'Teruel',
    'Toledo, Toledo, España'=>'Toledo',
    'Valencia, Valencia, España'=>'Valencia/València',
    'Valladolid, Valladolid, España'=>'Valladolid',
    'Bilbao, Bilbo, España'=>'Vizcaya',
    'Zamora, Zamora, España'=>'Zamora',
    'Zaragoza, Zaragoza, España'=>'Zaragoza'
);
//****map_modal_footer*****//
$_SESSION['center_map_on_LAT']= 38.314484;
$_SESSION['center_map_on_LNG']= -3.532618;
$_SESSION['center_map_ZOOM']= 5;

//************************ MARKET PARAMETERS ***************************//queries in this market don't have AND on purpouse
$_SESSION['mrkt_prds'] = '(prd_local=1)'; //on ddbb market:(pr.prd_local=1 OR pr.prd_ext=1)
$_SESSION['colmrkt_prds'] = '(prd_local=1)'; //collection
$_SESSION['mrkt_ign_models'] = '(100)'; //ignored models

//************************COOKIE-POLICY****************************//
define('has_policy', true);

//************************PRICES-AND-WISHLIST****************************//
define('has_prices', true);// TRUE: shows wishlist and prices
define('lCURR', '€');

//************************MAILING-DATA****************************//
define('from', 'support@seatowa.com');
define('sendTo', 'ricard@snconsultors.es');//(defecto)re-escrito por correo de concesionario.
$_SESSION['send']= array(
    'sendAlso' => 'soporte@snconsultors.com', 'sendBlind' => 'support@seatowa.com');
define('subject', 'Nuevo correo desde el formulario del Catàlogo de Accesorios de SEAT');
$_SESSION['fields']= array(
    'nombre_conc' => 'Nombre_concesionario', 'direcc_conc' => 'Direccion_concesionario',
    'mail_conc' => 'Correo_concesionario', 'name' => 'Nombre_cliente', 
    'surname' => 'Apellidos_cliente', 'phone' => 'Telefono_cliente', 
    'email' => 'Correo_cliente', 'message' => 'Mensage'); // array variable name => Text to appear in the email
define('okMessage', 'Se ha completado con éxito el envío.');
define('errorMessage', 'Post vacio.');

?>	