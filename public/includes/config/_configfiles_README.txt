22/09/2017 - 12:38
@author Argenis Urribarri<argenis@snconsultors.es>

################################################################################
   Configuración de nuevo archivo 'config' - Catálogo General de Accesorios
################################################################################

El archivo "_templateconfig.php" es una plantilla para crear un nuevo fichero 
de configuracion de pais/mercado/lang. Solo se debe duplicar el archivo y renombrarlo
a "config_xx.php" donde "xx" es el codigo ISO de Lenguaje para el nuevo
archivo.
El proyecto se ha diseñado para trabajar con TODAS LAS CONSTANTES definidas
en este archivo. Esto significa que TODAS LAS CONSTANTES DEBEN DEFINIRSE, 
incluso si tienen un valor vacio del tipo ''; donde se indique, debe asignarse
un valor null y no un campo vacio tipo ''.

######## Navigation bar models per market ########
$_SESSION['ul_navbar']: es un array que contiene los modelos del market. Los 
    datos de este array DEBEN SER 'hard-codeados'. El nombre del elemento HTML
    que contiene la maquetacion para este array es: <li class="dropdown">
    ... se encuentra en views/layout/default/header.php
    (ej: Mexico no tiene algunos modelos que aparecen en este dropdown).

###### map_modal_body ###### 0: all provinces, [option_value => label] ######
$_SESSION['country_provinces']: es un array que contiene las provincias para
    realizar busquedas sobre el mapa en la wishlist/cart. 
    El comentario a la derecha indica que la posicion [0] de este array
    debe contener un valor que sea "ALL-PROVINCES" de todas las provincias
    para pasarlo como argumento[TODO: leer valor de traduccion].
    Se informa del nombre del archivo que contiene el HTML que se rellena con
    la informacion de este array (public/includes/map_modal/map_modal_body.php)

######## map_modal_footer ########
$_SESSION['center_map_on_LAT']= 0;
    Valor para LATITUDE en donde se va a CENTRAR EL MAPA al ser cargado.
$_SESSION['center_map_on_LNG']= 0;
    Valor para LONGITUDE en donde se va a CENTRAR EL MAPA al ser cargado.
$_SESSION['center_map_ZOOM']= 0;//zoom level
    Valor para nivel de ZOOM en donde se va a CENTRAR EL MAPA al ser cargado.

######## MARKET PARAMETERS ########
Todas las constantes relacionadas especificamente con el market/language dentro
de este apartado AFECTAN LAS QUERIES DE LOS MODELOS.
$_SESSION['mrkt_prds'] = ; 
    Son los productos que aplican al mercado X para ACCESORIOS.
$_SESSION['colmrkt_prds'] = ;
    Son los productos que aplican al mercado X para COLLECTION.        
$_SESSION['mrkt_ign_models'] = ;
    Son los modelos de vehiculo que SE IGNORAN POR NO APLICAR AL MERCADO.    

######## COOKIE POLICY ########
define('has_policy', true);
    Indica si aplica una politica de cookies o no. Este valor DEBE SER BOOLEANO.
    True: aplica/solicita aprobación de cookies; False: no aplica/no solicita.

######## PRICES-AND-WISHLIST ########
define('has_prices', false);
    true: muestra precios y TAMBIEN LA WISHLIST; false: no los muestra.
    Este valor DEBE SER BOOLEANO.
define('lCURR', '€');
    Valor de tipo de divisa para el mercado.

######## MAILING-DATA ########
Incluye la informacion adicional sobre contactos a los que deben llegar los
correos electornicos generados en la wishlist.

    ! A fecha 21/11/2017 estos campos NO SON UTILIZADOS; se usan los valores definidos
    en el Config.php de proyecto, bajo el directorio app/.
    
    define('from', '');
    define('sendTo', '');//(defecto)re-escrito por correo de concesionario.
    $_SESSION['send']= array(
        'sendAlso' => '', 'sendBlind' => '');
    define('subject', '');
    $_SESSION['fields']= array(
        'nombre_conc' => '', 'direcc_conc' => '',
        'mail_conc' => '', 'name' => '', 
        'surname' => '', 'phone' => '', 
        'email' => '', 'message' => ''); //array: ('variable_name' => 'Text to appear in the email.')
    define('okMessage', '');
    define('errorMessage', '');