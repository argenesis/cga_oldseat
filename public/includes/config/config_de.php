<?php

//************************ Navigation bar models per market ****************************// 
$ctlr = 'model/model/'; //controller
$_SESSION['ul_navbar'] = array(
    'Arona' => array('Arona' => BASE_URL . $ctlr . '45'),
    'Ateca' => array('Ateca' => BASE_URL . $ctlr . '40'),
    'Alhambra' => array('Alhambra GP' => BASE_URL . $ctlr . '39'),
    'Ibiza' => array('Neuer Ibiza' => BASE_URL . $ctlr . '44'),
    'Mii' => array('Mii<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' => BASE_URL . $ctlr . '26',
        'Mii 5P' => BASE_URL . $ctlr . '27'),
    'León' => array('X-PERIENCE' => BASE_URL . $ctlr . '35',
        'Leon ST PA' => BASE_URL . $ctlr . '43',
        'Leon 5P PA' => BASE_URL . $ctlr . '41',
        'Leon SC PA' => BASE_URL . $ctlr . '42'),
    'Toledo' => array('Toledo' => BASE_URL . $ctlr . '33')
);
//************************ MARKET PARAMETERS ****************************//
$_SESSION['mrkt_prds'] = '(prd_ext=1)'; //
$_SESSION['colmrkt_prds'] = '(prd_ext=1)';
$_SESSION['mrkt_ign_models'] = '(2, 3, 4, 6, 8, 9, 100)'; //ignored models

//************************COOKIE-POLICY****************************//
define('has_policy', true);


//************************PRICES-AND-WISHLIST****************************//
define('has_prices', false);// TRUE: shows wishlist and prices
define('lCURR', '€');

?>