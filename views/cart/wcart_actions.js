/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function emptyCart(msg)
{
    alert(msg);
    $.ajax({
        type: "POST",
        url: get_baseurl() + 'cart/emptycart'
    }).done(function () {
        location.reload();
    });
}
function deleteItem(uid)
{
    $.ajax({
        type: "POST",
        url: get_baseurl() + 'cart/delete',
        data: {"uid": uid}
    }).done(function () {
        location.reload();
    });
}
function updateQuantity(uid, newQuantity)
{
    var update_data = {
        "uid": uid,
        "newQuantity": newQuantity
    };
    $.ajax({
        type: "POST",
        url: get_baseurl() + 'cart/update',
        data: {"updatedata": update_data}
    }).done(function () {
        location.reload();
    });
}
function validateQuantity(uid, oldcount, msg)
{
    newQuantity = document.getElementById("input_quantity_" + uid).value;
    /*primer valido que no estigui el camp del formulari buit '' i si ho està llenço error, poso valor original i surto*/
    if ((newQuantity == "") || (newQuantity == null)) {
        alert(msg);//"Este campo..."
        document.getElementById("input_quantity_" + uid).value = oldcount;
        return;
    }
    /*després valido que el contingut de camp del formulari sigui numèric i si NO ho és llenço error, recupero el valor original i surto*/
    if (isNaN(newQuantity)) {
        alert(msg);//"Este campo..."
        document.getElementById("input_quantity_" + uid).value = oldcount;
        return;
    }
    /*finalment valido que el valor sigui major a 1 y enter i si no llenço error, recupero valor original i surto*/
    if ((newQuantity < 1) || ((newQuantity) % 1 != 0)) {
        alert(msg);//"Este campo..."
        document.getElementById("input_quantity_" + uid).value = oldcount;
        return;
    }
    /*finalment crido al métode per desar la nova cantitat que finalment fa un reload de la vista per recalcular totals*/
    updateQuantity(uid, newQuantity);
}

