function openCart()
{
    window.open(get_baseurl() + 'cart/cart', "Cart", "toolbar=no, scrollbars=yes, resizable=yes, top=0px, left=0px, width=900px, height=1000px");
}
function addProduct(prd, msg)
{   //"ctg_prd","prd_pk","prd_sku","prd_name","prd_notes","prd_img","prd_price","prd_ut","prd_new"        
    $.ajax({
        type: "POST",
        url: get_baseurl() + 'cart/add',
        data: {"prd_data": prd}
    }).done(function () {
        confirm(msg);
        location.reload();
    });
}