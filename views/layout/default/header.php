<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html>
<html lang="<?= $_SESSION['lang_tag']; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Catálogo General de Accesorios SEAT">
        <meta name="author" content="SN Consultors">
        <link rel="icon" type="image/vnd.microsoft.icon" href="<?= IMG_REPO ?>favicon.ico">
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<?= IMG_REPO ?>favicon.ico">
        <title><?= constant('Head1'); ?></title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?= CSS_REPO ?>jquery-ui-1.10.4.custom.min.css"/>
        <link href="<?= CSS_REPO ?>bootstrap.min.css" rel="stylesheet">
       
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="<?= CSS_REPO ?>ie10-viewport-bug-workaround.css" rel="stylesheet">    
        <!-- Custom styles for this template -->
        <link href="<?= CSS_REPO ?>CGA.css" rel="stylesheet">
        <!--<link href="<?= CSS_REPO ?>map_view.css" rel="stylesheet">-->
        <link href="<?= CSS_REPO ?>carousel.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>nav-tabs.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>cookiesModal.css" rel="stylesheet">
            
        <script src="<?= JS_REPO ?>ie-emulation-modes-warning.js"></script>
         
    </head>
    <body> 
        <!-- HEADER & CONTENT -->
        <?= "<div id=\"wrap\">"; // wrapping to fix footer at the bottom: +1 </div> on each view ?>
        <div class="container-fluid" id="head_cga">
            <!--Head-->
            <div class="row">
                <!--Logo-->
                <div class="col-xs-6 col-md-2 no-h-padding">
                    <a href="<?= BASE_URL . 'index'; ?>" style="padding-left: 5px;">
                        <img class="" src="<?= IMG_REPO ?>seat-logo.png" height="80" alt="Seat logo">
                    </a>
                </div>
                <!--Title space-->
                <div class="col-xs-6 col-md-push-7 col-md-3" id="seat-catalogue-title">
                    <div class="row center-block">
                        <div class="pull-right">
                            <p class="seat-dark-grey-text meta-pro-thin fs-55 mrgn-bottom-0" style="padding-left:10px; line-height:50px;">SEAT</p>
                        </div>
                        <div style="text-align:right;">
                            <span class="seat-dark-grey-text meta-pro-light fs-20 text-right"><?= constant('Head2'); ?></span></br>
                            <span class="seat-red-text meta-pro-light fs-15 text-right"><?= constant('Head3'); ?></span>
                        </div>
                    </div>
                </div>
                <!--Central space-->
                <div class="col-xs-12  col-md-pull-3 col-md-7">
                    <div class="row">
                        <!--Empty div-->
                        <div class="hidden-xs col-sm-4"></div>
                        <!--Lang selector dropdown-->
                        <div class="col-xs-12 col-sm-3 text-center" style="margin-top: 10px;">
                            <div class="dropdown">
                                <span class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="text-uppercase seat-dark-grey-text meta-pro-normal"><?= $_SESSION['lang']; ?></span>
                                    <span>&nbsp;</span>
                                    <span class="caret seat-light-grey-text"></span>
                                </span>
                                <ul class="dropdown-menu text-center meta-pro-normal" id="langs-dropdown">                  
                                    <?php
                                    foreach ($this->ul_langsdropdown as $lang => $lang_values) {
                                        foreach ($lang_values as $lang_name => $lang_href) {
                                            ?>
                                            <li class="text-center">
                                                <a href="<?= $lang_href; ?>" onclick="javascript:changeLang('<?= $lang; ?>')">
                                                    <span class="text-uppercase seat-dark-grey-text" ><?= $lang_name; ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>                  
                                </ul>
                            </div>
                        </div>
                        <!--Search form-->
                        <div class="hidden-xs col-sm-4" style="margin-top: 2px;">
                            <?php include INCLUDES_REPO . 'buscador.php' ?>
                        </div>
                        <!--Empty div-->
                        <div class="hidden-xs col-sm-1"></div>
                    </div>
                    <!--Family cars navigation bar-->
                    <nav class="navbar navbar-default center-block" id="cars-nav">
                        <!--Collapsed menu icon-->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                
                            </button>
                        </div>
                        <!--Collapsible cars menu by dropdowns-->
                        <div class="collapse navbar-collapse meta-pro-normal" id="myNavbar">            
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="<?= BASE_URL . 'collection/collectionselector'; ?>">
                                        <span class="seat-red-text">SEAT COLLECTION</span>
                                    </a>
                                </li>                                
                                <!--Item dropdown-->                
                                <?php foreach ($this->ul_models as $model_family_name => $model_values) { ?>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <?= $model_family_name; ?><span class="caret seat-red-text"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($model_values as $model_name => $href) { ?>
                                                <li><a href="<?= $href; ?>"><?= $model_name; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>                               
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!--end of Head-->
        </div>
        <?php
// Display breadcrumbs [Vehicles OR Collection]
        if ((isset($this->recordSet['category_data']['bc_elements'])) || (isset($this->recordSet['coll_data']['bc_elements']))) {
            include_once INCLUDES_REPO . 'breadcrumbs.php';
        }
