<!--separates footer-->
<footer id="footer">
  <div class="footer-wraper">
    <div class="container-fluid">
        <div class="row footer-text">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="container-fluid">
                    <div class="col-xs-12 text-primary text-center">
                        <p class="seat-white seat-footer-little"><?= constant('Footer1'); ?><strong><?= constant('Footer2'); ?></strong></p>
                    </div>
                </div>
            </div>
            <?= '<!--@[2017/10/16]-->' ?>
                </div>
        </div>
    </div>
        <!--Cookies modal-->
        <div id="myCookieModal"  class="modal fade in" role="dialog">
            <div id="cookies-modal-dialog" class="modal-dialog" style="width: 100%; height: 18%;">
                <!-- Modal content-->
                <div id="cookies-modal-content" class="modal-content" style="width: 100%; height: 100%;">
                    <div class="modal-body center-block text-center" style="width: 100%; height: 100%;">
                        <div class="copy link-text">
                            <span type="button" class="close" data-dismiss="modal">&times;</span>
                            <h3 class="modal-title"><?= constant('modal_title') ?></h3>
                            <p><?= constant('modal_txt11') ?><a href="<?= BASE_URL . 'policy/policy' ?>"><?= constant('modal_txt12_href') ?></a><?= constant('modal_txt13') ?></p>
                            <a class="btn btn-danger cookie-button seat_button_ficha bg-red" type="button" data-dismiss="modal" onclick="javascript: setCookie('username', 'invitado', '21');"><span class="fs-15"><?= constant('accept_btn') ?></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
</footer>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?= JS_REPO ?>jquery.min.js"><\/script>')</script>
<script src="<?= JS_REPO ?>jquery-2.1.1.js"></script>
<script src="<?= JS_REPO ?>commonviews.js"></script>
<script type="text/javascript" src="<?= JS_REPO ?>jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript">
    // Change JQueryUI plugin names to fix name collision with Bootstrap.
    $.widget.bridge('uitooltip', $.ui.tooltip);
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?= JS_REPO ?>bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="<?= JS_REPO ?>holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?= JS_REPO ?>ie10-viewport-bug-workaround.js"></script>

    <script type="text/javascript">
        $(function () {
            /*En onload activar autocompletado del buscador*/
            $("#prd_sku").autocomplete({
                minLength: 3,
                source: function (request, response) {
                    $.ajax({
                        url: "<?= BASE_URL ?>searchresults/autocomplete/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.rows, function (item, i) {
                                return {
                                    label: item['prd_name'],
                                    value: item['prd_sku']
                                };
                            }));
                        }
                    });
                }
            });
            /*En onload activar política cookies modal*/
            checkCookie();
        });

        /* Cookies - si en fer check no existeix et conecta la modal de cookies*/
        function checkCookie() {
            var user = getCookie("username");
            if (user != "") {
                /*alert("Welcome again " + user);*/
                return;
            } else {
                $('#myCookieModal').modal('show');
                /*user = prompt("Please enter your name:", "");*/
                /*if (user != "" && user != null) {
                 setCookie("username", user, 365);
                 }*/
            }
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    </script>
</body> 
</html>
<?php include INCLUDES_REPO . 'media/video_modal.php' ?>