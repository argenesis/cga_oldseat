<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html>
<html lang="<?= $_SESSION['lang_tag']; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Catálogo General de Accesorios SEAT">
        <meta name="author" content="SN Consultors">
        <link rel="icon" type="image/vnd.microsoft.icon" href="<?= IMG_REPO ?>favicon.ico">
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<?= IMG_REPO ?>favicon.ico">
        <title><?= constant('Head1'); ?></title>
        <!-- Bootstrap core CSS -->
        <link href="<?= CSS_REPO ?>bootstrap.min.css" rel="stylesheet">
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="<?= CSS_REPO ?>ie10-viewport-bug-workaround.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= CSS_REPO ?>CGA.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>SEAT_estils.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>carousel.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>nav-tabs.css" rel="stylesheet">
        <link href="<?= CSS_REPO ?>cookiesModal.css" rel="stylesheet">

        <script src="scripts/ie-emulation-modes-warning.js"></script>
    </head>