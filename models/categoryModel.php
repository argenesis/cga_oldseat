<?php

/**
 * Class categoryModel | models/categoryModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * CATEGORY - handles the STRUCTURE for categories in the VEHICLES MODEL section.
 * 
 * This class builds data for two outputs: the "images-navigation", and the BREADCRUMB.
 * All the methods/functions requiring TEXT in the return value contain two SQL strings;
 * this is because in the database the translations for the products and categories texts
 * and everything related DOESN'T have the spanish value, because the values for this are
 * included in the same table(products/categories).
 * 
 * @todo Make ALL METHODS return whether array type or object type(->rows).
 * @todo Refactor URL assignment function names (ctg,prd)...
 * 
 */
class categoryModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Main categories(ctg_level=1) are set in the "sidebar".
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function get_mainctgs($pk_car, $pk_lng) {
        $result = null;
        $sql = "SELECT ctg_pkcategory AS `ctg_pkcategory`,
                IF( $pk_lng != 5, cattrans_name, ctg_name)  AS `ctg_name`,
                ctg_is_list AS `is_list` 
                FROM categories 
                LEFT JOIN  cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng
                LEFT JOIN catdetails ON ctg_pkcategory = cdt_cat 
                WHERE ctg_level = 1 
                ORDER BY ctg_order ASC;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->mainctgs_url($pk_car, $result->rows);
    }

    /** Check if there are any child categories.
     * $_SESSION['mrkt_ign_models'] is defined in "config_xx.php"
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID). */
    public function haschildlevel($pk_car, $pk_ctg) {
        $result = null;
        $sql_ctgHasSubLevel = " SELECT hrc_child 
                                FROM hierarchy 
                                WHERE hrc_pkcar= $pk_car AND hrc_pkcar NOT IN " . $_SESSION['mrkt_ign_models'] . " AND hrc_parent= $pk_ctg;";
        $result_query = $this->_db->query($sql_ctgHasSubLevel);
        $result->rows = $this->GetAllRecords($result_query);
        return $result;
    }

    /** Check if the argument "pk_ctg" is of type list.
     * $_SESSION['mrkt_ign_models'] is defined in "config_xx.php".
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function is_list($pk_car, $pk_ctg, $pk_lng) {
        $result = null;
        $list = array();
        $sql = "    SELECT DISTINCT ctg_is_list AS `is_list`,
                    IF( $pk_lng != 5, cattrans_name, ctg_name)  AS `ctg_name`,
                    cdt_image AS `ctg_img`
                    FROM catdetails
                    LEFT JOIN categories ON cdt_cat = ctg_pkcategory  
                    LEFT JOIN cattrans ON cdt_cat = cattrans_pkcategory AND cattrans_lang = $pk_lng
                    LEFT JOIN hierarchy ON hrc_child = cdt_cat AND hrc_pkcar = $pk_car
                    WHERE cdt_cat = $pk_ctg AND cdt_model IN (0, $pk_car) AND cdt_model NOT IN " . $_SESSION['mrkt_ign_models'] . " 
                    ORDER BY hrc_order, hrc_child ASC;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        $is_list = $result->rows[0]['is_list'];
        if ($is_list == 1) {
            $list['is_list'] = 'true';
            $img = $result->rows[0]['ctg_img'];
            $list['img'] = IMG_REPO . "imgs/cats/$img";
            $list['name'] = $result->rows[0]['ctg_name'];
            return $list;
        } else {
            return $list;
        }
    }

    /** Only for TRANSITIONAL categories. 
     * $_SESSION['mrkt_ign_models'] is defined in "config_xx.php".
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function manager($pk_car, $pk_ctg, $pk_lng) {
        $result = null;
        $sql = "SELECT DISTINCT hrc_child as `ctg`,
                    IF($pk_lng = 5, ctg_name, cattrans_name) AS `ctg_name`, 
                    ctg_is_list as `is_list`,
                    cdt_image as `ctg_image`   
                    FROM hierarchy 
                    JOIN categories ON hrc_child = ctg_pkcategory
                    LEFT JOIN cattrans ON hrc_child= cattrans_pkcategory AND cattrans_lang = $pk_lng 
                    LEFT JOIN catdetails ON hrc_child = cdt_cat AND cdt_model IN (0, $pk_car)
                    WHERE hrc_parent = $pk_ctg AND hrc_pkcar = $pk_car AND hrc_pkcar NOT IN " . $_SESSION['mrkt_ign_models'] . "  
                    ORDER BY hrc_order, hrc_child ASC";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result;
    }

    /** Products in the category(final-category). 
     * $_SESSION['mrkt_prds'] is defined in "config_xx.php".
     * There is a hard-coded condition in the SQL, which is the following:
     * "(pr.prd_visible!='0000-00-00' AND pr.prd_baja='0000-00-00')".
     * The former is included because the situation can be that of a product existing
     * but its not meant to be seen on the catalogue.
     * 
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function get_ctg_prds($pk_car, $pk_ctg, $pk_lng) { 
        $result = null;
        $current_country = constant('_COUNTRY');
        $sql = "SELECT DISTINCT 
                    prd_pkproduct AS `pk_prd`,
                    prd_sku AS `prd_sku`,
                    prd_install AS `prd_ut`,
                    IF($pk_lng = 5, prd_name, prodtrans_name) AS `prd_name`, 
                    IF($pk_lng = 5, prd_notes, prodtrans_notes) AS `prd_notes`,
                    IF(d.dtl_image IS NULL, IF(d1.dtl_image IS NULL, '00.jpg', d1.dtl_image),d.dtl_image) AS `prd_img`,
                    IF('$current_country'='es', prd_price, (IF(prc_price IS NULL, 0, prc_price))) as `prd_price`, 
                    prt_pkcategory AS `pk_ctgfinal`
                    FROM products
                    JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcategory = $pk_ctg AND prt_pkcar = $pk_car
                    LEFT JOIN prodtrans ON prd_pkproduct = prodtrans_pkproduct AND prodtrans_lang = $pk_lng
                    LEFT JOIN langs ON LOWER(langs_tag) = '$current_country'
                    LEFT JOIN prices ON prd_pkproduct = prc_pkproduct AND prc_country = langs_pk
                    LEFT JOIN details d ON prd_pkproduct = d.dtl_pkproduct AND d.dtl_car = $pk_car
                    LEFT JOIN details d1 ON prd_pkproduct = d1.dtl_pkproduct AND d1.dtl_car = 0
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] .";";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result;
    }

    /** To build the path as string for the beauty image.
     * The structure follows the next pattern:
     * 1) $pk_ctg= NULL; Landing image for model(model view).
     * 2) sfx= XX; Name of suffix for the picture.
     * 2.1) For ctg_beauty_models(current models) there is a SPECIFIC SUFFIX(each case on the switch)
     * FOR EACH MAIN CATEGORY.
     * 2.2) For ctg_beauty_models(other models) there is only a "default" image ('default' of switch).
     * 
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @todo Make "source" notation shows this function's source code. */
    public function get_imgbeauty($pk_car, $pk_ctg) {
        if ($pk_ctg === NULL) {
            return IMG_REPO . "coches/fondo_$pk_car.jpg";
        } else {
            $sfx = '';
            $img_beauty = IMG_REPO . "beauty/cat-beauty/";

            switch ($pk_car) {
                case 26: case 27://Fml Mii
                    $sfx = '_mii';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 39://Fml Alhambra
                    $sfx = '_alh';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 36: case 37: case 38://Fml Ibiza
                    $sfx = '_ib';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 35://Fml León X-PERIENCE
                    $sfx = '_xp';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 33://Fml Toledo
                    $sfx = '_tld';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 31: case 32: case 34://Fml León (no X-PERIENCE)
                    $sfx = '_ln';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 41: case 42: case 43://Fml León PA (no X-PERIENCE)
                    $sfx = '_lnpa';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 40://Fml Ateca
                    $sfx = '_atk';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 44://Fml Ibiza NF
                    $sfx = '_ibnf';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                case 45://Fml Arona
                    $sfx = '_arn';
                    return $img_beauty . $pk_ctg . $sfx . ".jpg";
                default:
                    return IMG_REPO . "coches/fondo_$pk_car.jpg";
            }
        }
    }

    /** To format img,URL and link-type(controller)for each element of mainctgs.
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param mixed $mainctgs Array containing RAW results from ddbb. */
    public function mainctgs_url($pk_car, &$mainctgs) {
        foreach ($mainctgs as $key => $value) {
            $m_ctg = $value['ctg_pkcategory'];
            if ($value['is_list'] == 0) { //has child_ctg
                $ctg_res = $this->haschildlevel($pk_car, $m_ctg);
                if (sizeof($ctg_res->rows) != 0) {
                    $mainctgs[$key]['url'] = BASE_URL . "category/category/$pk_car/$m_ctg";
                } elseif (sizeof($ctg_res->rows) == 0) {
                    $mainctgs[$key]['url'] = BASE_URL . "category/ctg_prds/$pk_car/$m_ctg";
                }
            } elseif ($value['is_list'] == 1) {
                $mainctgs[$key]['url'] = BASE_URL . "category/ctg_list/$pk_car/$m_ctg";
            }
        }
        return $mainctgs;
    }

    /** To format img,URL and link-type(controller)for each element of category.
     * If a value is missing or NULL is replaced by a "default" value(example:"00.jpg").
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param mixed $results Array containing RAW results from ddbb. */
    public function url_selector($pk_car, &$results) {
        foreach ($results as $key => $value) {
            $rel = $value['ctg'];
            $img = $value['ctg_image'];
            $results[$key]['ctg_image'] = IMG_REPO . "imgs/cats/$img";
            $results[$key]['error'] = IMG_REPO . "imgs/cats/00.jpg";

            if ($value['is_list'] == 0) { //check if has child_ctg
                $ctg_res = $this->haschildlevel($pk_car, $value['ctg']);
                if (sizeof($ctg_res->rows) != 0) {
                    $results[$key]['url'] = BASE_URL . "category/category/$pk_car/$rel";
                } elseif ((sizeof($ctg_res->rows) == 0) || (sizeof($ctg_res->rows) == null)) {
                    $results[$key]['url'] = BASE_URL . "category/ctg_prds/$pk_car/$rel";
                }
            } elseif ($value['is_list'] == 1) {
                $results[$key]['url'] = BASE_URL . "category/ctg_list/$pk_car/$rel";
                ;
            }
        }
        return $results;
    }

    /** To format img,URL and link-type(controller)for each element of product.
     * If a value is missing or NULL is replaced by a "default" value(example:"00.jpg").
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param mixed $results Array containing RAW results from ddbb. */
    public function assign_type($pk_car, &$results) {
        foreach ($results as $key => $value) {
            $ctgfinal = $value['pk_ctgfinal'];
            $pkprd = $value['pk_prd'];
            $img = $value['prd_img'];
            $results[$key]['prd_img'] = IMG_REPO . "imgs/petites/$img";
            $results[$key]['error'] = IMG_REPO . "imgs/cats/00.jpg";
            $results[$key]['url'] = BASE_URL . "product/product/$pk_car/$ctgfinal/$pkprd";
        }
        return $results;
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * The builder starts from the CURRENT CTG; so it actually loops its way
     * UP towards a MAIN CATEGORY.
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function bc_builder($pk_car, $pk_ctg, $pk_lng) {
        $_pk_ctg = $pk_ctg;
        $breadcrumbs_structure = array();
        $looper = true;

        while (!$looper == false) {
            $has_parent = $this->bc_getnames($pk_car, $_pk_ctg, $pk_lng, 1);
            if (sizeof($has_parent) < 1) {
                $val = $this->bc_getnames($pk_car, $_pk_ctg, $pk_lng, 2);
                array_push($breadcrumbs_structure, $val[0]);
                $looper = false;
            } else {
                $i = 0;
                $val = $this->bc_getnames($pk_car, $_pk_ctg, $pk_lng, 0);
                array_push($breadcrumbs_structure, $val[0]);
                $_pk_ctg = $has_parent[$i]['bc_ctg']; //change the current ctg to 'parent'
                $i++;
            }
        }
        $bc = array_reverse($breadcrumbs_structure); //...because we build it from 'last' to 'first' ctg.
        return $this->bc_assigntype($pk_car, $bc);
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * Both variables are arrays corresponding to each required query.
     * 
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param int $pk_ctg Category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID).
     * @param int $type The request type to select the query string[0:ctg name; 1:name of ctg->parent; 2:mainctg for ctg].
     * @return array Key=>pair values; it contains the pk_ctg(int), pk_name(string) and isList(1/0:true/false). */
    public function bc_getnames($pk_car, $pk_ctg, $pk_lng, $type) {
        $result = null;
        $sql = array(
            "   SELECT DISTINCT ctg_pkcategory AS `bc_ctg`, 
                IF($pk_lng = 5, ctg_name, cattrans_name) AS `bc_ctg_name`, 
		ctg_is_list AS `bc_ctg_is_list` 
		FROM hierarchy
                LEFT JOIN categories ON hrc_child = ctg_pkcategory
		LEFT JOIN cattrans ON hrc_child = cattrans_pkcategory AND cattrans_lang = $pk_lng
		LEFT JOIN catdetails ON hrc_child = cdt_cat
		WHERE hrc_pkcar = $pk_car AND hrc_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." AND hrc_child = $pk_ctg ;", 
            "   SELECT DISTINCT ctg_pkcategory as `bc_ctg`,
		IF($pk_lng = 5, ctg_name, cattrans_name) AS `bc_ctg_name`, 
		ctg_is_list AS `bc_ctg_is_list` 
                FROM hierarchy
                LEFT JOIN categories ON hrc_parent = ctg_pkcategory
                LEFT JOIN cattrans ON hrc_parent = cattrans_pkcategory AND cattrans_lang = $pk_lng
                LEFT JOIN catdetails cd ON hrc_parent = cdt_cat
		WHERE hrc_pkcar = $pk_car AND hrc_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." AND hrc_child = $pk_ctg ;",
            "   SELECT DISTINCT ctg_pkcategory as `bc_ctg`,
		IF($pk_lng = 5, ctg_name, cattrans_name) AS `bc_ctg_name`,
                '0' AS `bc_ctg_is_list` 
		FROM categories
		LEFT JOIN cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng
                WHERE ctg_pkcategory = $pk_ctg ;");
        $result_query = $this->_db->query($sql[$type]);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * To format URL and link-type(controller)for each element of breadcrumb.
     * @param int $pk_car Vehicle model ddbb unique identifier(ID).
     * @param mixed $results Array containing RAW results from ddbb. */
    public function bc_assigntype($pk_car, &$results) {
        foreach ($results as $key => $value) {
            $rel = $value['bc_ctg'];
            if ($value['bc_ctg_is_list'] == 0) { //has child_ctg
                $ctg_res = $this->haschildlevel($pk_car, $value['bc_ctg']);
                if (sizeof($ctg_res->rows) != 0) {
                    $results[$key]['bc_url'] = BASE_URL . "category/category/$pk_car/$rel";
                } elseif (sizeof($ctg_res->rows) == 0) {
                    $results[$key]['bc_url'] = BASE_URL . "category/ctg_prds/$pk_car/$rel";
                }
            } elseif ($value['bc_ctg_is_list'] == 1) {
                $results[$key]['bc_url'] = BASE_URL . "category/ctg_list/$pk_car/$rel";
            }
        }
        return $results;
    }

}
