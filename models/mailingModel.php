<?php 
/**
 * Class mailingModel | models/mailingModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * MAILING - MODEL: gestiona la configuracion de parametros y datos que se
 * requieren para iniciar un servicio de envio de correo electronico.
 * 
 */
class mailingModel extends Model {

    public function __construct() {
        parent::__construct();        
    }
    
    /** Verifica si hay elementos en la wishlist y construye una respuesta en string.
     * 
     * @param mixed $wlContent Array que contiene los items de la wishlist.
     * @return string El contenido en formato string para agregarlo al string final. */
    private function verifyWlContent($wlContent, $type) {        
        switch ($type) {
            case constant('DEFAULT_LANG'): //dealer/market lang.
                if((empty($wlContent))||($wlContent==null)){
                    $wl_content= constant('mdEmptyWL');
                } else {
                $wl_content= mailingModel::wlcontentForDealer_toHTML($wlContent,$type);
                } break;
            case $_SESSION['lang_tag']: //ui/client selected lang.
                if((empty($wlContent))||($wlContent==null)){
                    $wl_content= constant('mEmptyWL');
                } else {
                $wl_content= mailingModel::wlcontentForClient_toHTML($wlContent,$type);
                } break;
            }
            return $wl_content;
        }
    
    /** Contenido del mensaje a enviar al concesionario/dealer.
     * El idioma del contenido del correo debe ser el idioma por defecto DEL MERCADO.
     * 
     * @todo Implementar constantes que contengan los valores segun idioma. */
    private function toConcessionaire_mailContent($formValues,$wlContent) {
        $type= constant('DEFAULT_LANG');//idioma en que generar el wlContent.
        $wl_content = $this->verifyWlContent($wlContent, $type);
        $emailText = "<body style='font-family: MetaProNormal, Arial, sans-serif; width=650px; margin-left: auto; margin-right: auto;'>"
                        . "<p>".constant('mH2'). "<b>". $formValues['dealer_name'] ."</b></p>";
        $emailText .= "<p>". constant('mP1'). $formValues['user_name']." ".$formValues['user_surname'] . constant('mP2') ."</p>";         
        $emailText .= "<p>". constant('mP3_1') ."</p>";
        
        $emailText .= "<p>". constant('mP3_2') ."</p>";        
        $emailText .= "<p><b>". $formValues['user_name']." ".$formValues['user_surname'] ."</b></p>";
        $emailText .= "<p>". $formValues['user_email'] ."</p>";
        $emailText .= "<p>". $formValues['user_phone'] ."</p>";
        
        $emailText .= "<p>". constant('mP4_1') ."</p>";   
        $emailText .= "<p>". constant('mP4_2') ."</p>";
        $emailText .= $wl_content;
        
        $emailText .= "<p>". constant('mP5') ."</p>";        
        $emailText .= "<p>". constant('mP6') ."</p>";
        $emailText .= "<p><i>". constant('mP7') ."</i></p></br></div>";        
        $emailText .= "</body>";
        return $emailText;
    }
    /** Contenido del mensaje a enviar al cliente/usuario.
     * El idioma del contenido del correo debe ser aquel que este seleccionado por el usuario.
     * 
     * @todo Implementar constantes que contengan los valores segun idioma. */
    private function toClient_mailContent($formValues,$wlContent) {
        $type= $_SESSION['lang_tag'];//idioma en que generar el wlContent.
        $wl_content = $this->verifyWlContent($wlContent, $type);
        $emailText = "<body style='font-family: MetaProNormal, Arial, sans-serif; width=650px; margin-left: auto; margin-right: auto;'>"
                        . "<p>".constant('mcH2').$formValues['user_name']." ".$formValues['user_surname']. "</p>";
        $emailText .= "<p>". constant('mcP1') ."</p>";
        $emailText .= "<p>". constant('mcP2') . $formValues['dealer_name'] . constant('mcP3_1') ."</p></br>";
        $emailText .= "<p>". constant('mcP3_2') ."</p>";
        
        $emailText .= "<p><b>". $formValues['dealer_name'] ."</b></p>";
        $emailText .= "<p><b>". $formValues['dealer_adress'] ."</b></p>";
        $emailText .= "<p>". constant('mcP4_1') . $formValues['dealer_phone']."</p></br>";
        
        $emailText .= "<p>". constant('mcP4_2') ."</p>";
        $emailText .= $wl_content;
        
        $emailText .= "<p>". constant('mcP5') ."</p></br>";
        $emailText .= "<p>". constant('mcP6') ."</p>";
        $emailText .= "<p><i>". constant('mcP7') ."</i></p></br></div>";
        $emailText .= "</body>";
        return $emailText;
    }
    
    /** Envio de correo al concesionario/dealer.
     * 
     * @author Argenis Urribarri<argenis@snconsultors.es>
     * @param string $from Direccion del remitente.
     * @param string $sendTo Direccion del ...
     * @param string $sendAlso Direccion(es) a enviar como copia.
     * @param string $sendBlind Direccion a enviar(modo oculto).
     * @param mixed $formValues Valores introducidos por el usuario.
     * @param mixed $wlContent Contenido de la wishlist.
     * @return boolean El envio del correo; true:enviado - false:error. */
    public function toConcessionaire_mail($from, $sendTo, $sendAlso, $sendBlind, $formValues, $wlContent) {
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset="UTF-8"' . "\r\n";
        $cabeceras .= 'From: ' . $from . "\r\n";
        $cabeceras .= 'Cc: ' . $sendAlso . "\r\n";
        $cabeceras .= 'Bcc: ' . $sendBlind . "\r\n";
        
        $subject= constant('mSubjectForDealer');
        $emailText= $this->toConcessionaire_mailContent($formValues,$wlContent);
        return $this->sendMail($sendTo, $subject, $emailText, $cabeceras);
    }
    
    /** Envio de correo al cliente/usuario. 
     * 
     * @author Argenis Urribarri<argenis@snconsultors.es>
     * @param string $from Direccion del remitente.
     * @param string $sendTo Direccion del destinatario.
     * @param string $sendAlso Direccion(es) a enviar como copia.
     * @param string $sendBlind Direccion a enviar(modo oculto).
     * @param mixed $wlContent Contenido de la wishlist.
     * @return boolean El envio del correo; true:enviado - false:error. */
    public function toClient_mail($from, $sendTo, $sendAlso, $sendBlind, $formValues, $wlContent) {
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset="UTF-8"' . "\r\n";
        $cabeceras .= 'From: ' . $from . "\r\n";
        $cabeceras .= 'Cc: ' . $sendAlso . "\r\n";
        $cabeceras .= 'Bcc: ' . $sendBlind . "\r\n";
        
        $subject= constant('mcSubjectForClient');
        $emailText= $this->toClient_mailContent($formValues,$wlContent);
        return $this->sendMail($sendTo, $subject, $emailText, $cabeceras);
    }
    
    /** El envio del correo. Se informa del valor que devuelve para gestionar
     * el mensaje de informacion al usuario.
     * 
     * @author Argenis Urribarri<argenis@snconsultors.es>
     * @return boolean true: si se ha aceptado el email de destino como valido para envio. */
    private function sendMail($sendTo, $subject, $emailText, $cabeceras) {
        return mail($sendTo, $subject, $emailText, $cabeceras);
    }
    
    /** Conversor de contenido de wishlist a string HTML - DEALER.
     * constant('DEFAULT_LANG') -> market_lang/dealer
     * $_SESSION['lang_tag'] -> lang/client.
     * Los nombres de las constantes son diferentes a los de la funcion equivalente
     * para el client.
     * 
     * @author Argenis Urribarrri<argenis@snconsultors.es>
     * @param mixed $vListItems Array con los elementos de la wishlist.
     * @param string $type Tipo de seleccion: dealer(DEFAULT_LANG),client(lang_tag).
     * @return string String que contiene el texto en HTML del contenido de wishlist. */
    public static function wlcontentForDealer_toHTML($vListItems, $type) {        
        $stringBuilder = '
        <div style="width:650px; margin:0px auto 20px auto; font-family: MetaProNormal, Arial, sans-serif;">
        <h3>'.constant('mwlTITLE').'</h3>
            <table style=" width:100%;">
                <tr height="30px" style="color: red; font-size: 12px;">
                    <th width="18%">&nbsp;&nbsp;'.constant('mlREF').'</th>
                    <th width="15%">&nbsp;&nbsp;</th>
                    <th width="30%">&nbsp;&nbsp;'.constant('mwlNAME').'</th>
                    <th width="6%" style="text-align:right;">'.constant('mwlUNIT').'.&nbsp;&nbsp;</th>
                    <th width="13%" style="text-align:right;">'.constant('mwlPRC').'&nbsp;&nbsp;</th>
                    <th width="13%" style="text-align:right;">'.constant('mwlTOT').'&nbsp;&nbsp;</th>
                    <th width="5%" style="text-align:center;">&nbsp;</th>
                </tr>';       
        $vTotal = 0;
        foreach ($vListItems as $item) {
            $vSubTotal = $item[$type]['prd_price'] * $item['count'];            
            $vTotal = $vTotal + $vSubTotal;
            $stringBuilder .= '
                    <tr style="color: gray; font-family: MetaProNormal, Arial, sans-serif; font-size: 12px;">
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;">' . $item[$type]['prd_sku'] . '</td>
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><img src="' .$item[$type]['prd_img']. '" onerror="' .IMG_REPO. 'imgs/imgs/00.jpg;" width="100" height="80" border="0"/></td>
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;">' . $item[$type]['prd_name'] . '</td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . $item['count'].'</span></td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . $item[$type]['prd_price'] . ' ' .constant('lCURR'). '</span></td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . number_format((float)$vSubTotal, 2, '.', '') . ' '.constant('lCURR').'</span></td>
                        <td align="center" style=" padding:5px;">&nbsp;</td>
                    </tr>';
        } 
        $stringBuilder .= '
                <tr>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td align="right" style="font-family: MetaProBold, Arial, sans-serif; padding:5px;">'.strtoupper(constant('mwlTOT')).':</td>
                    <td align="right" style=" padding:5px;"><span style="font-family: helvetica, sans-serif; font-size: 12px;"><strong>' . number_format((float)$vTotal, 2, '.', '') . ' '.constant('lCURR').'</strong></span></td>
                    <td style=" padding:5px;">&nbsp;</td>
                </tr>
            </table>
            <br/>
            <div style="clear: both;">                
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('mwlDIS').'</span>	
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('mwlDIS_1_1').'</span>	
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('mwlDIS_1_2').'</span>	
            </div>
            </div>
        </div>';
        return $stringBuilder;
    }
    
    /** Conversor de contenido de wishlist a string HTML - CLIENT.
     * constant('DEFAULT_LANG') -> market_lang/dealer
     * $_SESSION['lang_tag'] -> lang/client.
     * Los nombres de las constantes son diferentes a los de la funcion equivalente
     * para el dealer.
     * 
     * @author Argenis Urribarrri<argenis@snconsultors.es>
     * @param mixed $vListItems Array con los elementos de la wishlist.
     * @param string $type Tipo de seleccion: dealer(DEFAULT_LANG),client(lang_tag).
     * @return string String que contiene el texto en HTML del contenido de wishlist. */
    public static function wlcontentForClient_toHTML($vListItems, $type) {        
        $stringBuilder = '
        <div style="width:650px; margin:0px auto 20px auto; font-family: MetaProNormal, Arial, sans-serif;">
        <h3>'.constant('wlTITLE').'</h3>
            <table style=" width:100%;">
                <tr height="30px" style="color: red; font-size: 12px;">
                    <th width="18%">&nbsp;&nbsp;'.constant('lREF').'</th>
                    <th width="15%">&nbsp;&nbsp;</th>
                    <th width="30%">&nbsp;&nbsp;'.constant('wlNAME').'</th>
                    <th width="6%" style="text-align:right;">'.constant('wlUNIT').'.&nbsp;&nbsp;</th>
                    <th width="13%" style="text-align:right;">'.constant('wlPRC').'&nbsp;&nbsp;</th>
                    <th width="13%" style="text-align:right;">'.constant('wlTOT').'&nbsp;&nbsp;</th>
                    <th width="5%" style="text-align:center;">&nbsp;</th>
                </tr>';       
        $vTotal = 0;
        foreach ($vListItems as $item) {
            $vSubTotal = $item[$type]['prd_price'] * $item['count'];            
            $vTotal = $vTotal + $vSubTotal;
            $stringBuilder .= '
                    <tr style="color: gray; font-family: MetaProNormal, Arial, sans-serif; font-size: 12px;">
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;">' . $item[$type]['prd_sku'] . '</td>
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><img src="' .$item[$type]['prd_img']. '" onerror="' .IMG_REPO. 'imgs/imgs/00.jpg;" width="100" height="80" border="0"/></td>
                        <td style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;">' . $item[$type]['prd_name'] . '</td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . $item['count'].'</span></td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . $item[$type]['prd_price'] . ' ' .constant('lCURR'). '</span></td>
                        <td align="right" style=" padding:5px; font-family: MetaProNormal, Arial, sans-serif;"><span style="font-family: helvetica, sans-serif;">' . number_format((float)$vSubTotal, 2, '.', '') . ' '.constant('lCURR').'</span></td>
                        <td align="center" style=" padding:5px;">&nbsp;</td>
                    </tr>';
        } 
        $stringBuilder .= '
                <tr>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td style=" padding:5px;">&nbsp;</td>
                    <td align="right" style="font-family: MetaProBold, Arial, sans-serif; padding:5px;">'.strtoupper(constant('wlTOT')).':</td>
                    <td align="right" style=" padding:5px;"><span style="font-family: helvetica, sans-serif; font-size: 12px;"><strong>' . number_format((float)$vTotal, 2, '.', '') . ' '.constant('lCURR').'</strong></span></td>
                    <td style=" padding:5px;">&nbsp;</td>
                </tr>
            </table>
            <br/>
            <div style="clear: both;">
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('wlDIS').'</span>	
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('lDIS_1_1').'</span>	
                <span style="font-family: MetaProNormal, Arial, sans-serif; font-size: 12px; color: gray;">'.constant('lDIS_1_2').'</span>
            </div>
        </div>';
        return $stringBuilder;
    }
}
