<?php

/**
 * Class documentsModel | models/documentsModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * DOCUMENTS - handles relations between pk_prd and documents->files.
 * 
 * @todo Make ALL METHODS return whether array type or object type(->rows).
 * @todo Refactor URL assignment function names (ctg,prd)...
 * 
 */
class documentsModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getFiles($pk_prd) {
        $result = null;
        $sql = "SELECT doc_pkproduct AS `pk_prd`, doc_pkfile AS `pk_file`, doc_dag AS `file_dag`, file_name, file_mime, file_size, file_type 
                FROM documents 
                LEFT JOIN files ON doc_pkfile = file_pkfile 
                WHERE doc_pkproduct = $pk_prd 
                ORDER BY doc_order;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->assign_type($result->rows);
    }

    public function assign_type(&$results) {
        foreach ($results as $key => $value) {
            $name = $value['file_name'];
            $results[$key]['url'] = DOCS_REPO . $name;
        }
        return $results;
    }

}
