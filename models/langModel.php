<?php

/**
 * Class langModel | models/langModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * LANG - MODEL: handles tags and pk for language related queries.
 * 
 * Dating 19/04/2017 the($pk_lng) is related to the market and country as well.
 * 
 * @todo Build an abstraction layer to make a difference between MARKET, LANGUAGE and COUNTRY. 
 * 
 */
class langModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Retrieve lng_pk from ($_SESSION['lang_tag']) 
     * MX: translations dont exists for pk_lng 12(MX), so query sets 12 as 5 when using MX.
     * 
     * @param string $lng_tag Lang tags are ISO 639-1 code based. */
    public function getLngPK($lng_tag) {
        $_lng_tag = strtoupper($lng_tag); //important!!
        $result = null;
        $sql = "SELECT langs_pk as `lng_pk` FROM langs WHERE langs_tag LIKE '" . $_lng_tag . "';";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows[0];  
    }

}
