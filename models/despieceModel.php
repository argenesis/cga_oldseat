<?php 
/**
 * Class despieceModel | models/despieceModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.5
 */
/**
 * DESPIECE - manipulacion de datos que relacionan pk_prd, pk_car y despieces.
 * 
 */
class despieceModel extends Model {   
        public function __construct() 
    {   parent::__construct();
    }
        /** Obtener despieces para un producto. 
         * 
         * @revision Ricard Beyloc
         * @param int $pk_prd Identificador en BBDD de producto(obtener a partir de SKU).
         * @param int $pk_car Identificador en BBDD de modelo de vehiculo.
         * @param int $pk_lng Identificador en BBDD de idioma.
         * @return mixed Array asociativo: 'desp_sku','desp_text','pk_lng'.*/
        public function getDespieces($pk_prd,$pk_car,$pk_lng) {
        $result= null;
        $current_country = constant('_COUNTRY');
        $sql= "SELECT 
                desp_sku, 
                IF(dt_name IS NOT NULL, dt_name, IF(tes_text IS NOT NULL, tes_text, '')) AS `desp_text`,
                IF(dpr_price IS NOT NULL, dpr_price, '--') AS `desp_price`,
                tes_id_lang AS `pk_lng`
                FROM despieces
                LEFT JOIN langs ON langs_tag = UPPER('".$current_country."')
                LEFT JOIN desp_prices ON dpr_sku = desp_SKU AND dpr_country = langs_pk
                LEFT JOIN desp_trans ON dt_sku = desp_sku AND dt_lang = $pk_lng
                LEFT JOIN vw_tesauro ON tes_id_element = desp_id AND tes_id_object = 3 AND tes_id_lang = $pk_lng
                WHERE desp_pkproduct = $pk_prd AND desp_pkcar = $pk_car 
                ORDER BY tes_id_lang , desp_sku;";
            $result_query = $this->_db->query($sql);
            $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }
}