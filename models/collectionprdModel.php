<?php

/**
 * Class categoryModel | models/collectionModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * COLLECTIONPRD - retrieves collection product data.
 * 
 * All the methods/functions requiring TEXT in the return value contain two SQL strings;
 * this is because in the database the translations for the products and categories texts
 * and everything related DOESN'T have the spanish value, because the values for this are
 * included in the same table(col_products).
 *  
 */
class collectionprdModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Product info; all of the product fields use the 'prd_' prefix, except the 'pk_prd'.
     * @param int $pk_prd Unique identifier in ddbb for product.
     * @param int $pk_lng Unique identifier in ddbb for language. */
    public function collection_prd_info($pk_prd, $pk_lng) {
        $current_country = constant('_COUNTRY');
        $result = null;
        $sql = "SELECT pk_ctgfinal, prd_pk, prd_sku, prd_name, prd_notes, prd_color, prd_size, prd_size_apl, prd_img, prd_price, prd_new, pk_lng, LOWER(lang_tag) AS `lang_tag` 
                FROM (
                SELECT DISTINCT 
                prt_pkcategory AS `pk_ctgfinal`,
                prd_pkproduct AS `prd_pk`,
                prd_sku AS `prd_sku`,
                prodtrans_name AS `prd_name`,
                prodtrans_notes AS `prd_notes`,
                prodtrans_color AS `prd_color`,
                prodtrans_size AS `prd_size`,
                prd_size_apl,
                IF(dtl_image IS NOT NULL, dtl_image, '00.jpg') AS `prd_img`,
                IF('$current_country' = 'es', prd_price, prc_price) AS `prd_price`,
                prd_new,
                l1.langs_pk AS `pk_lng`,
                l1.langs_tag AS `lang_tag`
                FROM col_products 
                LEFT JOIN col_prodtrans ON prd_pkproduct = prodtrans_pkproduct
                LEFT JOIN col_parents ON prd_pkproduct = prt_pkproduct
                LEFT JOIN col_details ON dtl_pkproduct = prd_pkproduct
                LEFT JOIN langs l2 ON l2.langs_tag = UPPER('$current_country') 
                LEFT JOIN col_prices ON prc_pkproduct = prd_pkproduct AND prc_country = l2.langs_pk
                LEFT JOIN langs l1 ON l1.langs_pk = prodtrans_lang
                WHERE prd_pkproduct = $pk_prd
                UNION ALL
                SELECT DISTINCT 
                prt_pkcategory AS `pk_ctgfinal`,
                prd_pkproduct AS `prd_pk`,
                prd_sku AS `prd_sku`,
                prd_name AS `prd_name`,
                prd_notes AS `prd_notes`,
                prd_color AS `prd_color`,
                prd_size AS `prd_size`,
                prd_size_apl,
                IF(dtl_image IS NOT NULL, dtl_image, '00.jpg') AS `prd_img`,
                IF('$current_country' = 'es', prd_price, prc_price) AS `prd_price`,
                prd_new,
                5 AS `pk_lng`,
                'ES' AS `lang_tag`
                FROM col_products
                LEFT JOIN col_parents ON prd_pkproduct = prt_pkproduct
                LEFT JOIN col_details ON dtl_pkproduct = prd_pkproduct 
                LEFT JOIN langs ON langs_tag = UPPER('pt') 
                LEFT JOIN col_prices ON prc_pkproduct = prd_pkproduct AND prc_country = langs_pk
                WHERE prd_pkproduct = $pk_prd
                ) 
                results ORDER BY pk_lng;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->assign_type($result->rows);
    }
    
    /** Asignar formato de datos; construye un array asociativo para cada 'lang_tag'
     * que se usa como indice.
     * 
     * @param mixed $results Datos de producto; contiene en cada registro su 'lang_tag'. 
     * @return mixed Array asociativo; el 'tag' de idioma es la 'key'.*/
    public function assign_type($results) {
        $update= array();        
        foreach ($results as $value) {
            $_tag= $value['lang_tag'];
            $img = $value['prd_img'];
            $value['prd_img'] = IMG_REPO . "col-prods/$img";
            $value['error'] = IMG_REPO . "imgs/00.jpg";
            if(!(array_key_exists($_tag, $update))){                
                $update[$_tag]= $value;
            }            
        }        
        return $update;
    }

}
