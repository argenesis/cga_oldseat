<?php

/**
 * Class seleccionModel | models/seleccionModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * SELECCION - contains programatical and hard-coded data in the sorting indicated by SEAT
 * for VEHICLES MODELS "image-navigation".
 * 
 * This class was build to keep consistency in the MVC arquitecture, so it contains
 * hardcoded data in some methods(indicated with names containing "_hc_"). This data is
 * displayed in the "image-navigation" through buttons, so its not related to the
 * breadcrumb or anything else. Nevertheless, the URL build in this model MUST POINT
 * TO VALID/EXISTING targets.
 *  
 */
class seleccionModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Retrieve other models from DDBB.
     * 
     * @return mixed An array with the parameters for each vehicle model. */
    public function get_othermodels() {
        $result = null;
        $sql = "SELECT DISTINCT c.car_name AS `name`,c.car_key AS `key`,
            c.car_mod AS `mod`,c.car_special_offer_prefix AS `year`,
            c.car_pkcar AS `pk_car`
        FROM cars c WHERE c.car_dag IS NOT NULL
            AND c.car_pkcar NOT IN " . $_SESSION['mrkt_ign_models'] . "
            ORDER BY c.car_strec ASC";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->assign_othermodels($result->rows);
    }

    /** To format img,URL and link-type(controller)for each element.
     * 
     * @param mixed $results An array with the RAW data comming from DDBB. */
    private function assign_othermodels(&$results) {
        foreach ($results as $key => $value) {
            $pk_car = $value['pk_car'];
            $results[$key]['url'] = BASE_URL . "model/model/$pk_car";
            $results[$key]['img'] = IMG_REPO . "coches/$pk_car.png";
        }
        return $results;
    }

    //[OLD] hc_: hard-coded
    public function get_hc_currentmodels() {   //array with current vehicle models so data is not inside the view.
        $ctlr = 'model/model/'; //controller
        $img = IMG_REPO . 'coches/';
        //value-array: 0->car-name,1->key,2->re_direct-url,3->img_path

        if (constant('_COUNTRY') == 'mx') {
            return $currentmodels = array(
                array("Arona", " - KJ7", BASE_URL . $ctlr . '45', $img . '45_home.png'),
                array("Nuevo Ibiza", " - KJ1", BASE_URL . $ctlr . '44', $img . '44.png'),
                array("Leon 5P PA", " - 5F1", BASE_URL . $ctlr . '41', $img . '41.png'),
                array("Leon SC PA", " - 5F5", BASE_URL . $ctlr . '42', $img . '42.png'),
                array("Leon ST PA", " - 5F8", BASE_URL . $ctlr . '43', $img . '43.png'),
                array("Toledo", " - KG3", BASE_URL . $ctlr . '33', $img . '33.png'),
                array("Ateca", " - KH7", BASE_URL . $ctlr . '40', $img . '40.png'),
                array("Alhambra GP", " - 711", BASE_URL . $ctlr . '39', $img . '39.png')
            );
        } else {
            return $currentmodels = array(
                array("Arona", " - KJ7", BASE_URL . $ctlr . '45', $img . '45_home.png'),
                array("Mii", " - KF12", BASE_URL . $ctlr . '26', $img . '26.png'),
                array("Mii 5P", " - KF13", BASE_URL . $ctlr . '27', $img . '27.png'),
                array("Nuevo Ibiza", " - KJ1", BASE_URL . $ctlr . '44', $img . '44.png'),
                array("Toledo", " - KG3", BASE_URL . $ctlr . '33', $img . '33.png'),
                array("Leon 5P PA", " - 5F1", BASE_URL . $ctlr . '41', $img . '41.png'),
                array("Leon SC PA", " - 5F5", BASE_URL . $ctlr . '42', $img . '42.png'),
                array("Leon ST PA", " - 5F8", BASE_URL . $ctlr . '43', $img . '43.png'),
                array("X-PERIENCE", " - 5F88", BASE_URL . $ctlr . '35', $img . '35.png'),
                array("Ateca", " - KH7", BASE_URL . $ctlr . '40', $img . '40.png'),
                array("Alhambra GP", " - 711", BASE_URL . $ctlr . '39', $img . '39.png')
            );
        }
    }

    //hc_: hard-coded
    /*public function get_hc_othermodels() {   //array with current vehicle models so data is not inside the view.
        $ctlr = 'model/model/'; //controller
        $img = IMG_REPO . 'coches/';
        //value-array: 0->car-name,1->key,2->years,3->re_direct-url,4->img_path
        return $currentmodels = array(
            array("Ibiza SC", " - 6P5", "2015-2017", BASE_URL . $ctlr . '36', $img . '36.png'),
            array("Ibiza 5P", " - 6P1", "2015-2017", BASE_URL . $ctlr . '37', $img . '37.png'),
            array("Ibiza ST", " - 6P8", "2015-2017", BASE_URL . $ctlr . '38', $img . '38.png'),
            array("León ST", " - 5F8", "2013-2016", BASE_URL . $ctlr . '34', $img . '34.png'),
            array("León 5P", " - 5F1", "2013-2016", BASE_URL . $ctlr . '32', $img . '32.png'),
            array("León SC", " - 5F5", "2013-2016", BASE_URL . $ctlr . '31', $img . '31.png'),
            array("Alhambra NF", " - 710", "2010-2015", BASE_URL . $ctlr . '25', $img . '25.png'),
            array("Ibiza SC", " - 6J1", "2012-2015", BASE_URL . $ctlr . '28', $img . '28.png'),
            array("Ibiza 5P", " - 6J5", "2012-2015", BASE_URL . $ctlr . '29', $img . '29.png'),
            array("Ibiza ST", " - 6J8", "2012-2015", BASE_URL . $ctlr . '30', $img . '30.png'),
            array("Ibiza SC", " - 6J1", "2009-2012", BASE_URL . $ctlr . '16', $img . '16.png'),
            array("Ibiza 5P", " - 6J5", "2009-2012", BASE_URL . $ctlr . '17', $img . '17.png'),
            array("Ibiza ST", " - 6J8", "2009-2012", BASE_URL . $ctlr . '24', $img . '24.png'),
            array("Ibiza 02", " - 6L1", "2002-2009", BASE_URL . $ctlr . '5', $img . '5.png'),
            array("Ibiza", " - 6K1", "1993-2002", BASE_URL . $ctlr . '4', $img . '4.png'),
            array("León", " - 1P1", "2008-2012", BASE_URL . $ctlr . '19', $img . '19.png'),
            array("León 05", " - 1P1", "2005-2009", BASE_URL . $ctlr . '13', $img . '13.png'),
            array("León", " - 1M1", "2000-2005", BASE_URL . $ctlr . '7', $img . '7.png'),
            array("Altea", " - 5P1", "2009-2014", BASE_URL . $ctlr . '20', $img . '20.png'),
            array("Altea XL", " - 5P5", "2009-2014", BASE_URL . $ctlr . '21', $img . '21.png'),
            array("Freetrack", " - 5P8", "2009-2014", BASE_URL . $ctlr . '23', $img . '23.png'),
            array("Altea", " - 5P1", "2004-2009", BASE_URL . $ctlr . '11', $img . '11.png'),
            array("Altea XL", " - 5P5", "2007-2009", BASE_URL . $ctlr . '14', $img . '14.png'),
            array("Freetrack", " - 5P8", "2007-2009", BASE_URL . $ctlr . '15', $img . '15.png'),
            array("Exeo", " - 3R2", "2009-2013", BASE_URL . $ctlr . '18', $img . '18.png'),
            array("Exeo ST", " - 3R5", "2009-2013", BASE_URL . $ctlr . '22', $img . '22.png'),
            array("Alhambra", " - 7V9", "1996-2010", BASE_URL . $ctlr . '1', $img . '1.png'),
            array("Toledo", " - 5P2", "2005-2009", BASE_URL . $ctlr . '12', $img . '12.png'),
            array("Toledo", " - 1M2", "1999-2005", BASE_URL . $ctlr . '8', $img . '8.png'),
            array("Córdoba", " - 6L2", "2003-2009", BASE_URL . $ctlr . '10', $img . '10.png'),
            array("Córdoba", " - 6K2", "1994-2002", BASE_URL . $ctlr . '3', $img . '3.png'),
            array("Vario", " - 6K5", "1999-2003", BASE_URL . $ctlr . '9', $img . '9.png'),
            array("Inca", " - 6K9", "1996-2004", BASE_URL . $ctlr . '6', $img . '6.png'),
            array("Arosa", " - 6H1", "1997-2004", BASE_URL . $ctlr . '2', $img . '2.png')
        );
    }*/

}
