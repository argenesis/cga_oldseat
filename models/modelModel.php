<?php

/**
 * Class modelModel | models/modelModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * MODEL - MODEL: handles names for vehicle model related queries.
 * The vehicle model's name has no translation.
 *  
 */
class modelModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getname($pk_car, $pk_lng) {
        $result = null;
        $sql_ES = "SELECT car_name FROM cars WHERE car_pkcar = $pk_car AND car_pkcar NOT IN " . $_SESSION['mrkt_ign_models'] . ";";
        $sql_lng = $sql_ES; //model name have no translation

        if ($pk_lng == 5) {
            $sql = $sql_ES;
        } else {
            $sql = $sql_lng;
        }
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows[0];
    }

}
