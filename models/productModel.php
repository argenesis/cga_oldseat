<?php

/**
 * Class productModel | models/productModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * PRODUCT: function 'prd_info' retrieves product data. 
 * All of the product fields use the 'prd_' prefix, except the 'pk_prd'.
 *  
 */
class productModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    //use_case: 1SL061129A(doesnt have a 0 model img)
    public function prd_info($pk_car, $pk_lng, $pk_prd) {
        $current_country = constant('_COUNTRY');
        $result = null;
        $sql = "SELECT prd_pk, prd_sku, prd_name, prd_notes, prd_img, prd_ut, prd_price, prd_new, pk_lng, LOWER(lang_tag) AS `lang_tag`
                FROM (
                SELECT DISTINCT 
                prt_pkcategory AS `ctg_prd`,
                prd_pkproduct AS `prd_pk`,
                prd_sku AS `prd_sku`,
                prodtrans_name AS `prd_name`,
                prodtrans_notes AS `prd_notes`,
                IF(d1.dtl_image IS NOT NULL, d1.dtl_image,(IF(d2.dtl_image IS NOT NULL, d2.dtl_image, '00.jpg'))) AS `prd_img`,
                prd_install AS `prd_ut`,
                IF('".$current_country."' = 'es', prd_price, prc_price) AS `prd_price`,
                prd_new,
                l1.langs_pk AS `pk_lng`,
                l1.langs_tag AS `lang_tag`
                FROM products 
                LEFT JOIN prodtrans ON prd_pkproduct = prodtrans_pkproduct
                LEFT JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcar = $pk_car
                LEFT JOIN details d1 ON d1.dtl_pkproduct = prd_pkproduct AND (d1.dtl_car = $pk_car )
                LEFT JOIN details d2 ON d2.dtl_pkproduct = prd_pkproduct AND (d2.dtl_car =  0)  
                LEFT JOIN langs l2 ON l2.langs_tag = UPPER('".$current_country."') 
                LEFT JOIN prices ON prc_pkproduct = prd_pkproduct AND prc_country = l2.langs_pk
                LEFT JOIN langs l1 ON l1.langs_pk = prodtrans_lang
                WHERE prd_pkproduct = $pk_prd 
                UNION ALL 
                SELECT DISTINCT 
                prt_pkcategory AS `ctg_prd`,
                prd_pkproduct AS `prd_pk`,
                prd_sku AS `prd_sku`,
                prd_name AS `prd_name`,
                prd_notes AS `prd_notes`,
                IF(d1.dtl_image IS NOT NULL, d1.dtl_image,(IF(d2.dtl_image IS NOT NULL, d2.dtl_image, '00.jpg'))) AS `prd_img`,
                prd_install AS `prd_ut`,
                IF('".$current_country."' = 'es', prd_price, prc_price) AS `prd_price`,
                prd_new,
                5 AS `pk_lng`,
                'ES' AS `lang_tag`
                FROM products
                LEFT JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcar = $pk_car
                LEFT JOIN details d1 ON d1.dtl_pkproduct = prd_pkproduct AND (d1.dtl_car = $pk_car)
                LEFT JOIN details d2 ON d2.dtl_pkproduct = prd_pkproduct AND (d2.dtl_car = 0)
                LEFT JOIN langs ON langs_tag = UPPER('".$current_country."') 
                LEFT JOIN prices ON prc_pkproduct = prd_pkproduct AND prc_country = langs_pk
                WHERE prd_pkproduct = $pk_prd
                ) 
                results ORDER BY pk_lng;" ;    
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->assign_type($result->rows);
    }
    /** Asignar formato de datos; construye un array asociativo para cada 'lang_tag'
     * que se usa como indice.
     * 
     * @param mixed $results Datos de producto; contiene en cada registro su 'lang_tag'. 
     * @return mixed Array asociativo; el 'tag' de idioma es la 'key'.*/
    public function assign_type($results) {
        $update= array();        
        foreach ($results as $value) {
            $_tag= $value['lang_tag'];
            $img = $value['prd_img'];
            $value['prd_img'] = IMG_REPO . "imgs/$img";
            $value['error'] = IMG_REPO . "imgs/00.jpg";
            if(!(array_key_exists($_tag, $update))){                
                $update[$_tag]= $value;
            }            
        }        
        return $update;
    }
    
    /** Cross-selling revisado: la query filtra los productos segun si aplican al mercado
     * en activo.
     * 
     * @author Ricard Beyloc <ricard@snconsultors.es>.*/    
    public function getCrossselling($pk_car, $pk_prd, $pk_lng) {
        $rel_type = 102; // as in 'relations' table
        $markets1 = str_replace('prd_', 'p1.prd_', $_SESSION['mrkt_prds']); //sustitución en el string de mercados para añadir alias de la tabla products p1 antes de formar la query
        $markets2 = str_replace('prd_', 'p2.prd_', $_SESSION['mrkt_prds']); //sustitución en el string de mercados para añadir alias de la tabla products p2 antes de formar la query
        /*$sql = "call GetCardRelations($pk_car,$pk_prd,$pk_lng,$rel_type);";*/ //anteriormente usaba procedure
        $sql = "SELECT DISTINCT 
                p2.prd_sku as sku,
                r1.rel_pkrelproduct as PKrelproduct,
                IF ($pk_lng = 5, p2.prd_name, prodtrans_name) as nameRelation,
                IF(d1.dtl_image IS NOT NULL, d1.dtl_image, IF(d2.dtl_image IS NOT NULL, d2.dtl_image, '00.jpg')) as image_rel
                FROM relations r1 
                JOIN products p1 ON p1.prd_pkproduct = r1.rel_pkproduct AND $markets1 AND p1.prd_visible != 0 AND p1.prd_baja = 0 AND p1.prd_pkproduct = $pk_prd 
                JOIN products p2 ON p2.prd_pkproduct = r1.rel_pkrelproduct AND $markets2 AND p2.prd_visible != 0 AND p2.prd_baja = 0
                JOIN parents pr1 ON pr1.prt_pkproduct = p1.prd_pkproduct AND pr1.prt_pkcar = $pk_car
                JOIN parents pr2 ON pr2.prt_pkproduct = p2.prd_pkproduct AND pr2.prt_pkcar = $pk_car
                LEFT JOIN details d1 ON d1.dtl_pkproduct = r1.rel_pkrelproduct AND d1.dtl_car = $pk_car
                LEFT JOIN details d2 ON d2.dtl_pkproduct = r1.rel_pkrelproduct AND d2.dtl_car = 0
                LEFT JOIN prodtrans ON prodtrans_pkproduct = r1.rel_pkrelproduct AND prodtrans_lang = $pk_lng
                WHERE r1.rel_type = $rel_type
                ORDER BY r1.rel_order, p2.prd_name;"
        ;
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return ($result->rows);
    }

    public function cross_urlBuilder($results) { 
        foreach ($results as &$val) {
            $val['crs_sku'] = $val['sku']; //copy in new key
            unset($val['sku']); //unset old key
            $val['crs_rel'] = $val['PKrelproduct'];
            unset($val['PKrelproduct']);
            $val['crs_name'] = $val['nameRelation'];
            unset($val['nameRelation']);
            $val['crs_img'] = IMG_REPO . "imgs/" . $val['image_rel']; //(19/06/17)IMG_REPO."imgs/petites/". $val['image_rel'];
            unset($val['image_rel']);
            $val['url'] = BASE_URL . "searchresults/searchresults/" . $val['crs_sku'];
        }
        return $results;
    }
}
