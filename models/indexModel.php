<?php

/**
 * Class indexModel | models/indexModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * INDEX - contains hard-coded data for the landing HOME page.
 * 
 * This class was build to keep consistency in the MVC arquitecture, so it contains
 * hardcoded data in its methods(indicated with names containing "_hc_") in order
 * to keep the data OUTSIDE THE VIEW. Nevertheless,the URL build in this model 
 * MUST POINT TO VALID/EXISTING targets.
 *  
 */
class indexModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Hard-coded data for index landing HOME page. 
     * 
     * @return mixed An array with the parameters for each component. */
    public function get_hc_indexdata() {
        $img = IMG_REPO . 'beauty/';
        //value-array: 0->home-txt, 1->URL, 2->img_path, 3->alt
        return $hc_indexdata = array(
            array("home" => constant('Home1'), "url" => BASE_URL . 'collection/collectionselector', "img" => $img . 'home_collection.jpg', "alt" => constant('Home1alt')),
            array("home" => constant('Home2'), "url" => BASE_URL . 'seleccion/seleccion', "img" => $img . 'home_coches.jpg', "alt" => constant('Home2alt'))
        );
    }

}
