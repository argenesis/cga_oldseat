<?php

/**
 * Class mapModel | models/mapModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * MAP: handles map related queries.
 *  
 */
class mapModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Perform search under radius criteria.
     *   
     * @param double $radius The radius value.
     * @param double $latitude Latitude value(reference point).
     * @param double $longitude Longitude value(reference point). */
    public function searchby_radius($radius, $latitude, $longitude) {
        $result = null;        
        $current_country = constant('_COUNTRY');        
        $sql = "SELECT DISTINCT dlr_id, dlr_seat_id, dlr_name, dlr_address, dlr_lat, dlr_lng, dlr_email
                FROM dealer
                WHERE dlr_country_tag = UPPER('".$current_country."')
                AND (((dlr_lat <= ($latitude+$radius)) AND (dlr_lat >= ($latitude-$radius))) AND ((dlr_lng <= ($longitude+$radius)) AND (dlr_lng >= ($longitude-$radius))))
                AND dlr_email != '' AND dlr_email IS NOT NULL
                ORDER BY dlr_seat_id, dlr_id ASC;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->makeGeoJSON($result->rows);
    }

    /** Representational array to later build of geoJson structure as string.
     * "geometry" is a json object type with geoJSON notation; the "Point" type
     * takes 2 cordinates(lat/lng) but geoJSON implements the WGS84 datum system,
     * with longitude and latitude units of decimal degrees. The values for latitude
     * and longitude are, therefore, swapped.
     * 
     * @param mixed $results An array containing the results from the query.
     * @return mixed $str_geojson contains ALL DEALERS returned in the result with
     * the corresponding format to later output as json_object. */
    private function makeGeoJSON($results) {
        $str_geojson = array();
        foreach ($results as $value) {
            $dealer_onmap = array(
                "type" => "Feature",
                "id" => $value['dlr_seat_id'],
                "geometry" => array("type" => "Point", "coordinates" => array((double) $value['dlr_lng'], (double) $value['dlr_lat'])),
                "properties" => array("name" => $value['dlr_name'],
                    "address" => $value['dlr_address'],
                    "email" => $value['dlr_email']
                )
            );
            $str_geojson[] = $dealer_onmap;
        }
        return $str_geojson;
    }

}
