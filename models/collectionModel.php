<?php

/**
 * Class categoryModel | models/collectionModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * COLLECTION - handles the STRUCTURE for collection categories in the COLLECTION section.
 * 
 * This class builds data for two outputs: the "images-navigation", and the BREADCRUMB.
 * All the methods/functions requiring TEXT in the return value contain two SQL strings;
 * this is because in the database the translations for the products and categories texts
 * and everything related DOESN'T have the spanish value, because the values for this are
 * included in the same table(col_products/col_categories).
 * 
 * @todo Make ALL METHODS return whether array type or object type(->rows).
 * @todo Refactor URL assignment function names (ctg,prd)...
 * 
 */
class collectionModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    /** Main Collections (ctg_level=1).
     * "ctg_pkcategory<>100004" is explicitly set to be excluded (23/5/2017).
     * 
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function collection_selector($pk_lng) {
        $result = null;
        $sql = "SELECT ctg_pkcategory AS `pk_coll`,
                IF( $pk_lng != 5, cattrans_name, ctg_name)  AS `name`
                FROM col_categories 
                LEFT JOIN  col_cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng
                WHERE ctg_level = 1 AND ctg_pkcategory <> 100004
                ORDER BY ctg_order ASC;";

        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->coll_selector_url($result->rows);
    }

    /** Values for CSS property. The property is later edited using jQuery in "jumbotron_manager.js".
     * 
     * @param int $pk_coll Collection/category unique identifier in DDBB. */
    public function jumbo($pk_coll) {
        switch ($pk_coll) {
            case 100001: return IMG_REPO . 'beauty/seat_col_ateca.jpg';
            case 100002: return IMG_REPO . 'beauty/seat_col_essentials.jpg';
            case 100003: return IMG_REPO . 'beauty/seat_col_motorsport.jpg';
            case 100005: return IMG_REPO . 'beauty/seat_col_mediterranean.jpg';
        }
    }

    /** Check if there are any child collections/categories.
     * 
     * @param int $pk_coll Collection/category ddbb unique identifier(ID). */
    public function collection_haschild($pk_coll) {
        $result = null;
        $sql = "SELECT hrc_child as 'haschild' FROM col_hierarchy WHERE hrc_parent = $pk_coll;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result;
    }

    /** Check if the argument "pk_coll" is of type list.
     * 
     * @param int $pk_coll Collection/category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function is_list($pk_coll, $pk_lng) {
        $list = array();
        $result = null;
        $sql = "SELECT DISTINCT ctg_is_list AS `is_list`,
                IF( $pk_lng != 5, cattrans_name, ctg_name)  AS `ctg_name`,
                cdt_image AS `ctg_img`
                FROM col_catdetails
                LEFT JOIN col_categories ON cdt_cat = ctg_pkcategory  
                LEFT JOIN col_cattrans ON cdt_cat = cattrans_pkcategory AND cattrans_lang = $pk_lng
                LEFT JOIN col_hierarchy ON hrc_child = cdt_cat
                WHERE cdt_cat = $pk_coll
                ORDER BY hrc_order, hrc_child ASC;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        $is_list = $result->rows[0]['is_list'];
        if ($is_list == 1) {
            $list['is_list'] = 'true';
            $img = $result->rows[0]['ctg_img'];
            $list['img'] = IMG_REPO . "col-prods/cats-col/$img";
            $list['name'] = $result->rows[0]['ctg_name'];
            return $list;
        } else {
            return $list;
        }
    }

    /** Only for TRANSITIONAL collection/categories.
     * 
     * @param int $pk_lng Language ddbb unique identifier(ID).
     * @param int $pk_coll Collection/category ddbb unique identifier(ID). */
    public function coll_manager($pk_lng, $pk_coll) {
        $result = null;
        $sql = "SELECT DISTINCT hrc_child as `pk_ctg`,
                    IF($pk_lng = 5, ctg_name, cattrans_name) AS `name`, 
                    ctg_is_list as `is_list`,
                    cdt_image as `img`   
                    FROM col_hierarchy
                    JOIN col_categories ON hrc_child = ctg_pkcategory
                    LEFT JOIN col_cattrans ON hrc_child= cattrans_pkcategory AND cattrans_lang = $pk_lng 
                    LEFT JOIN col_catdetails cd ON hrc_child = cdt_cat
                    WHERE hrc_parent = $pk_coll
                    ORDER BY hrc_order ASC";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->coll_assigntype($result->rows);
    }

    /** Specific to COLLECTION, but implementing existing layouts.
     * It loads the previous collections BEFORE reaching a product.
     * 
     * @param int $pk_lng Language ddbb unique identifier(ID).
     * @param int $pk_coll Collection/category ddbb unique identifier(ID). */
    public function sidebar_manager($pk_lng, $pk_coll) {
        $result = null;
        $sql = "    SELECT hrc_child AS `ctg_pkcategory`,
                    IF($pk_lng = 5, ctg_name, cattrans_name) AS `ctg_name`,
                    ctg_is_list as `is_list` 
                    FROM col_hierarchy
                    JOIN col_categories ON ctg_pkcategory = hrc_child
                    LEFT JOIN col_cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng 
                    LEFT JOIN col_catdetails ON cdt_cat = hrc_child 
                    WHERE hrc_parent = $pk_coll 
                    ORDER BY hrc_order;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $this->sidebar_url($result->rows);
    }

    /** Products in the collection(final-category). 
     * $_SESSION['colmrkt_prds'] is defined in "config_xx.php".
     * There is a hard-coded condition in the SQL, which is the following:
     * "(colprd.prd_baja= 0 AND colprd.prd_visible > 0)".
     * The former is included because the situation can be that of a product existing
     * but its not meant to be seen on the catalogue.
     * Prices for PT(6) are taken from 'col_prices' table.
     * 
     * @param int $pk_lng Language ddbb unique identifier(ID).
     * @param int $pk_coll Collection/category ddbb unique identifier(ID). */
    public function get_ctg_prds($pk_coll, $pk_lng) {
        $result = null;
        $current_country = constant('_COUNTRY');
        $sql = "SELECT DISTINCT 
                    prd_pkproduct AS `pk_prd`,
                    prd_sku AS `prd_sku`,
                    IF('$pk_lng'= 5, prd_size, prodtrans_size) AS `prd_size`, 
                    IF('$pk_lng'= 5, prd_color, prodtrans_color) AS `prd_color`, 
                    IF('$pk_lng'= 5, prd_name, prodtrans_name) AS `prd_name`, 
                    IF('$pk_lng'= 5, prd_notes, prodtrans_notes) AS `prd_notes`,
                    IF(dtl_image IS NOT NULL, dtl_image, '00.jpg') AS `prd_img`,
                    IF('$current_country'='es', prd_price, (IF(prc_price IS NOT NULL, prc_price, NULL))) as `prd_price`, 
                    prt_pkcategory AS `pk_ctgfinal`
                    FROM col_products
                    JOIN col_parents ON prd_pkproduct = prt_pkproduct AND prt_pkcategory = $pk_coll
                    LEFT JOIN col_prodtrans ON prd_pkproduct = prodtrans_pkproduct AND prodtrans_lang = $pk_lng
                    LEFT JOIN langs ON LOWER(langs_tag) = '$current_country'
                    LEFT JOIN col_prices ON prd_pkproduct = prc_pkproduct AND prc_country = langs_pk
                    LEFT JOIN col_details ON prd_pkproduct = dtl_pkproduct
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] .";";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }

    /** To format img,URL and link-type(controller)for each element of collection-product.
     * 
     * @param mixed $results Array containing RAW results from ddbb.
     * @todo Make method assign a "default" error value (like "00.jpg"). */
    public function assign_type(&$results) {
        foreach ($results as $key => $value) {
            $ctgfinal = $value['pk_ctgfinal'];
            $pkprd = $value['pk_prd'];
            $img = $value['prd_img'];
            $results[$key]['prd_img'] = IMG_REPO . "col-prods/$img";
            $results[$key]['error'] = IMG_REPO . "imgs/cats/00.jpg";
            $results[$key]['url'] = BASE_URL . "collectionprd/collectionprd/$ctgfinal/$pkprd";
        }
        return $results;
    }

    /** To format img,URL and link-type(controller)for each collection IN THE SIDEBAR.
     * 
     * @param mixed $results Array containing RAW results from ddbb.
     * @todo Make method assign a "default" error value (like "00.jpg"). */
    public function sidebar_url(&$results) {
        foreach ($results as $key => $value) {
            $pk_coll = $value['ctg_pkcategory'];
            //check if ctg is ctg/list/prds
            if ($value['is_list'] == 0) {
                //check if has child_ctg
                $ctg_res = $this->collection_haschild($pk_coll);
                if (sizeof($ctg_res->rows) != 0) {
                    $results[$key]['url'] = BASE_URL . "collection/collctg/$pk_coll";
                } elseif (sizeof($ctg_res->rows) == 0) {
                    $results[$key]['url'] = BASE_URL . "collection/collctg_prds/$pk_coll";
                }
            } elseif ($value['is_list'] == 1) {
                $results[$key]['url'] = BASE_URL . "collection/collctg_list/$pk_coll";
            }
        }
        return $results;
    }

    /** To format img,URL and link-type(controller)for each element of a MAIN Collection.
     * 
     * @param mixed $collections Array containing RAW results from ddbb.
     * @todo Make method assign a "default" error value (like "00.jpg"). */
    public function coll_selector_url(&$collections) {
        foreach ($collections as $key => $value) {
            $pk_coll = $value['pk_coll'];
            $name = $value['name'];
            $collections[$key]['name_uppercase'] = strtoupper($name);
            $collections[$key]['url'] = BASE_URL . "collection/collctg/$pk_coll";
            //Main Collection img_selector...
            if ($pk_coll == 100001) {
                $collections[$key]['img'] = IMG_REPO . "beauty/seat_cat_ateca.jpg";
                $collections[$key]['alt'] = constant('Col1alt');
            } elseif ($pk_coll == 100002) {
                $collections[$key]['img'] = IMG_REPO . "beauty/seat_cat_essentials.jpg";
                $collections[$key]['alt'] = constant('Col2alt');
            } elseif ($pk_coll == 100003) {
                $collections[$key]['img'] = IMG_REPO . "beauty/seat_cat_motorsport.jpg";
                $collections[$key]['alt'] = constant('Col3alt');
            } elseif ($pk_coll == 100005) {
                $collections[$key]['img'] = IMG_REPO . "beauty/seat_cat_mediterranean.jpg";
                $collections[$key]['alt'] = constant('Col4alt');
            }
        }
        return $collections;
    }

    /** To format img,URL and link-type(controller)for each element of a collection.
     * 
     * @param mixed $collections Array containing RAW results from ddbb.
     * @todo Make method assign a "default" error value (like "00.jpg"). */
    public function coll_assigntype(&$collections) {
        foreach ($collections as $key => $value) {
            $pk_coll = $value['pk_ctg'];
            $img = $value['img'];
            $collections[$key]['img'] = IMG_REPO . "col-prods/cats-col/$img";
            $collections[$key]['error'] = IMG_REPO . "imgs/cats/00.jpg";

            if ($value['is_list'] == 0) {
                //check if has child_ctg
                $ctg_res = $this->collection_haschild($pk_coll);
                if (sizeof($ctg_res->rows) != 0) {
                    $collections[$key]['url'] = BASE_URL . "collection/collctg/$pk_coll";
                } elseif (sizeof($ctg_res->rows) == 0) {
                    $collections[$key]['url'] = BASE_URL . "collection/collctg_prds/$pk_coll";
                }
            } elseif ($value['is_list'] == 1) {
                $collections[$key]['url'] = BASE_URL . "collection/collctg_list/$pk_coll";
            }
        }
        return $collections;
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * The builder starts from the CURRENT COLL/CTG; so it actually loops its way
     * UP towards a MAIN COLLECTION/CATEGORY.
     * 
     * @param int $pk_coll Collection/category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID). */
    public function bc_builder($pk_coll, $pk_lng) {
        $_pk_ctg = $pk_coll;
        $breadcrumbs_structure = array();
        $looper = true;

        while (!$looper == false) {
            $has_parent = $this->bc_getnames($_pk_ctg, $pk_lng, 1); //1:name of parent-for-ctg
            if (sizeof($has_parent) < 1) {
                $val = $this->bc_getnames($_pk_ctg, $pk_lng, 2); //2:main ctg
                array_push($breadcrumbs_structure, $val[0]);
                $looper = false;
            } else {
                $i = 0;
                $val = $this->bc_getnames($_pk_ctg, $pk_lng, 0); //0:ctg name
                array_push($breadcrumbs_structure, $val[0]);
                $_pk_ctg = $has_parent[$i]['bc_ctg']; //change the ctg to 'parent'
                $i++;
            }
        }
        $bc = array_reverse($breadcrumbs_structure); //because we build it from 'last' to 'first' ctg
        return $this->bc_assigntype($bc);
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * Both variables are arrays corresponding to each required query.
     * 
     * @param int $pk_coll Collection/category ddbb unique identifier(ID).
     * @param int $pk_lng Language ddbb unique identifier(ID).
     * @param int $type The request type to select the query string[0:ctg name; 1:name of ctg->parent; 2:mainctg for ctg].
     * @return array Key=>pair values; it contains the pk_ctg(int), pk_name(string) and isList(1/0:true/false). */
    public function bc_getnames($pk_coll, $pk_lng, $type) {
        $result = null;
        $sql = array(
            "   SELECT ctg_pkcategory AS `bc_ctg`,
                IF( $pk_lng != 5, cattrans_name, ctg_name) AS `bc_ctg_name`, 
		ctg_is_list AS `bc_ctg_is_list` 
                FROM col_hierarchy
                JOIN col_categories ON ctg_pkcategory = hrc_child
                LEFT JOIN col_cattrans ON  ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng 
                LEFT JOIN col_catdetails ON cdt_cat = hrc_child
                WHERE hrc_child = $pk_coll ;",
            "   SELECT ctg_pkcategory AS `bc_ctg`,
                IF( $pk_lng != 5, cattrans_name, ctg_name) AS `bc_ctg_name`, 
                ctg_is_list AS `bc_ctg_is_list`
                FROM col_hierarchy
                JOIN col_categories ON ctg_pkcategory = hrc_parent
                LEFT JOIN col_cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng
                LEFT JOIN col_catdetails ON cdt_cat = hrc_parent
                WHERE hrc_child = $pk_coll ;",
            "   SELECT ctg_pkcategory as `bc_ctg`,
		IF( $pk_lng != 5, cattrans_name, ctg_name) AS `bc_ctg_name`, 
		'0' as `bc_ctg_is_list` 
		FROM col_categories
                LEFT JOIN col_cattrans ON ctg_pkcategory = cattrans_pkcategory AND cattrans_lang = $pk_lng
                WHERE ctg_pkcategory = $pk_coll ;"
        );
        $result_query = $this->_db->query($sql[$type]);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }

    /** Prefix 'bc_': breadcrumbs related functions.
     * To format URL and link-type(controller)for each element of breadcrumb.
     *         
     * @param mixed $results Array containing RAW results from ddbb. */
    public function bc_assigntype(&$results) {
        foreach ($results as $key => $value) {
            $rel = $value['bc_ctg'];
            if ($value['bc_ctg_is_list'] == 0) {
                //has child_ctgcollection_haschild($pk_coll)
                $ctg_res = $this->collection_haschild($value['bc_ctg']);
                if (sizeof($ctg_res->rows) != 0) {
                    $results[$key]['bc_url'] = BASE_URL . "collection/collctg/$rel";
                } elseif (sizeof($ctg_res->rows) == 0) {
                    $results[$key]['bc_url'] = BASE_URL . "collection/collctg_prds/$rel";
                }
            } elseif ($value['bc_ctg_is_list'] == 1) {
                $results[$key]['bc_url'] = BASE_URL . "collection/collctg_list/$rel";
            }
        }
        return $results;
    }

}
