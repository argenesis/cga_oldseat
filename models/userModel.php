<?php

/**
 * Class userModel | models/userModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     ?
 */

/**
 * USER - description goes here.[Note: inherited from OWA].
 * 
 * This class handles: user state(logged in, catalogue_mode); role of user(authorization 
 * level[read-only][editor-mode]). 
 * 
 */
class userModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function isValid($user, $pass) {   //s_:sanitized
        $s_user = $this->_db->real_escape_string($user);
        $s_pass = $this->_db->real_escape_string($pass);

        $sql = "SELECT IF(COUNT(nombre)>0,'true','false') as `valid` 
                FROM user WHERE nombre='$s_user' AND pwd='$s_pass';";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        // Results exists= true; assign values and return 'true'
        if ($result->rows[0]['valid'] === 'false')
            return false;
        if ($result->rows[0]['valid'] === 'true')
            return true;
    }

    public function getUserData($user, $pass, &$data) {   //s_:sanitized
        $s_user = $this->_db->real_escape_string($user);
        $s_pass = $this->_db->real_escape_string($pass);

        $sql = "SELECT nombre as `usr_name`,rol as `usr_role` FROM user
                WHERE nombre='$s_user' AND pwd='$s_pass';";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows[0];
    }

}
