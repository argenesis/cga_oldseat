<?php

/**
 * Class searchresultsModel | models/searchresultsModel.php
 *
 * @package     models
 * @author      Argenis Urribarri<argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * SEARCHRESULTS: function 'searchresults_data' triggers the functions
 * needed to search in the DDBB. 
 * 
 * The first search performed on the first SQL query
 * check if the SKU exists on a certain order. Dating 18/5/2017 the pk_lng=mrkt.
 * Returns 'models' or 'collection'(true).
 * The first query in 'searchresults_data' filters the market valid products and models.
 * All the other services rely on this fact.
 * 
 * @todo Separate concerns to set in corresponding abstraction layer.
 *  
 */
class searchresultsModel extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function autocomplete_search($term) {
        $current_lang = $_SESSION['lang_tag'];
        $sql = "SELECT DISTINCT
                prd_sku,
                prd_name
                FROM (
                SELECT DISTINCT
                prd_sku,
                IF(prodtrans_name IS NOT NULL, prodtrans_name, prd_name) AS `prd_name`
                FROM products
                JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ."
                LEFT JOIN langs ON LOWER(langs_tag) = '".$current_lang."'
                LEFT JOIN prodtrans ON prd_pkproduct = prodtrans_pkproduct AND prodtrans_lang = langs_pk
                WHERE (prd_sku LIKE '%".$term."%' OR prd_name LIKE '%".$term."%' OR prodtrans_name LIKE '%".$term."%') AND prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ."
                UNION
                SELECT DISTINCT
                prd_sku,
                IF(prodtrans_name IS NOT NULL, prodtrans_name, prd_name) AS `prd_name`
                FROM col_products
                JOIN col_parents ON prd_pkproduct = prt_pkproduct
                LEFT JOIN langs ON LOWER(langs_tag) = '".$current_lang."'
                LEFT JOIN col_prodtrans ON prd_pkproduct = prodtrans_pkproduct AND prodtrans_lang = langs_pk
                WHERE (prd_sku LIKE '%".$term."%' OR prd_name LIKE '%".$term."%' OR prodtrans_name LIKE '%".$term."%') AND prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ."
                ) AS autocomplete
                ORDER BY prd_sku DESC
                LIMIT 20;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result;
    }
        
    //If null for cat search on coll and set type (cat/coll)...
    public function fetch_pk($sku, $mrkt) {
        $sql = "SELECT 
                IFNULL(
                (   SELECT DISTINCT prd_pkproduct 
                    FROM products 
                    JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." 
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ." AND REPLACE(prd_sku, ' ', '') = REPLACE('$sku', ' ', '')
                ),( SELECT DISTINCT prd_pkproduct 
                    FROM col_products 
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ." AND REPLACE(prd_sku, ' ', '') = REPLACE('$sku', ' ', '')
                ))  AS `pk_prd`, 
                IFNULL(
                (   SELECT DISTINCT 'cat' AS `type` 
                    FROM products 
                    JOIN parents ON prd_pkproduct = prt_pkproduct AND prt_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." 
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ." AND REPLACE(prd_sku, ' ', '') = REPLACE('$sku', ' ', '')
                ),( SELECT DISTINCT 'coll' AS `type`
                    FROM col_products 
                    WHERE prd_visible != 0 AND prd_baja = 0 AND ". $_SESSION['mrkt_prds'] ." AND REPLACE(prd_sku, ' ', '') = REPLACE('$sku', ' ', '')
                ))  AS `type`;" ;
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }

    public function getfor_currentmodels($pk_prd) {   // Models for SKU (products already filtered)
        $sql = "SELECT DISTINCT 
                prt_pkcar as `pk_car`,
		prt_pkcategory as `pk_ctg`,
		prt_pkproduct as `pk_prd`,
		car_name as `name`,
                car_family as `fml`,
                car_mod as `mod`,
                car_key as `key` 
		FROM products
                JOIN parents ON prd_pkproduct = prt_pkproduct 
		JOIN cars ON car_pkcar = prt_pkcar AND car_dag IS NULL AND car_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." 
                WHERE prd_pkproduct = $pk_prd 
		ORDER BY car_strec ASC;";
        $result_query = $this->_db->query($sql);
        if ($result_query == 0) {
            return null;
        } else {
            $result->rows = $this->GetAllRecords($result_query);
            return $this->assigntype($result->rows);
        }
    }

    public function getfor_othermodels($pk_prd) {   // Other models for SKU (products already filtered)
        $sql = "SELECT DISTINCT 
                prt_pkcar AS `pk_car`,
                prt_pkcategory AS `pk_ctg`,
                prt_pkproduct AS `pk_prd`,
                car_name AS `name`,
                car_family AS `fml`,
                car_mod AS `mod`,
                car_key AS `key`,
                car_special_offer_prefix AS `year`
                FROM products 
                JOIN parents ON prd_pkproduct = prt_pkproduct
                JOIN cars ON car_pkcar = prt_pkcar AND car_dag IS NOT NULL AND car_pkcar NOT IN ". $_SESSION['mrkt_ign_models'] ." 
                WHERE prd_pkproduct = $pk_prd 
                ORDER BY car_strec ASC;";
        $result_query = $this->_db->query($sql);
        if ($result_query == 0) {
            return null;
        } else {
            $result->rows = $this->GetAllRecords($result_query);
            return $this->assigntype($result->rows);
        }
    }

    public function getfor_collection($pk_prd) {   // Only check if exists on Collection(model=100); assigned[img][url] here...(products already filtered)
        $sql = "SELECT prt_pkcategory as `pk_ctg`, prd_pkproduct as `pk_prd`
                FROM col_products
                JOIN col_parents ON prd_pkproduct= prt_pkproduct 
                WHERE prd_pkproduct = $pk_prd;";
        $result_query = $this->_db->query($sql);
        if ($result_query == 0) {
            return null;
        } else {
            $result->rows = $this->GetAllRecords($result_query);
            $result->rows[0]['img'] = $this->setimg_coll();
            $_pk_ctg = $result->rows[0]['pk_ctg'];
            $_pk_prd = $result->rows[0]['pk_prd'];
            $result->rows[0]['url'] = $this->seturl_coll($_pk_ctg, $_pk_prd);
            return $result->rows;
        }
    }

    // check if is_unassigned...
    public function is_unassigned($prd_pk) {   //SELECT the pk_prd that exists on the all_unassigned SELECT... (products already filtered)
        $sql = "SELECT prd_pkproduct as `is_unassigned` 
                FROM products 
                WHERE prd_pkproduct IN (
                    SELECT prd_pkproduct
                    FROM products
                    LEFT JOIN parents ON prd_pkproduct = prt_pkproduct
                    WHERE prt_pkproduct IS NULL AND (prd_baja <= 0 AND prd_visible > 0)
                    ORDER BY prd_pkproduct ASC
                ) 
                AND prd_pkproduct = $prd_pk;";
        $result_query = $this->_db->query($sql);
        $result->rows = $this->GetAllRecords($result_query);
        return $result->rows;
    }

    // check if is_collunassigned...
    public function is_collunassigned($pk_prd) {   //SELECT the pk_prd that exists on the all_unassigned SELECT... COLLECTION(products already filtered)
        $sql = "SELECT prd_pkproduct as `is_unassigned` 
                FROM col_products 
                WHERE prd_pkproduct IN (
                    SELECT prd_pkproduct
                    FROM col_products
                    LEFT JOIN col_parents ON prd_pkproduct = prt_pkproduct
                    WHERE prt_pkproduct IS NULL AND (prd_baja <= 0 AND prd_visible > 0)
                    ORDER BY prd_pkproduct ASC
                ) 
                AND prd_pkproduct = $prd_pk;";
        $result_query = $this->_db->query($sql);
        if ($result_query == 0) {
            return null;
        } else {
            $result->rows = $this->GetAllRecords($result_query);
            return $result->rows;
        }
    }

    // to add the img path for each item on results...
    public function assigntype($results) {
        $update = $results; //set INITIAL value        
        foreach ($update as $key => $value) {
            $car = $update[$key]['pk_car'];
            $ctg = $update[$key]['pk_ctg'];
            $prd = $update[$key]['pk_prd'];
            $update[$key]['img'] = $this->getimg_for($car); //path to...
            $update[$key]['url'] = BASE_URL . "product/product/$car/$ctg/$prd"; //url            
        }
        return $update;
    }

    // the path for img...
    public function getimg_for($pk_car) {
        return IMG_REPO . "coches/" . $pk_car . ".png";
    }

    // the path for img...
    public function setimg_coll() {
        return IMG_REPO . "coches/100.png";
    }

    // the url...
    public function seturl_coll($pk_ctg, $pk_prd) {
        return BASE_URL . "collectionprd/collectionprd/$pk_ctg/$pk_prd";
    }

}
