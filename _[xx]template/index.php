<?php 
/** @template Directorio plantilla; 'index.php' debe contener el nuevo pais según codigo ISO 639-1. */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', substr(realpath(dirname(__FILE__)), 0, -3) . DS);
define('APP_PATH', ROOT . 'app' . DS);
define('_COUNTRY', '');//country_tag
define('DEFAULT_LANG', '');//country_lang
include APP_PATH . 'Launcher.php';
?>