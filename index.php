<?php

//PRODUCTION ENVIRONMENT - Online Server
/**
 * Detección de códigos del navegador de idioma y estado para redirección
 */
$idiomas_navegador = explode(";", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
$codigos_ubicacion = explode("-", $idiomas_navegador[0]);
$codigo_idioma = $codigos_ubicacion[0];
$codigo_estado_idioma = explode(",", $codigos_ubicacion[1]);
$codigo_estado = $codigo_estado_idioma[0];
switch ($codigo_estado) {
    case "DE":
        $redirect_URL_country = "/de/";
        break;
    case "CS":
        $redirect_URL_country = "/cs/";
        break;
    case "GB":
        $redirect_URL_country = "/en/";
        break;
    case "FR":
        $redirect_URL_country = "/fr/";
        break;
    case "ES":
        $redirect_URL_country = "/es/";
        break;
    case "PT":
        $redirect_URL_country = "/pt/";
        break;
    case "IT":
        $redirect_URL_country = "/it/";
        break;
    case "HR":
        $redirect_URL_country = "/hr/";
        break;
    case "PL":
        $redirect_URL_country = "/pl/";
        break;
    case "MX":
        $redirect_URL_country = "/mx/";
        break;
    default:
        $redirect_URL_country = "/xx/";
}
header('Location: http://beta.seataccesoriescatalogue.com' . $redirect_URL_country . 'index.php', true, 301);
exit();
?>