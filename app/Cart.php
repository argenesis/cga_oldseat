<?php

/**
 * Class Cart | app/Cart.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Cart - [Singleton] Implemented as wishlist/cart in the project.
 * 
 * This class perform CRUD operations over the $_SESSION['cart'].
 * 
 * @used-by controllers/cartController::$_iCart
 * @revision Argenis Urribarri <argenis@snconsultors.es> 
 * 
 */
class Cart {

    private static $instance = NULL;
    private $_items = array();

    public function __construct() {
        
    }

    //singleton
    public static function singleton() {
        if (self::$instance == NULL) {
            session_start();
            if (isset($_SESSION['cart'])) {
                self::$instance = unserialize($_SESSION['cart']);
            } else {
                self::$instance = new Cart;
            }
            session_write_close();
        }
        return self::$instance;
    }

    //serialized in JSON
    private function _saveInSession() {
        session_start();
        $_SESSION['cart'] = serialize(self::singleton());
        session_write_close();
    }

    //add QUANTITY to...
    public function addItem(&$item) {   //item= array[prd_data]: generate uid in md5...
        $item_uid = md5($item[$_SESSION['lang_tag']]['prd_pk']); //'id'.//[$_SESSION['lang_tag']]
        $item['uid'] = $item_uid;
        $item['count'] = 1;
        // fetch; if found add 1 to quantity
        foreach ($this->_items as $current_item) {
            if ($current_item['uid'] === $item_uid) {
                $item['count'] = $current_item['count'] + 1;
            }
        }
        // add product
        $this->_items[$item_uid] = $item;
        $this->_saveInSession();//*/        
    }

    public function updateQuantity($upQ) {
        $item_uid = $upQ['uid'];
        $item = $this->_items[$item_uid];
        $item['count'] = $upQ['newQuantity'];
        $this->_items[$item_uid] = $item;
        $this->_saveInSession();
    }

    public function deleteItem($item_uid) {
        unset($this->_items[$item_uid]);
        $this->_saveInSession();
    }

    public function deleteAllItem() {
        unset($this->_items);
        $this->_saveInSession();
    }

    public function getItem($item_uid) {
        $vResult = $this->_items[$item_uid];
        return $vResult;
    }

    public function getItems() {
        $vResult = array();
        foreach ($this->_items as $current_item) {
            $vResult[] = $current_item;
        }
        return $vResult;        
    }

    public function getItemCount() {
        return count($this->_items);
    }
}
