<?php

/**
 * LAUNCHER File | app/Launcher.php
 *
 * @package     bootstrapping
 * @author      Ricard Beyloc <ricard@snconsultors.es>
 * @version     1.0
 * @revision    Ricard Beyloc
 * 
 * Contains all the parameters that are required for each "index.php" file in each
 * lang/market folder[cs][de][en][es][fr][hr][it][mx][pl][pt],so the ".htaccess" in 
 * the project's root directory can perform the rewrite rule accordingly.
 * 
 */
//require_once APP_PATH . 'Login.php';
require_once APP_PATH . 'Cart.php';
require_once APP_PATH . 'Config.php';
require_once APP_PATH . 'Request.php';
require_once APP_PATH . 'ControllersMapping.php';
require_once APP_PATH . 'Bootstrap.php';
require_once APP_PATH . 'Controller.php';
require_once APP_PATH . 'Model.php';
require_once APP_PATH . 'View.php';
require_once APP_PATH . 'Database.php';

/* Si no está iniciada sessió iniciarla i si no hi ha lang guardada guardar en sessio la per defecte */
if (!isset($_SESSION)) {
    session_start();
}
if(!isset($_SESSION['user_lang'])) {
    $_SESSION['lang_tag'] = DEFAULT_LANG;
} else {
    $_SESSION['lang_tag'] = $_SESSION['user_lang'];
   
}

try {
    Bootstrap::run(new Request);
} catch (Exception $e) {
    echo $e->getMessage();
}
?>