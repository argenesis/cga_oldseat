<?php

/**
 * Class Bootstrap | app/Bootstrap.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Bootstrap - [static]ENTRY SCRIPT/MAIN TRIGGER for the application.
 * 
 * This class checks the params for a Request instance in order to build it.
 * 
 * @uses Request::$_country for language TAGS and market parameters.
 * @uses Request::$_controller to check if controller file exists.
 * @uses Request::$_method to check if the method is a callable function.
 * @uses Request::$_parameters.
 * 
 * @revision Argenis Urribarri; Ricard Beyloc
 * @throws Exception Controller not found
 * 
 */

class Bootstrap {

    /** Main entry point.
     * 
     * @param Request $request Through the request getters all instantiation takes place.
     */
    public static function run(Request $request) {
        /* Carregar el config específic del country i el tesauro de la llegua de la sessió */
        require_once(ROOT . 'public/includes/config/config_' . _COUNTRY . '.php');
        require_once APP_PATH . DS . 'tesauro' . DS . 'tesauro_' . $_SESSION['lang_tag'] . '.php';
        require_once APP_PATH . DS . 'tesauro' . DS . 'tesauro_' . constant('DEFAULT_LANG') . '_mailto.php';
        $controller = $request->getController() . 'Controller';
        $controller_path = ROOT . 'controllers' . DS . $controller . '.php';
        $method = $request->getMethod();
        $parameters = $request->getParameters();
        //Controller file path avaliable -> make instance 
        if (is_readable($controller_path)) {
            require_once $controller_path;
            $mController = ControllersMapping::makeControllerInstance($controller);
            $controller = $mController; //instance of child-controller          
            //requested method is accesible -> get_name || set_index as default method
            if (is_callable(array($controller, $method))) {
                $method = $request->getMethod();
            } else {
                $method = 'index';
            }
            //with/without arguments selector
            if (!empty($parameters)) {
                call_user_func(array($controller, $method), $parameters);
            } else {
                call_user_func(array($controller, $method));
            }
        } else {
            throw new Exception('Controller not found [' . $controller . ']');
        }
    }
}
