<?php

/**
 * Class ControllerMapping | app/ControllerMapping.php
 *
 * @package     bootstrapping
 * @author      Argenis Urribarri <argenis@snconsultors.es>
 * @version     1.0
 */

/**
 * Class ControllerMapping - [Factory Pattern] hardcoded mapping of controllers name in "/controllers".
 * 
 * This class implements a Factory pattern so former command 'new $controller'
 * wont throw a 'Cannot instantiate abstract class \controller' error.
 * ALL CONTROLLERS MUST BE REGISTERED IN THIS CLASS FOLLOWING THE CORRESPONDING CODE CONVENTION.
 * 
 * @return Controller Makes the instance of the requested controller. 
 * 
 */
class ControllersMapping {

    static function makeControllerInstance($controller) {
        $mController = NULL; // instance
        $ext = 'Controller';
        switch ($controller) {
            case "cart" . $ext:
                $mController = new cartController;
                break;
            case "category" . $ext:
                $mController = new categoryController;
                break;
            case "collection" . $ext:
                $mController = new collectionController;
                break;
            case "collectionprd" . $ext:
                $mController = new collectionprdController;
                break;
            case "error" . $ext:
                $mController = new errorController;
                break;
            case "index" . $ext:
                $mController = new indexController;
                break;
            case "lang" . $ext:
                $mController = new langController;
                break;
//            case "login".$ext:
//                    $mController = new loginController;
//                break; 
            case "map" . $ext:
                $mController = new mapController;
                break;
            case "model" . $ext:
                $mController = new modelController;
                break;
            case "policy" . $ext:
                $mController = new policyController;
                break;
            case "print" . $ext:
                $mController = new printController;
                break;
            case "product" . $ext:
                $mController = new productController;
                break;
            case "searchresults" . $ext:
                $mController = new searchresultsController;
                break;
            case "seleccion" . $ext:
                $mController = new seleccionController;
                break;
        }
        return $mController;
    }

}
