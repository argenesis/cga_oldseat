<?php

/**
 * Class Controller | app/Controller.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Controller - [Abstract] Extended by all controllers.
 * 
 * Reacts on events triggered by the user(URL, href). This class defines the 
 * BASIC methods and params that a controller requires to be constructed. 
 * Controller names MUST NOT use uppercase(url handling). The following is a 
 * valid example: "controllernameController.php"
 * 
 * @revision Argenis Urribarri <argenis@snconsultors.es>
 * @throws ModelNotFound.
 * 
 */
abstract class Controller {

    //$_autRole[parameter to control the level-access/use of the controller]
    public $_autRole; //@catalogue-dev
    protected $_view;

    abstract public function index();

    abstract protected function set_autRole();

    public function get_autRole() {
        return $this->_autRole;
    }

    public function getClassName() {
        return __CLASS__;
    }

    public function __construct() {   //constructor; sets the value $_autRole defined for each child
        $this->_view = new View(new Request);
        //$this->_current_user = Login::singleton();//@catalogue-dev
        //$this->set_autRole();//@catalogue-dev
    }

    protected function loadModel($model_filename) {   //builds a php filepath containing the file
        $model_filename = $model_filename . 'Model';
        $model_filepath = ROOT . 'models' . DS . $model_filename . '.php';
        //instance of model if accessible 
        if (is_readable($model_filepath)) {
            require_once $model_filepath;
            $obj_model = new $model_filename();
            return $obj_model;
            //not accessible
        } else {
            throw new Exception('Model not found [' . $model_filename . ']');
        }
    }

}
