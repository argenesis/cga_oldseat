﻿<?php
/**
 * Class View | app/View.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class View - Builds the display logic and the results data on request by the user.
 * Some common params are defined with hardcoding in this class (hc_ul_langsdropdown)
 * for practical reasons. All hardcoding has the prefix "hc_".
 * 
 * @todo Modification of the class so $recordSet is replaced by objects type.
 * @return Array Recordset contains key=>pair values that are looped on the ".phtml" files.
 * 
 */
class View {

    public $recordSet;
    public $ul_models;
    public $ul_langsdropdown;
    private $_controller;

    public function __construct(Request $request) {
        $this->_controller = $request->getController();
    }

    /** Build the LAYOUT [header][view][footer](includes)
     * 
     * @param string $header The filename for the header to be loaded; on the file-tree is '.php'.
     * @param string $view The filename for the view; on the file-tree the content is '.phtml'.
     * @param string $footer The filename for the footer; on the file-tree is '.php'. */
    public function render($header, $view, $footer) {
        $path_for_view = ROOT . 'views' . DS . $this->_controller . DS . $view . '.phtml';

        if (is_readable($path_for_view)) {
            $this->ul_models = $_SESSION['ul_navbar'];
            $this->ul_langsdropdown = $this->hc_ul_langsdropdown();
            //[HEADER]
            require_once ROOT . 'views' . DS . 'layout' . DS . DEFAULT_LAYOUT . DS . $header . '.php';
            //[CONTENT].phtml
            require_once $path_for_view;
            //[FOOTER]
            //PRINT view conditional
            if (!($footer == NULL)) {
                require_once ROOT . 'views' . DS . 'layout' . DS . DEFAULT_LAYOUT . DS . $footer . '.php';
            }
        } else {
            throw new Exception('View not found [' . $view . ']');
        }
    }

    /** Languages data dropdown: COMMON TO ALL VIEWS. 
     * The $_SESSION['lang_tag'] is set through this function.
     * Lang tags use an ISO 639-1 code based identifier.
     * It is required to provide a string with the NAME of the lang.
     * The '#' is to avoid the setting of the items as a link. */
    public function hc_ul_langsdropdown() {
        return $langs = array(
            'es' => array('Español' => '#'),
            'en' => array('English' => '#'),
            'de' => array('Deutsch' => '#'),
            'fr' => array('Français' => '#'),
            'it' => array('Italiano' => '#'),
            'pt' => array('Português' => '#'),
            'cs' => array('Ceština' => '#'),
            'pl' => array('Polski' => '#'),
            'hr' => array('Hrvatski' => '#')
        );
    }

}
