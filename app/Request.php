<?php

/**
 * Class Request | app/Request.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Request - retrieves and parses the URL into [country][controller][method][parameters].
 * 
 * @revision Argenis Urribarri; Ricard Beyloc 
 * @param string $_country Country=market=language; language nomenclature is based on ISO 639-1 codes.
 * @param string $_controller Concept name for the controller(without 'Controller.php').
 * @param string $_method Callable function from controller.
 * @param string $_parameters These are shown in the URL; they're divided by the "/" character as well. 
 * @return Request The instance contains the configuration to build a response.
 * 
 */
class Request {

    private $_country;
    private $_controller;
    private $_method;
    private $_parameters;

    public function __construct() {
        if (isset($_GET['url'])) {
            $url = explode('/', $_GET['url']);
            $this->_country = strtolower(array_shift($url));
            $this->_controller = strtolower(array_shift($url));
            $this->_method = strtolower(array_shift($url));
            $this->_parameters = $url;
        }

        if (!$this->_country) {
            $this->_country = _COUNTRY;
        }
        if (!$this->_controller) {
            $this->_controller = DEFAULT_CONTROLLER;
        }
        if (!$this->_method) {
            $this->_method = 'index';
        }
        if (!isset($this->_parameters)) {
            $this->_parameters = array();
        }
    }

    public function getCountry() {
        return $this->_country;
    }

    public function getController() {
        return $this->_controller;
    }

    public function getMethod() {
        return $this->_method;
    }

    public function getParameters() {
        return $this->_parameters;
    }

}
