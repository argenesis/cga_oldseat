<?php

/**
 * Class Database | app/Database.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Database - [mysqli] MySQL functions related to CRUD(READ).
 * 
 * This class is specific to the mysql database manager; the connection to ddbb 
 * parameters are defined in "app/Config.php"
 *  
 * @revision Argenis Urribarri <argenis@snconsultors.es> 
 * 
 */
class Database extends mysqli {

    public function __construct() {   //"app/Config.php" parameters
        parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        if (!$this->set_charset("utf8")) {
            die("Error loading  el conjunto de caracteres utf8: " . $this->error);
        }
    }

    public function query($sql_query) {
        return parent::query($sql_query);
    }

    /** Retrieves query rows as an array. */
    public function queryParams($sql_query_raw, $params) {   //array of $row lines from query 'result' with added $params         
        $sql_query = vsprintf($sql_query_raw, $params);
        $result_query = $this->query($sql_query);
        $results = array();
        while ($row = $result_query->fetch_assoc()) {
            $results[] = $row;
        }
        return $results;
    }

}
