<?php

/**
 * Class Login | app/Login.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Login - [Singleton] Handles logged in user's attributes and methods.
 * 
 * The Singleton pattern avoids "cloned" users logged in. The methods allow 
 * managing the 'login' and it's saving on 'session'.
 * 
 * @revision Argenis Urribarri <argenis@snconsultors.es> 
 * 
 */
class Login {

    private static $instance = NULL;
    private $_login_id;
    private $_login_name;

    public static function singleton() {   //New session only if 'instance' NULL
        if (self::$instance == NULL) {
            //session_start();
            //$_SESSION['lang_tag'] = 'it'; //FIXME: 'mx' breaks
            //Deserialize 'login'; retrieve attributes values
            if (isset($_SESSION['login'])) {
                self::$instance = unserialize($_SESSION['login']);
                //No 'login' object
            } else {
                self::$instance = new Login;
            }
            return self::$instance;
        }
    }

    private function __construct() {
        $this->_initialize();
    }

    private function _initialize() {   //init empty
        $this->_login_id = 0;
        $this->_login_name = '';
        $this->_login_role = '';
    }

    public function assign($values) {   //Save in session; FIXME: assignment depends on 'user=viewer || user=editor'
        $this->_initialize();
        $this->_login_id = $values['login_user_id'];
        $this->_login_name = $values['login_user'];
        $this->_login_role = $values['login_role'];
        $this->_saveInSession();
    }

    private function _saveInSession() {   //Save 'login' when session didn't exist before
        session_start();
        $_SESSION['login'] = serialize($this->singleton());
    }

    public function getUserId() {
        return $this->_login_id;
    }

    public function getLoginName() {
        return $this->_login_name;
    }

    public function getLoginRole() {
        return $this->_login_role;
    }

    public function getBaseUrl() {
        return BASE_URL;
    }

    public function isLogged() {   //'true' when there is a logged user
        return ($this->_login_name != "");
    }

    public function __clone() {   // Rewrite method 'clone' avoiding clonation of object
        trigger_error('Object clonation not allowed', E_USER_ERROR);
    }

    public function logout() {   //Empty array for _session; destroy session; init empty login and save it on session
        session_start();
        $_SESSION = array();
        session_destroy();
        $this->_initialize();
        $this->_saveInSession();
        return true;
    }

    public function isInRole($role) {   //access level
        $vResult = false;
        if ($role == 'ALL') {
            $vResult = true;
        }
        if ($role == 'USER') {
            $vResult = $this->_login_role == 'USER';
        }
        if ($role == 'ADMIN') {
            $vResult = $this->_login_role == 'ADMIN';
        }
        return $vResult;
    }

}
