<?php

/**
 * CONFIG File | app/Config.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 * @revision Argenis Urribarri; Ricard Beyloc
 * 
 * Constants and base configuration values.
 * 
 */
//PRODUCTION ENVIRONMENT - Online Server
define('GENERAL_URL', 'http:' . DS . DS . 'beta.seataccesoriescatalogue.com' . DS);
define('BASE_URL', GENERAL_URL . _COUNTRY . DS);

//PROJECT REPOSITORIES PREFIXES(***see Config-README.txt***)
$server_root_docs = $_SERVER['DOCUMENT_ROOT'];
define('DOCS_ROOT', $server_root_docs);
define('IMG_REPO', 'http://gestor.seataccesoriescatalogue.com/imgs/');//'http://seataccesoriescatalogue.com/imgs/'
define('DOCS_REPO', 'http://gestor.seataccesoriescatalogue.com/files/documents/');
define('MEDIA_REPO', 'http://gestor.seataccesoriescatalogue.com/media/');
define('CSS_REPO', GENERAL_URL . 'public' . DS . 'css' . DS);
define('FONTS_REPO', GENERAL_URL . 'public' . DS . 'fonts' . DS);
define('JS_REPO', GENERAL_URL . 'public' . DS . 'js' . DS);
/** The webroot is mapped to a path in the filesystem(/var/www/example.com/http_docs/).
 * The webserver is not asked to translate the path when using 'include'.*/
define('INCLUDES_REPO', DOCS_ROOT . DS . 'public' . DS . 'includes' . DS);

//DEFAULT VALUES - USED BY VIEWS(render)
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');
define('HEADER', 'header');
define('FOOTER', 'footer');
define('FOOTER_VIDEO', 'footer_video_modal');
define('SIMPLE_HEADER', 'header_simple');
define('PRINTING_HEADER', 'header_print');
define('MAP_HEADER', 'header_map');

//Google APIs keys: account[user: snconsultors@gmail.com - pass: Snc_2017@]
define('googlemaps', '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcNDgMVLYdw5_rco9U7bCgqtya-xkyk_U&callback=initMap" async defer></script>');
//Site-domain configuration must be set in "https://www.google.com/recaptcha"
define('recaptcha', '<div class="g-recaptcha" data-sitekey="6LesmzkUAAAAAP1o6pRww1m8A2HTUSBXt5GIPNTm" async defer></div>');
define('recaptchaSecret', '6LesmzkUAAAAAExoeV_iA0BdqWr_-RCR1niax2Ze');

//DDBB - Online GEST
define('DB_HOST', 'seataccesoriescatalogue.com');
define('DB_USER', 'sncgest');
define('DB_PASS', 'nMSN68t5jedfPGn7');
define('DB_NAME', 'catalogogest');
define('DB_CHAR', 'utf8');

/*//DDBB - Online COM
define('DB_HOST', 'seataccesoriescatalogue.com');
define('DB_USER', 'snccat');
define('DB_PASS', 'nMSN68t5jedfPGn7');
define('DB_NAME', 'catalogocom');
define('DB_CHAR', 'utf8');//*/

//PLATFORM CONFIGURATION
define('show_ut', false);

//MAILING SERVICES
//--los valores vacios deben ser NULL.
define('mailORIGIN', 'shop@seataccesoriescatalogue.net');
define('mailCC', null);
define('mailBLIND', 'marketingaccessories@seat.es');