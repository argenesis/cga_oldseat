//---------------------------------------------------------------------------//
* autor: SNCONSULTORS
* descripcion: Este documento registra los cambios solicitados por SEAT sobre 
el diseño/funcionalidad del catalogo; generalmente se contemplan elementos que 
se eliminan, pues el seguimiento a incluir elementos se hace en el control de
version. 
    Los cambios se indican en el codigo con un comentario de HTML generado por 
un echo de php con la etiqueta que contiene la fecha[YEAR/MONTH/DAY] en que se 
hizo el cambio. Por ejemplo...  
        <?= '<!--@[2017/10/16]-->' ?> 
    ... se relaciona con un cambio realizado el 16 de octubre de 2017.
//---------------------------------------------------------------------------//
###############################################################################
@[2017/00/00]
Descripcion del change_request

    codigo_removido;

###############################################################################
<!--@[2017/10/16]-->
SEAT solicita remover el siguiente codigo de "views/layout/default/footer.php"

    <div class="col-xs-12 col-sm-12 col-md-4 text-primary text-center">
      <span class="seat-white seat-footer-big"><?= constant('Footer3'); ?></span>
    </div>