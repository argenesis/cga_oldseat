2017/12/07 - 12:00
@author Argenis Urribarri<argenis@snconsultors.es>

################################################################################
       IMPLEMENTACION DEL PROYECTO EN DIFERENTES ENTORNOS/SERVIDORES
################################################################################

Ademas del cambio en las constantes que sirven de referencia a cada repositorio,
deben considerarse los siguientes cambios cuando se realice una implementacion 
del proyecto para el funcionamiento completo del sistema:

.htaccess:
Las siguientes notas se documentan debido al error con la codificacion
\xef\xbb\xbf
1. GUARDAR EL ARCHIVO COMO UTF-8 SIN BOM
2. CARGAR EL ARCHIVO EN EL SERVIDOR (NO SOBREESCRIBIR EL EXISTENTE, SINO BORRAR
    EL QUE HAYA EN EL SERVIDOR Y CARGAR UNO NUEVO)

> Archivo "public/css/CGA.css":
    Debemos buscar la siguiente linea y editar la URL de la imagen para que cargue
    la imagen desde el repositorio que corresponda; esto asigna un fondo rojo tipo
    punta en el primer elemento del breadcrumbs
    
    .breadcrumb>li:first-child {
        background: url('http://gestor.seataccesoriescatalogue.com/imgs/punta_red.png') no-repeat right;

> Archivo "public/css/overlaySpinner.css":
    Debemos buscar la siguiente linea y editar la URL de la imagen para que cargue
    la imagen desde el repositorio que corresponda; esto muestra el indicador de
    carga de dealer-point en el mapa

    .spin-loader { 
        height: 45px;
        background: url("http://gestor.seataccesoriescatalogue.com/imgs/load_wheel.gif") no-repeat center center transparent;

> Archivo "public/js/map_output.js":
    Debemos buscar la siguiente linea y editar la URL de la imagen para que cargue
    la imagen desde el repositorio que corresponda; la imagen genera el icono de 
    cada punto de dealer en el map.

    map.data.setStyle(function() {
        return {icon:'http://gestor.seataccesoriescatalogue.com/imgs/find_dealer_logo.png'};//find_dealer_logo.png
        });

> Archivo "public/js/commonviews.js":
    Este archivo contiene una funcion javascript "get_baseurl()" en la que debe
    considerarse las siguientes lineas:

        var n = absURL.indexOf(".com");
        return absURL.substr(0, n + 8);

    ... el numero 8 (n + 8) son el numero de caracteres desde el indice del string 
    ".com" hasta el final del indicador de idioma y la barra.
        Es decir:
            [...].com/es/
            desde el primer punto "." hasta la ultima barra "/" hay 8 caracteres.
    
    DEBE EDITARSE ".com" DONDE CORRESPONDA (POR EJEMPLO PARA .NET); SINO NO SE
    REALIZA LA EXTRACCION DE LA BASEURL PARA JS.