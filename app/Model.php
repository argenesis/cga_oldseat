<?php

/**
 * Class Model | app/Model.php
 *
 * @package     bootstrapping
 * @author      SNCONSULTORS
 * @version     1.0
 */

/**
 * Class Model - [extended by all models] Base class to models.
 * 
 * The business logic and representation of data; all managing of information
 * takes place in the model.
 * 
 * @uses Database::query Connect to a database(MySQL in this project)
 * @todo Modification of the class so the database can be chosen through the query.
 * 
 */
class Model {

    protected $_db;

    public function __construct() {
        $this->_db = new Database();
    }

    /** Returns all records as an array */
    public function getAllRecords($query) {
        $results = array();
        while ($row = $query->fetch_assoc()) {
            $results[] = $row;
        }
        return $results;
    }

}
