<?php
//--html--//
define('mlREF', 'OT-Nr.');
define('mlPRD', 'Produkt');
define('mlPRC', 'Preis');
define('mlUT', 'Montagezeit (ZE)');
define('mlDIS', '* Die angegebenen Preise verstehen sich inklusive MwSt. und ohne Montagekosten. Weitere Informationen erhalten Sie von Ihrem SEAT Händler.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Meine Wunschliste');
define('mwlNAME', 'Anzahl');
define('mwlPRC', 'Preis');
define('mwlTOT', 'Gesamt');
define('mwlUNIT', 'Einheiten');
define('mwlDIS', '* Die angegebenen Preise verstehen sich ohne Montagekosten.');
define('mwlDIS_1_1', 'Diese Liste ausgewählter Artikel ist nicht verbindlich und stellt keine Verpflichtung zum Kauf der Artikel dar. Sie dient ausschließlich informativen Zwecken und bietet die Möglichkeit zum Abschluss des Kaufs, sofern dies von Ihnen gewünscht wird. Der Kauf wird erst gültig nach entsprechender Vereinbarung mit dem SEAT Händler Ihrer Wahl.');
define('mwlDIS_1_2', 'In dem Moment, in dem Sie einen SEAT Händler für die Bereitstellung von Informationen und den Kauf sowie die Montage des in der Wunschliste enthaltenen Zubehörs ausgewählt und Ihre persönlichen Daten angegeben haben, erhält der gewählte Händler automatisch eine Benachrichtigung. Darin wird er aufgefordert, mit Ihnen Kontakt aufzunehmen und Sie über alle benötigten Details zu informieren.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Es wurde eine leere Wunschliste gesendet.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Neue Anfrage über SEAT Original-Zubehör');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'An den Leiter der Abteilung für SEAT Ersatzteile und Zubehör des Händlers ');
define('mP1', 'Über den Online-Katalog für SEAT Original Zubehör hat der Kunde ');
define('mP2', ' sein Interesse an Kauf und Montage der in der nachstehenden Liste aufgeführten Zubehörartikel bekundet.');
define('mP3_1', 'Dieser Kunde wartet auf Ihren Anruf, um die letzten Details der Transaktion zu besprechen. Nehmen Sie daher bitte schnellstmöglich mit dem Kunden Kontakt auf, um diesen Geschäftsabschluss zu sichern.');
define('mP3_2', 'Nachfolgenden sind die Kontaktdaten des Kunden angegeben:');
define('mP4_1', 'Wir hoffen, dass dieses Geschäft zu einem erfolgreichen Abschluss kommt und dass diese neue Funktion des Online-Zubehörkatalogs weitere Transaktionen ermöglicht, die zweifellos das Geschäft mit Zubehörartikeln fördern werden.');
define('mP4_2', 'Ausgewählte Zubehörartikel') ;
define('mP5', 'Mit freundlichen Grüßen.');
define('mP6', 'SEAT Original Zubehör.');
define('mP7', 'DISCLAIMER: Diese Nachricht enthält proprietäre Informationen, die möglicherweise in Teilen oder vollständig als vertraulich eingestuft und/oder rechtlich geschützt sind. Der Inhalt ist ausschließlich für den vorgesehenen Empfänger bestimmt. Wenn Sie nicht der richtige Empfänger sind oder diese E-Mail irrtümlich erhalten haben, informieren Sie bitte den Absender. Wenn Sie nicht der vorgesehene Empfänger sind, dürfen Sie diese Mitteilung auf keinerlei Art und Weise verwenden, weitergeben, ausdrucken, kopieren oder verbreiten.');

?>