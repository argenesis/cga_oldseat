<?php
//--html--//
define('mlREF', 'Référence');
define('mlPRD', 'Produit');
define('mlPRC', 'Prix');
define('mlUT', 'Temps de montage (U.T.)');
define('mlDIS', '* Les prix indiqués incluent la TVA et n\’incluent pas les coûts de montage ; pour plus d\’informations contactez votre concessionnaire SEAT.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Voir ma liste de souhaits');
define('mwlNAME', 'Nom');
define('mwlPRC', 'Prix');
define('mwlTOT', 'Total');
define('mwlUNIT', 'Unités');
define('mwlDIS', '* Les prix indiqués n\’incluent pas de montage.');
define('mwlDIS_1_1', 'Cette liste de produits sélectionnés ne vous contraint ni ne vous engage à l\’achat de ces produits. Elle est fournie à titre purement informatif avec la possibilité de réaliser l\’opération si vous le souhaitez et au moment convenu avec le concessionnaire SEAT sélectionné.');
define('mwlDIS_1_2', 'Lorsque vous choisissez le concessionnaire SEAT que vous souhaiteriez contacter pour recevoir des informations, acquérir et  monter les accessoires figurant dans la liste de souhaits et après demande de vos données personnelles, une communication automatique est envoyée au concessionnaire sélectionné afin qu\’il vous contacte et vous informe de tous les détails nécessaires.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Une liste de souhaits vide a été envoyée.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Nouvelle demande d’Accessoires d\’origine SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Pour le responsable du service de Pièces de rechange et Accessoires SEAT du concessionnaire ');
define('mP1', 'Par l\’intermédiaire du Catalogue en ligne d’Accessoires d\’origine SEAT, le client ');
define('mP2', ' a manifesté son intérêt pour l\’achat et le montage des accessoires figurant dans la liste ci-dessous.');
define('mP3_1', 'Ce client attend un appel de votre part pour examiner et valider les détails de cette vente. Nous vous invitons donc à prendre contact avec lui dans les plus brefs délais afin de garantir l\’opération.');
define('mP3_2', 'Voici les coordonnées du client :');
define('mP4_1', 'Nous espérons que cette opération se confirme et que cette nouveauté intégrée au Catalogue d\’Accessoires en ligne suscite d\’autres opérations similaires qui favoriseront sans aucun doute le développement de l\’activité d’Accessoires.');
define('mP4_2', 'Accessoires sélectionnés') ;
define('mP5', 'Cordialement.');
define('mP6', 'Accessoires d\’origine SEAT.');
define('mP7', 'AVERTISSEMENT : Ce message contient des informations personnelles dont tout ou partie peut contenir des informations confidentielles ou protégées légalement. Ces informations s\’adressent exclusivement au destinataire de ce message. Si vous n\’êtes pas le destinataire de ce message mais que vous l\’avez reçu suite à une erreur d\’envoi ou de transmission, veuillez en informer l\’expéditeur. Si vous n\’êtes pas le destinataire final de ce message, vous ne devez utiliser, informer, distribuer, imprimer, copier ou diffuser ce message d\’aucune manière.');
?>