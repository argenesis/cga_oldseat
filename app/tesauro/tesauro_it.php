<?php
$_SESSION['lang'] = 'italiano'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Il prodotto non è distribuito su questo mercato.');
define('aut_role', 'Non si hanno permessi sufficienti per accedere a questo servizio.');
define('model_mrkt', 'Il modello selezionato non è in commercio su questo mercato.');
define('browser', 'Il browser non supporta questa opzione.');
define('err_wlITEM', 'Questo campo non può rimanere vuoto o a zero, deve essere un numero intero positivo.'); //Wishlist - error on quantity
define('err_videoplayer', 'Il browser non permette la riproduzione di questo tipo di file.');

//************************ TRANSLATION ****************************//
define('Head1', 'Catalogo Generale degli Accessori SEAT');
define('Head2', 'Catalogo Generale');
define('Head3', 'Accessori Originali');
define('Footer1', 'ACCESSORI ORIGINALI');
define('Footer2', ' - SEAT applica una politica di continuo sviluppo del prodotto e si riserva il diritto di apportare modifiche alle specifiche.');
define('Footer3', 'TECHNOLOGY TO ENJOY');
define('Home1', 'Accessori per te');
define('Home1alt', 'a SEAT Collection home');
define('Home2', 'Accessori per l’auto');
define('Home2alt', 'a SEAT Vehicles home');
define('Col1alt', 'a SEAT Collection Ateca');
define('Col2alt', 'a SEAT Collection Essentials');
define('Col3alt', 'a SEAT Collection Motorsports');
define('Col4alt', 'a SEAT Collection Mediterranean');
define('cat1col1', 'Non ci sono prodotti disponibili per questo mercato.');
//--product view(coll&acc)
define('tabTXT', 'Prodotto');
define('tabDESP', 'Sezioni');
define('lREF', 'Codice');
define('lNEW', 'Nuovo!');
define('lSIZ', 'Taglia');
define('lCOL', 'Colore');
define('lPRD', 'Prodotto');
define('lPRC', 'PVP');
define('lUT', 'Tempo di montaggio (U.T.)');
define('lDIS', '* I prezzi sono IVA inclusa, non comprendono il montaggio. Per maggiori informazioni rivolgersi al proprio rivenditore SEAT di fiducia.');
define('lPRNT', 'Stampa');
define('lCROSS', 'Potrebbe interessarti anche'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Seleziona il prodotto');
define('listREF', 'Codice');
define('listPRO', 'Prodotto');
define('listUT', 'Tempo di montaggio (U.T.)');
define('listPRC', 'PVP');
//--searchresults & searchbox
define('search1', 'Il prodotto con codice');
define('search2', 'non è stato trovato');
define('search3', 'a nessun modello');
define('search4', 'trovato');
define('search5', 'per i seguenti modelli attuali');
define('search6', 'a modello attuale');
define('search7', 'per i seguenti modelli precedenti');
define('search8', 'a modello precedente');
define('search9', 'in SEAT Collection');
define('buscador','Nome o codice');
//--wishlist(xxx_WL)
define('add_WL', 'Aggiungi alla Lista dei desideri');
define('view_WL', 'Vedi Lista dei desideri');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'La mia Lista dei desideri');
define('wlNAME', 'Nome');
define('wlPRC', 'Prezzo');
define('wlTOT', 'Totale');
define('wlUPDATE', 'Aggiorna Lista');
define('wlEMPTY', 'Svuota Lista');
define('wlEMPTIED', 'Lista dei desideri svuotata');
define('wlPRNT', 'Stampa Lista');
define('wlUNIT', 'Unità');
define('wlADD', 'Aggiunto alla Lista dei desideri');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Lista dei desideri');
define('wlTAB_CONTACTFORM', 'Invia la lista al rivenditore');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Tutte');
define('mpTITLE', 'Seleziona il distributore');
define('mpCLOSE_btn', 'Chiudi');
define('mpPROV', 'Seleziona provincia');
define('mpSEARCH_btn', 'Trova rivenditore');
define('mpCHOSE_btn', 'Seleziona e chiudi');
define('cformTITLE', 'Invia la Lista dei desideri al rivenditore:');
define('cformSELDEALER_btn', 'Seleziona rivenditore');
define('cformSELDEALER_error', 'Seleziona un rivenditore.');
define('cformDEALERNAME', 'Nome del rivenditore *');
define('cformDEALERNAME_hint', 'Selezionare un rivenditore sulla mappa*');
define('cformDEALERNAME_error', ' Nome del rivenditore obbligatorio.');
define('cformDEALERADRESS', 'Indirizzo del rivenditore *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Indirizzo del rivenditore obbligatorio.');
define('cformDEALERMAIL', 'E-mail del rivenditore *');
define('cformDEALERMAIL_hint', 'Selezionare un rivenditore sulla mappa o scrivere l’indirizzo e-mail del rivenditore*');
define('cformDEALERMAIL_error', 'Indirizzo e-mail del rivenditore obbligatorio.');
define('cformNAME', 'Nome *');
define('cformNAME_hint', 'Inserisci il tuo nome *');
define('cformNAME_error', 'Nome obbligatorio.');
define('cformSURNAME', 'Cognome *');
define('cformSURNAME_hint', 'Inserisci il tuo cognome *');
define('cformSURNAME_error', 'Cognome obbligatorio.');
define('cformMAIL', 'E-mail *');
define('cformMAIL_hint', 'Inserisci il tuo indirizzo e-mail *');
define('cformMAIL_error', 'Indirizzo e-mail valido obbligatorio.');
define('cformPHONE', 'Telefono');
define('cformPHONE_hint', 'Inserisci il tuo numero di telefono di contatto (optional)');
define('cformMSG', 'Messaggio');
define('cformMSG_hint', 'Se si desidera, è possibile inserire un messaggio di testo (optional)');
define('cformSEND_btn', 'Invia messaggio');
define('cformNOTICE','Questi campi sono obbligatori.');
define('cform_POSTerror','Errore nell´invio del modulo. Riprovare.');
define('cform_CAPTCHAerror','reCaptcha non valido o non inserito.');
define('cform_SUCCESS','La Lista dei desideri è stata inviata con successo.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-off');
define('vidmodal_LINKTOMODAL', 'Vedi video del Making-off');
//--
define('furtherinfo', 'Per maggiori informazioni rivolgersi al concessionario SEAT di fiducia');
define('selcar', 'Seleziona auto');
define('prevcars', 'MODELLI PRECEDENTI');
define('newcars', 'MODELLI ATTUALI');
define('dealer', 'Dove acquistare?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Seleziona il prodotto');
define('clistREF', 'Codice');
define('clistPRO', 'Prodotto');
define('clistSIZ', 'Taglia');
define('clistCOL', 'Colore');
define('clistPRC', 'PVP');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Politica dei cookie di SEAT Accessori');
define('modal_txt11', 'SEAT Accessori usa cookie per migliorare la tua esperienza di navigazione attraverso il nostro sito web. È possibile consultare la nostra ');
define('modal_txt12_href', 'politica dei cookie qui');
define('modal_txt13', '.');
define('accept_btn', 'Accetta');
define('policy1', 'Cosa sono i cookie?');
define('policy2', 'Un “cookie” è un piccolo file di testo che viene memorizzato su computer, tablet, telefoni cellulari e su qualunque dispositivo utilizzato per navigare in Internet, in grado di memorizzare le informazioni di navigazione al fine di facilitarla, alcuni cookie possono essere utilizzati per es. per riconoscere un utente già registrato, per facilitare l’accesso alle diverse aree della web, ai servizi e alle promozioni o concorsi esclusivi, senza dover effettuare una nuova registrazione ad ogni visita. Ci sono inoltre cookie statistici per la misurazione dell’audience, che permettono di monitorare ed analizzare il comportamento degli utenti del sito web.');
define('policy3', 'SEAT Accessori si impegna a stabilire sempre gli adeguati meccanismi per ottenere il consenso dell\'Utente all\'installazione di cookie che lo richiedano. Ciononostante, in conformità con quanto indicato dalla legge, resta inteso che (1) l\'Utente ha dato il proprio consenso qualora modifichi la configurazione del browser disattivando le limitazioni all’entrata di cookie e (2) che non è necessario ottenere un consenso preventivo in tutti quei casi in cui l\'utilizzo di cookie serva esclusivamente a scopi tecnici per la prestazione di un servizio esplicitamente richiesto dall\'Utente (mediante previa registrazione).');
define('policy4', 'Tuttavia, la loro disattivazione potrebbe modificare il funzionamento del sito web. Per maggiori informazioni, consultare le istruzioni e i manuali del browser.');
define('policy5', '(1) Per Microsoft Internet Explorer, accedere a Menu, Strumenti, Opzioni Internet e selezionare Privacy.');
define('policy6', 'Se si usa Firefox su Mac andare all’opzione menu Preferenze, Privacy, accedere a Visualizza Cookie. se si usa Windows andare su menu, Strumenti, Opzioni, Privacy e alla voce Impostazioni cronologia, selezionare Utilizza impostazioni personalizzate.');
define('policy7', 'Per Safari, andare a menu Preferenze e selezionare Privacy.');
define('policy8', 'Per Google Chrome, accedere a menu Strumenti, selezionare Opzioni (Preferenze su Mac), fare clic su Mostra impostazioni avanzate, e nella sezione Privacy selezionare Impostazioni contenuti.');
define('lDIS_1_1', 'Questa lista di prodotti non è vincolante e non implica nessun obbligo di acquisto, è solamente di carattere informativo. Se si desidera, è possibile chiudere l’operazione dopo averla concordata con il distributore SEAT selezionato.');
define('lDIS_1_2', 'Al momento di selezionare il distributore SEAT dal quale si desidera ricevere informazioni sull’acquisto e montaggio degli accessori aggiunti alla Lista dei desideri e, dopo aver inserito i propri dati personali, viene inviato automaticamente un messaggio al rivenditore selezionato il quale vi contatterà per informarvi di tutti i dettagli necessari.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'La tua Lista dei desideri è vuota.');
//--toClient_mail()--
define('mcSubjectForClient', 'Conferma richiesta Lista dei desideri');//***
//--toClient_mailContent()--//
define('mcH2', 'Gentile Cliente ');//*
define('mcP1', 'La ringraziamo per l’interesse dimostrato nei confronti della gamma di Accessori Originali che la SEAT mette a disposizione per la personalizzazione dell’auto.');//*
define('mcP2', 'Nelle prossime ore il responsabile del dipartimento Accessori del rivenditore ');//*
define('mcP3_1', ' da Lei selezionato la contatterà per ultimare i dettagli riguardanti l’operazione di vendita e montaggio degli accessori scelti.');//*
define('mcP3_2', 'Di seguito sono indicati i recapiti del rivenditore per eventuali dubbi, nonché un riepilogo degli Accessori selezionati');//*
define('mcP4_1', 'Telefono ');
define('mcP4_2', 'Accessori Selezionati') ;
define('mcP5', 'Cordiali saluti');
define('mcP6', 'Accessori Originali SEAT.');
define('mcP7', 'DISCLAIMER: Questo messaggio contiene informazione riservate che, in toto o in parte, possono essere di carattere confidenziale e protette legalmente. Questo messaggio è destinato esclusivamente al destinatario. Qualora fosse stato ricevuto per errore si prega di informare tempestivamente il mittente. Sono strettamente proibite la copia, la comunicazione, la trasmissione, la diffusione o la riproduzione in qualsiasi modo eseguite, dei contenuti di questo messaggio.');
?>	