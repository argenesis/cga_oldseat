<?php
//--html--//
define('mlREF', 'Reference');
define('mlPRD', 'Product');
define('mlPRC', 'Price');
define('mlUT', 'Installation Time (T.U.)');
define('mlDIS', '* The prices include VAT and do not include installation costs. For more information please contact your SEAT dealer.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'My wish list');
define('mwlNAME', 'Name');
define('mwlPRC', 'Price');
define('mwlTOT', 'Total');
define('mwlUNIT', 'Units');
define('mwlDIS', '* The prices shown do not include installation.');
define('mwlDIS_1_1', 'This list of selected products is not linked to nor does it imply an obligation to purchase. It is for information purposes only with the possibility of completing the operation if you are interested and once you have agreed on it with the selected SEAT dealer.');
define('mwlDIS_1_2', 'When you select the SEAT dealer that you are interested to contact for information, purchase and installation of the accessories in the wish list, and after providing your personal details, a communication is submitted automatically to the selected dealer so that they can contact you and advise you on the details you need.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'You have a sent an empty wish list.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'New request from SEAT Original Accessories');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'For the SEAT Parts and Accessories department manager at the dealer ');
define('mP1', 'Through the Online SEAT Original Accessories Catalogue, the customer ');
define('mP2', ' has shown interest in the purchase and installation of the Accessories specified in the following list.');
define('mP3_1', 'This customer is waiting for your call to arrange and finalise the details of the sale, therefore please contact them as soon as possible to ensure the operation.');
define('mP3_2', 'Below are the customer\'s contact details:');
define('mP4_1', 'We hope that this operation is confirmed and that this new functionality implemented in the Online Accessories Catalogue results in more similar operations that will undoubtedly produce benefits for the expansion of the Accessories business.');
define('mP4_2', 'Selected Accessories') ;
define('mP5', 'Kind Regards,');
define('mP6', 'SEAT Original Accessories');
define('mP7', 'DISCLAIMER: This message contains proprietary information, all or part of which may contain confidential or legally protected information. It is exclusively intended for the target user. If you have received this message due to a submission error and are not the recipient, please notify us immediately. If you are not the final recipient of this message you must not use, inform, distribute, print, copy or disclose this message by any means.');
?>