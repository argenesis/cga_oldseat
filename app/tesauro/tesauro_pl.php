<?php
$_SESSION['lang'] = 'polski'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Produkt nie jest dostępny na tym rynku.');
define('aut_role', 'Nie masz odpowiednich praw dostępu do tej usługi.');
define('model_mrkt', 'Wybrany model nie jest dostępny na tym rynku.');
define('browser', 'Przeglądarka nie obsługuje tej opcji.');
define('err_wlITEM', 'To pole nie może być puste ani wypełnione zerem, musi być tu wpisana dodatnia liczba całkowita.'); //Wishlist - error on quantity
define('err_videoplayer', 'Używana przeglądarka nie obsługuje odtwarzania tego formatu plików.');

//************************ TRANSLATION ****************************//
define('Head1', 'Katalog ogólny akcesoriów SEAT');
define('Head2', 'Katalog ogólny');
define('Head3', 'oryginalnych akcesoriów');
define('Footer1', 'ORYGINALNE AKCESORIA');
define('Footer2', ' Ponieważ firma SEAT stosuje politykę stałego doskonalenia swoich produktów, zastrzegamy sobie prawo do wprowadzania zmian w specyfikacjach.');
define('Footer3', 'TECHNOLOGIA DAJĄCA RADOŚĆ');
define('Home1', 'Akcesoria dla ciebie');
define('Home1alt', 'do SEAT Collection home');
define('Home2', 'Akcesoria do twojego samochodu');
define('Home2alt', 'do SEAT Vehicles home');
define('Col1alt', 'do SEAT Collection Ateca');
define('Col2alt', 'do SEAT Collection Essentials');
define('Col3alt', 'do SEAT Collection Motorsports');
define('Col4alt', 'do SEAT Collection Mediterranean');
define('cat1col1', 'Brak produktów dla tego rynku.');
//--product view(coll&acc)
define('tabTXT', 'Produkt');
define('tabDESP', 'Elementy');
define('lREF', 'Nr katalogowy');
define('lNEW', 'Nowość!');
define('lSIZ', 'Rozmiar');
define('lCOL', 'Kolor');
define('lPRD', 'Produkt');
define('lPRC', 'PVP');
define('lUT', 'Czas montażu (U.T.)');
define('lDIS', '* Podane ceny zawierają podatek VAT ale nie obejmują kosztów montażu. Aby uzyskać więcej informacji, należy skontaktować się z przedstawicielem firmy SEAT.');
define('lPRNT', 'Drukuj');
define('lCROSS', 'Inne produkty, które mogą cię zainteresować'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Wybierz produkt');
define('listREF', 'Nr katalogowy');
define('listPRO', 'Produkt');
define('listUT', 'Czas montażu (U.T.)');
define('listPRC', 'PVP');
//--searchresults & searchbox
define('search1', 'Produkt oznaczony numerem katalogowym');
define('search2', 'nie został znaleziony');
define('search3', 'do żadnego modelu');
define('search4', 'został znaleziony');
define('search5', 'do następujących aktualnych modeli');
define('search6', 'do aktualnego modelu');
define('search7', 'do następujących poprzednich modeli');
define('search8', 'do poprzedniego modelu');
define('search9', 'w SEAT Collection');
define('buscador','Nazwa lub nr katalogowy');
//--wishlist(xxx_WL)
define('add_WL', 'Dodaj do listy życzeń');
define('view_WL', 'Zobacz listę życzeń');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Moja lista życzeń');
define('wlNAME', 'Imię');
define('wlPRC', 'Cena');
define('wlTOT', 'Razem');
define('wlUPDATE', 'Aktualizuj listę');
define('wlEMPTY', 'Wyczyść listę');
define('wlEMPTIED', 'Lista życzeń została wyczyszczona');
define('wlPRNT', 'Drukuj listę');
define('wlUNIT', 'Jednostki');
define('wlADD', 'Produkt dodany do listy życzeń.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Lista życzeń');
define('wlTAB_CONTACTFORM', 'Wyślij listę do przedstawiciela');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Wszystkie');
define('mpTITLE', 'Wybierz dystrybutora');
define('mpCLOSE_btn', 'Zamknij');
define('mpPROV', 'Wybierz województwo');
define('mpSEARCH_btn', 'Szukaj przedstawicieli');
define('mpCHOSE_btn', 'Wybierz i zamknij');
define('cformTITLE', 'Wyślij swoją listę życzeń do przedstawiciela:');
define('cformSELDEALER_btn', 'Wybierz przedstawiciela');
define('cformSELDEALER_error', 'Należy wybrać przedstawiciela.');
define('cformDEALERNAME', 'Imię przedstawiciela *');
define('cformDEALERNAME_hint', 'Proszę wybrać przedstawiciela na mapie *');
define('cformDEALERNAME_error','Nazwa przedstawiciela jest obowiązkowa.');
define('cformDEALERADRESS', 'Adres przedstawiciela *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Adres przedstawiciela jest obowiązkowy.');
define('cformDEALERMAIL', 'Adres e-mail przedstawiciela *');
define('cformDEALERMAIL_hint', 'Proszę wybrać przedstawiciela na mapie albo wpisać jego e-mail*');
define('cformDEALERMAIL_error', 'Adres e-mail przedstawiciela jest obowiązkowy.');
define('cformNAME', 'Imię *');
define('cformNAME_hint', 'Proszę podać imię *');
define('cformNAME_error', 'Imię jest obowiązkowe.');
define('cformSURNAME', 'Nazwisko *');
define('cformSURNAME_hint', 'Proszę podać nazwisko *');
define('cformSURNAME_error', 'Nazwisko jest obowiązkowe.');
define('cformMAIL', 'E-mail *');
define('cformMAIL_hint', 'Proszę wpisać adres e-mail *');
define('cformMAIL_error', 'Prawidłowy adres e-mail jest obowiązkowy.');
define('cformPHONE', 'Nr telefonu');
define('cformPHONE_hint', 'Wpisz swój kontaktowy nr telefonu (opcja)');
define('cformMSG', 'Wiadomość');
define('cformMSG_hint', 'Tutaj można wpisać wiadomość tekstową (opcja)');
define('cformSEND_btn', 'Wyślij wiadomość');
define('cformNOTICE','Te pola są obowiązkowe.');
define('cform_POSTerror','Błąd podczas wysyłania formularza. Spróbuj ponownie.');
define('cform_CAPTCHAerror','reCaptcha nieprawidłowe lub niewprowadzone.');
define('cform_SUCCESS','Lista życzeń została wysłana prawidłowo.');
//-- Video_modal
define('vidmodal_TITLE', 'Sesja making-off Mediterranean');
define('vidmodal_LINKTOMODAL', 'Zobacz filmik making-off');
//--
define('furtherinfo', 'Więcej informacji można uzyskać u przedstawiciela SEAT');
define('selcar', 'Wybierze samochód');
define('prevcars', 'POPRZEDNIE MODELE');
define('newcars', 'AKTUALNE MODELE');
define('dealer', 'Gdzie kupić?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Wybierz produkt');
define('clistREF', 'Nr katalogowy');
define('clistPRO', 'Produkt');
define('clistSIZ', 'Rozmiar');
define('clistCOL', 'Kolor');
define('clistPRC', 'PVP');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Polityka cookies witryny z akcesoriami SEAT');
define('modal_txt11', 'Witryna z akcesoriami SEAT korzysta z plików cookies, aby ułatwić użytkownikowi poruszanie się w niej. Można zapoznać się z naszą ');
define('modal_txt12_href', 'polityką cookies tutaj');
define('modal_txt13', '.');
define('accept_btn', 'Akceptuj');
define('policy1', 'Czym są pliki cookies?');
define('policy2', 'Cookie to niewielkie pliki tekstowe instalujące się na komputerze, w przeglądarce internetowej użytkownika celem rejestrowania jego działań. Wysyłają anonimowy identyfikator zapisany w pliku, po to aby nawigowanie po witrynie było prostsze, na przykład umożliwiając zarejestrowanym wcześniej użytkownikom dostęp do stron, usług, promocji lub konkursów zarezerwowanych wyłącznie dla tych użytkowników, bez konieczności rejestrowania się przy każdej wizycie na stronie. Mogą także służyć do liczenia użytkowników, mierzenia parametrów ruchu i nawigacji w witrynie, długości sesji, a także kontrolowania postępów i liczby wejść.');
define('policy3', 'Witryna SEAT Akcesoria stosuje odpowiednie mechanizmy celem uzyskania zgody użytkownika na zainstalowanie plików cookie, które wymagają takiej zgody. Niemniej jednak należy mieć na względzie, iż zgodnie z prawem przyjmuje się, że (1) użytkownik udziela zgody, jeżeli zmienił ustawienia przeglądarki w taki sposób, że wyłączone są mechanizmy zapobiegające instalacji plików cookies oraz (2) zgoda, o której mowa powyżej, nie jest wymagana w przypadku instalacji tych plików cookies, które są konieczne do świadczenia usługi wyraźnie zamówionej przez użytkownika (poprzez uprzednie zarejestrowanie się).');
define('policy4', 'Wyłączenie plików cookie może spowodować zmianę w funkcjonowaniu witryny. Więcej informacji na ten temat można znaleźć w instrukcji obsługi używanej przeglądarki internetowej.');
define('policy5', '(1) W przypadku korzystania z przeglądarki Microsoft Internet Explorer, otworzyć menu Narzędzia, wybrać Opcje internetowe, a następnie zakładkę Prywatność.');
define('policy6', 'W przypadku przeglądarki Firefox na komputery Mac, wybrać menu Preferencje, następnie Prywatność i kliknąć przycisk Wyświetl ciasteczka. Natomiast w systemie Windows, wybrać menu Narzędzia, następnie Opcje, zakładka Prywatność i pod sekcją Historia zaznaczyć Program Firefox będzie używał ustawień użytkownika.');
define('policy7', 'W przeglądarce Safari wybrać menu Preferencje, a następnie zakładkę Prywatność.');
define('policy8', 'W przypadku przeglądarki Google Chrome, z menu Narzędzia wybrać Ustawienia (Preferencje na komputerze Mac), następnie Zaawansowane, w części Prywatność i bezpieczeństwo wybrać Ustawienia treści i tam kliknąć Pliki cookie.');
define('lDIS_1_1', 'Niniejsza lista wybranych produktów nie wiąże się z jakimkolwiek zobowiązaniem do nabycia ich i ma jedynie charakter informacyjny, dając możliwość dokończenia procesu zakupu, jeżeli użytkownik będzie tym zainteresowany, po uzgodnieniu z wybranym przedstawicielem firmy SEAT.');
define('lDIS_1_2', 'W chwili wybrania przedstawiciela marki SEAT, z którym użytkownik chce się skontaktować i u którego chce dokonać zakupu oraz montażu akcesoriów z listy życzeń, po podaniu danych osobowych, zostanie automatycznie wysłana wiadomość do wybranego przedstawiciela, aby skontaktował się z użytkownikiem i poinformował o wszystkich potrzebnych szczegółach.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Twoja lista życzeń jest pusta.');
//--toClient_mail()--
define('mcSubjectForClient', 'Potwierdzenie zamówienia listy życzeń');//***
//--toClient_mailContent()--//
define('mcH2', 'Szanowny Kliencie! ');//*
define('mcP1', 'Zespół Seat dziękuje za zainteresowanie gamą oryginalnych akcesoriów, udostępnianych przez naszą markę celem umożliwienia spersonalizowania swojego pojazdu.');//*
define('mcP2', 'W odpowiedzi na wyrażone zainteresowanie, w ciągu kilku najbliższych godzin pracownik działu akcesoriów u wybranego przedstawiciela ');//*
define('mcP3_1', ' skontaktuje się z Państwem celem ustalenia szczegółów procesu zakupu i montażu wybranych akcesoriów.');//*
define('mcP3_2', 'Poniżej podane są dane kontaktowe przedstawiciela na wypadek dodatkowych pytań lub wątpliwości oraz podsumowanie wybranych akcesoriów.');//*
define('mcP4_1', 'Nr telefonu ');
define('mcP4_2', 'Wybrane akcesoria') ;
define('mcP5', 'Serdecznie pozdrawiamy.');
define('mcP6', 'Oryginalne akcesoria SEAT.');
define('mcP7', 'ZASTRZEŻENIE PRAWNE: Niniejsza wiadomość zawiera informacje, których część lub całość może być poufna bądź chroniona prawnie. Adresowana jest wyłącznie do adresata. Jeżeli w wyniku błędnego nadania lub przekazania otrzymali Państwo niniejszą wiadomość nie będąc jej adresatem, uprzejmie prosimy o poinformowanie nadawcy o tym fakcie. Osoby niebędące adresatem niniejszej wiadomości nie mają prawa do wykorzystywania, przekazywania, dystrybuowania, drukowania, kopiowania czy rozpowszechniania jej treści w jakikolwiek sposób.');
?>	