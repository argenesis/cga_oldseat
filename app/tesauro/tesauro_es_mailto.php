<?php
//--html--//
define('mlREF', 'Referencia');
define('mlPRD', 'Producto');
define('mlPRC', 'Precio');
define('mlUT', 'Tiempo de Montaje (U.T.)');
define('mlDIS', '* Los precios indicados incluyen IVA y no incluyen costes de montaje, para más información contacte con su concesionario SEAT.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Mi lista de deseos');
define('mwlNAME', 'Nombre');
define('mwlPRC', 'Precio');
define('mwlTOT', 'Total');
define('mwlUNIT', 'Unidades');
define('mwlDIS', mlDIS);
define('mwlDIS_1_1', 'Esta lista de productos seleccionados no está vinculada ni supone compromiso de compra de los mismos, siendo únicamente de carácter informativo con la posibilidad de cerrar la operación si es de su interés  y una vez usted la haya acordado con el concesionario SEAT seleccionado.');
define('mwlDIS_1_2', 'En el momento que usted realiza la selección del concesionario SEAT donde estaría interesado en contactar para la información, adquisición y montaje de los accesorios incluídos en la lista de deseos,  y previa solicitud de sus datos personales, se emite automáticamente una comunicación al concesionario seleccionado para que contacte con usted y le informe sobre todos los detalles que necesite.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Se ha enviado una wishlist vacia.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Nueva petición desde Accesorios Originales SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Para el responsable del departamento de Recambios y Accesorios SEAT del concesionario ');
define('mP1', 'A través del Catálogo On-line de Accesorios Originales SEAT, el cliente ');
define('mP2', ' se ha mostrado interesado en la compra y montaje de los Accesorios relacionados en la lista que indicamos a continuación.');
define('mP3_1', 'Este cliente está esperando su llamada para concertar y ultimar los detalles de esta venta, por lo que es urgente que contacten con él lo antes posible para asegurar esta operación.');
define('mP3_2', 'A continuación le indicamos los datos de contacto del cliente:');
define('mP4_1', 'Esperamos que se confirme esta operación así como que esta novedad incorporada en el Catalogo de Accesorios On-line suponga más operaciones similares que sin duda aportarán beneficios en el desarrollo del negocio de Accesorios.');
define('mP4_2', 'Accesorios Seleccionados') ;
define('mP5', 'Saludos cordiales.');
define('mP6', 'Accesorios Originales SEAT.');
define('mP7', 'DISCLAIMER: Este mensaje contiene información propietaria de la cual parte o toda puede contener información confidencial o protegida legalmente. Esta exclusivamente destinado al usuario de destino. Si, por un error de envio o transmisión, ha recibido este mensaje y usted no es el destinatario del mismo, por favor, notifique de este hecho al remitente. Si no es el destinatario final de este mensaje no debe usar, informar, distribuir, imprimir, copiar o difundir este mensaje bajo ningún medio.');

?>