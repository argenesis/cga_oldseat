<?php
$_SESSION['lang'] = 'français'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Le produit n\’est pas distribué sur ce marché.');
define('aut_role', 'Vous ne disposez pas de suffisamment de privilèges pour accéder à ce service.');
define('model_mrkt', 'Le modèle sélectionné n\’est pas commercialisé sur ce marché.');
define('browser', 'Le navigateur n\’est pas compatible avec cette option.');
define('err_wlITEM', 'Ce champ ne peut pas être vide ou égal à zéro et il doit s\’agir d\’un nombre entier positif.'); //Wishlist - error on quantity
define('err_videoplayer', 'Votre navigateur ne prend pas en charge la lecture de ce format de fichier.');

//************************ TRANSLATION ****************************//
define('Head1', 'Catalogue général des Accessoires SEAT');
define('Head2', 'Catalogue général');
define('Head3', 'des Accessoires d\’origine');
define('Footer1', 'ACCESSOIRES D\’ORIGINE');
define('Footer2', ' - SEAT applique une politique de développement continu de ses produits et se réserve le droit de modifier leurs caractéristiques.');
define('Footer3', 'TECNOLOGIE À VIVRE');
define('Home1', 'Accessoires pour vous');
define('Home1alt', 'à l\’accueil SEAT Collection');
define('Home2', 'Accessoires pour votre voiture');
define('Home2alt', 'à l\’accueil SEAT Vehicles');
define('Col1alt', 'à SEAT Collection Ateca');
define('Col2alt', 'à SEAT Collection Essentials');
define('Col3alt', 'à SEAT Collection Motorsports');
define('Col4alt', 'à SEAT Collection Mediterranean');
define('cat1col1', 'Il n\’existe pas de produit pour ce marché.');
//--product view(coll&acc)
define('tabTXT', 'Produit');
define('tabDESP', 'Liste de pièces');
define('lREF', 'Référence');
define('lNEW', 'Nouveau !');
define('lSIZ', 'Taille');
define('lCOL', 'Couleur');
define('lPRD', 'Produit');
define('lPRC', 'PPC');
define('lUT', 'Temps de montage (U.T.)');
define('lDIS', '* Les prix indiqués incluent la TVA et n\’incluent pas les coûts de montage ; pour plus d\’informations contactez votre concessionnaire SEAT.');
define('lPRNT', 'Imprimer');
define('lCROSS', 'Cela peut également vous intéresser'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Sélectionnez votre produit');
define('listREF', 'Référence');
define('listPRO', 'Produit');
define('listUT', 'Temps de montage (U.T.)');
define('listPRC', 'PPC');
//--searchresults & searchbox
define('search1', 'Le produit dont la référence est');
define('search2', 'n\’a pas été trouvé');
define('search3', 'vers aucun modèle');
define('search4', 'a été trouvé');
define('search5', 'pour les modèles actuels suivants');
define('search6', 'vers modèle actuel');
define('search7', 'pour les modèles précédents suivants');
define('search8', 'vers modèle précédent');
define('search9', 'dans la Collection SEAT');
define('buscador','Nom ou référence');
//--wishlist(xxx_WL)
define('add_WL', 'Ajouter à la liste de souhaits');
define('view_WL', 'Voir la liste de souhaits');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Voir ma liste de souhaits');
define('wlNAME', 'Nom');
define('wlPRC', 'Prix');
define('wlTOT', 'Total');
define('wlUPDATE', 'Mettre à jour la liste');
define('wlEMPTY', 'Vider la liste');
define('wlEMPTIED', 'Liste de souhaits vidée');
define('wlPRNT', 'Imprimer la liste');
define('wlUNIT', 'Unités');
define('wlADD', 'Produit ajouté à la liste de souhaits.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Liste de souhaits');
define('wlTAB_CONTACTFORM', 'Envoyer la liste au concessionnaire');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Toutes');
define('mpTITLE', 'Sélectionnez votre distributeur');
define('mpCLOSE_btn', 'Fermer');
define('mpPROV', 'Sélectionnez votre région');
define('mpSEARCH_btn', 'Rechercher un concessionnaire');
define('mpCHOSE_btn', 'Sélectionner et fermer');
define('cformTITLE', 'Envoyer la liste de souhaits au concessionnaire :');
define('cformSELDEALER_btn', 'Sélectionner le concessionnaire');
define('cformSELDEALER_error', 'Veuillez sélectionner un concessionnaire.');
define('cformDEALERNAME', 'Nom du concessionnaire *');
define('cformDEALERNAME_hint', 'Veuillez sélectionner un concessionnaire sur la carte*');
define('cformDEALERNAME_error','Nom du concessionnaire obligatoire.');
define('cformDEALERADRESS', 'Adresse du concessionnaire *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Adresse du concessionnaire obligatoire.');
define('cformDEALERMAIL', 'Adresse électronique du concessionnaire *');
define('cformDEALERMAIL_hint', 'Veuillez sélectionner un concessionnaire sur la carte ou saisir son adresse e-mail*');
define('cformDEALERMAIL_error', 'Adresse e-mail du concessionnaire obligatoire.');
define('cformNAME', 'Prénom *');
define('cformNAME_hint', 'Veuillez saisir votre prénom *');
define('cformNAME_error', 'Nom obligatoire.');
define('cformSURNAME', 'Nom *');
define('cformSURNAME_hint', 'Veuillez saisir votre nom *');
define('cformSURNAME_error', 'Nom obligatoire');
define('cformMAIL', 'Email *');
define('cformMAIL_hint', 'Veuillez saisir votre adresse e-mail *');
define('cformMAIL_error', 'Adresse e-mail valide obligatoire.');
define('cformPHONE', 'Téléphone');
define('cformPHONE_hint', 'Veuillez saisir votre téléphone de contact (facultatif)');
define('cformMSG', 'Message');
define('cformMSG_hint', 'Si vous le souhaitez, saisissez un message de texte (facultatif)');
define('cformSEND_btn', 'Envoyer un message');
define('cformNOTICE','Ces champs sont obligatoires.');
define('cform_POSTerror','Erreur lors de l´envoi du formulaire. Veuillez réessayer.');
define('cform_CAPTCHAerror','reCaptcha invalide ou non saisi.');
define('cform_SUCCESS','La liste de souhaits a été envoyée avec succès.');
//-- Video_modal
define('vidmodal_TITLE', 'Making-off de la séance photo Mediterranean');
define('vidmodal_LINKTOMODAL', 'Voir la vidéo du Making-off');
//--
define('furtherinfo', 'Pour plus d\’informations contactez votre concessionnaire SEAT');
define('selcar', 'Sélectionnez votre voiture');
define('prevcars', 'MODÈLES ANTÉRIEURS');
define('newcars', 'MODÈLES ACTUELS');
define('dealer', 'Où acheter ?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Sélectionnez votre produit');
define('clistREF', 'Référence');
define('clistPRO', 'Produit');
define('clistSIZ', 'Taille');
define('clistCOL', 'Couleur');
define('clistPRC', 'PPC');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Politique de cookies de SEAT Accessoires');
define('modal_txt11', 'SEAT Accessoires utilise des cookies pour améliorer votre expérience de navigation sur notre site Web. Vous pouvez consulter notre ');
define('modal_txt12_href', 'politique de Cookies ici');
define('modal_txt13', '.');
define('accept_btn', 'Accepter');
define('policy1', 'Qu\’est-ce que les cookies ?');
define('policy2', 'Les cookies sont de petits fichiers textes qui s’installent dans le navigateur de l\’ordinateur de l\’utilisateur pour enregistrer son activité. Ils envoient une identification anonyme qu\’ils stockent dans le but de faciliter la navigation, en permettant, par exemple, l\’accès aux utilisateurs qui se sont enregistrés au préalable et l\’accès aux espaces, services, promotions ou concours qui leurs sont exclusivement réservés sans avoir à s\’enregistrer à chaque visite. Ils peuvent également être utilisés pour mesurer l\’audience, les paramètres de trafic et navigation, la durée de session, et/ou contrôler l\’avancement et le nombre d\’entrées.');
define('policy3', 'SEAT Accessoires tâchera à tout moment de mettre en place les mécanismes adéquats pour obtenir le consentement de l\’Utilisateur lors de l\’installation de cookies le requérant. Cependant, il faudra tenir compte du fait que, conformément à la loi, il sera considéré que (1) l\’Utilisateur a donné son consentement s\’il modifie la configuration du navigateur en désactivant les restrictions qui empêchent l\’entrée de cookies et (2) que ledit consentement n\’est pas nécessaire pour l\’installation des cookies strictement nécessaires à la prestation d\’un service expressément demandé par l\’Utilisateur (après enregistrement).');
define('policy4', 'Néanmoins, la désactivation de ces cookies pourrait modifier le fonctionnement du site Internet. Veuillez consulter les instructions et manuels de votre navigateur pour de plus amples informations.');
define('policy5', '(1) Si vous utilisez Microsoft Internet Explorer, dans le menu Outils, en sélectionnant Options d\’Internet puis l\’onglet Confidentialité.');
define('policy6', 'Si vous utilisez Firefox pour Mac, dans le menu Préférences, en sélectionnant Confidentialité, puis Afficher les cookies, et sur Firefox pour Windows dans le menu Outils, en sélectionnant Options, puis Vie privée et sécurité puis Utiliser les paramètres personnalisés pour l\’historique.');
define('policy7', 'Si vous utilisez Safari, dans le menu Préférences, sélectionner Confidentialité.');
define('policy8', 'Si vous utilisez Google Chrome, dans le menu Outils, en sélectionnant Options (Préférences sur Mac), puis Avancées et ensuite, dans l\’option Configuration de contenu de la section Confidentialité et enfin en sélectionnant Cookies dans la boîte de dialogue de Configuration de contenu.');
define('lDIS_1_1', 'Cette liste de produits sélectionnés ne vous contraint ni ne vous engage à l\’achat de ces produits. Elle est fournie à titre purement informatif avec la possibilité de réaliser l\’opération si vous le souhaitez et au moment convenu avec le concessionnaire SEAT sélectionné.');
define('lDIS_1_2', 'Lorsque vous choisissez le concessionnaire SEAT que vous souhaiteriez contacter pour recevoir des informations, acquérir et  monter les accessoires figurant dans la liste de souhaits et après demande de vos données personnelles, une communication automatique est envoyée au concessionnaire sélectionné afin qu\’il vous contacte et vous informe de tous les détails nécessaires.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Votre liste de souhaits est vide.');
//--toClient_mail()--
define('mcSubjectForClient', 'Confirmation de demande de liste de souhaits');//***
//--toClient_mailContent()--//
define('mcH2', 'Cher/Chère client(e) ');//*
define('mcP1', 'Chez SEAT, nous vous remercions de l\’intérêt que vous portez à la gamme d\’Accessoires d\’origine que notre Marque met à votre disposition pour personnaliser votre véhicule.');//*
define('mcP2', 'Pour répondre à l’intérêt que vous nous portez, dans les prochaines heures le responsable du service Accessoires du concessionnaire ');//*
define('mcP3_1', ' que vous avez sélectionné prendra contact avec vous pour finaliser les détails de l\’opération de vente et le montage des Accessoires sélectionnés.');//*
define('mcP3_2', 'Vous trouverez ci-dessous les coordonnées du concessionnaire pour toute question supplémentaire, ainsi qu\’un résumé des Accessoires sélectionnés.');//*
define('mcP4_1', 'Téléphone ');
define('mcP4_2', 'Accessoires sélectionnés') ;
define('mcP5', 'Cordialement.');
define('mcP6', 'Accessoires d\’origine SEAT.');
define('mcP7', 'AVERTISSEMENT : Ce message contient des informations personnelles dont tout ou partie peut contenir des informations confidentielles ou protégées légalement. Ces informations s\’adressent exclusivement au destinataire de ce message. Si vous n\’êtes pas le destinataire de ce message mais que vous l\’avez reçu suite à une erreur d\’envoi ou de transmission, veuillez en informer l\’expéditeur. Si vous n\’êtes pas le destinataire final de ce message, vous ne devez utiliser, informer, distribuer, imprimer, copier ou diffuser ce message d\’aucune manière.');
?>	