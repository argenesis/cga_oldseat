<?php
$_SESSION['lang'] = 'Deutsch'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Der Artikel wird auf diesem Markt nicht vertrieben.');
define('aut_role', 'Sie verfügen nicht über die erforderlichen Rechte, um auf diesen Service zuzugreifen.');
define('model_mrkt', 'Das gewählte Modell wird auf diesem Markt nicht vertrieben.');
define('browser', 'Diese Option wird vom Browser nicht unterstützt.');
define('err_wlITEM', 'Dieses Feld darf weder leer sein noch eine Null enthalten. Es muss eine Ganzzahl enthalten.'); //Wishlist - error on quantity
define('err_videoplayer', 'Die Wiedergabe dieses Dateiformats wird vom Browser nicht unterstützt.');

//************************ TRANSLATION ****************************//
define('Head1', 'SEAT Zubehör Gesamtkatalog');
define('Head2', 'Original Zubehör');
define('Head3', 'Gesamtkatalog');
define('Footer1', 'ORIGINAL ZUBEHÖR');
define('Footer2', ' - SEAT hat sich der kontinuierlichen Produktentwicklung verschrieben und behält es sich vor, die Spezifikationen zu ändern.');
define('Footer3', 'FREUDE DURCH TECHNIK');
define('Home1', 'Zubehör für Sie');
define('Home1alt', 'zur SEAT Kollektion Startseite');
define('Home2', 'Zubehör für Ihr Fahrzeug');
define('Home2alt', 'zur SEAT Fahrzeug Startseite');
define('Col1alt', 'zur SEAT Ateca Kollektion');
define('Col2alt', 'zur SEAT Essentials Kollektion');
define('Col3alt', 'zur SEAT Motorsports Kollektion');
define('Col4alt', 'zur SEAT Mediterranean Kollektion');
define('cat1col1', 'Für diesen Markt gibt es keine Artikel.');
//--product view(coll&acc)
define('tabTXT', 'Produkt');
define('tabDESP', 'Aufschlüsselung');
define('lREF', 'OT-Nr.');
define('lNEW', 'Neu!');
define('lSIZ', 'Größe');
define('lCOL', 'Farbe');
define('lPRD', 'Produkt');
define('lPRC', 'Unverbindliche Preisempfehlung');
define('lUT', 'Montagezeit (ZE)');
define('lDIS', '* Die angegebenen Preise verstehen sich inklusive MwSt. und ohne Montagekosten. Weitere Informationen erhalten Sie von Ihrem SEAT Händler.');
define('lPRNT', 'Drucken');
define('lCROSS', 'Das könnte Sie auch interessieren'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Artikel auswählen');
define('listREF', 'OT-Nr.');
define('listPRO', 'Produkt');
define('listUT', 'Montagezeit (ZE)');
define('listPRC', 'Unverbindliche Preisempfehlung');
//--searchresults & searchbox
define('search1', 'Produkt mit der Referenz');
define('search2', 'für kein');
define('search3', 'Modell gefunden');
define('search4', 'gefunden');
define('search5', 'für folgende aktuelle Modelle');
define('search6', 'für das aktuelle Modell');
define('search7', 'für folgende Vorgängermodelle');
define('search8', 'für das folgende Vorgängermodell');
define('search9', 'in der SEAT Kollektion');
define('buscador','Name oder Referenz');
//--wishlist(xxx_WL)
define('add_WL', 'Zur Wunschliste hinzufügen');
define('view_WL', 'Wunschliste anzeigen');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Meine Wunschliste');
define('wlNAME', 'Anzahl');
define('wlPRC', 'Preis');
define('wlTOT', 'Gesamt');
define('wlUPDATE', 'Liste aktualisieren');
define('wlEMPTY', 'Liste löschen');
define('wlEMPTIED', 'Wunschliste löschert');
define('wlPRNT', 'Liste ausdrucken');
define('wlUNIT', 'Einheiten');
define('wlADD', 'Der Artikel wurde zur Wunschliste hinzugefügt.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Wunschliste');
define('wlTAB_CONTACTFORM', 'Liste an Händler senden');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Alle');
define('mpTITLE', 'Händler auswählen');
define('mpCLOSE_btn', 'Schließen');
define('mpPROV', 'Region auswählen');
define('mpSEARCH_btn', 'Händlersuche');
define('mpCHOSE_btn', 'Auswählen und schließen');
define('cformTITLE', 'Wunschliste zum Händler senden:');
define('cformSELDEALER_btn', 'Händler auswählen');
define('cformSELDEALER_error', 'Sie müssen einen Händler auswählen.');
define('cformDEALERNAME', 'Name des Händlers *');
define('cformDEALERNAME_hint', 'Wählen Sie bitte einen Händler auf der Karte.*');
define('cformDEALERNAME_error','Der Händlername ist obligatorisch.');
define('cformDEALERADRESS', 'Anschrift des Händlers *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Die Anschrift des Händlers ist obligatorisch.');
define('cformDEALERMAIL', 'E-Mail-Adresse des Händlers *');
define('cformDEALERMAIL_hint', 'Wählen Sie bitte einen Händler auf der Karte oder geben Sie die E-Mail-Adresse des Händlers ein.*');
define('cformDEALERMAIL_error', 'Die E-Mail-Adresse des Händlers ist obligatorisch.');
define('cformNAME', 'Vorname *');
define('cformNAME_hint', 'Geben Sie bitte Ihren Vornamen ein. *');
define('cformNAME_error', 'Der Vorname ist obligatorisch.');
define('cformSURNAME', 'Nachname *');
define('cformSURNAME_hint', 'Geben Sie bitte Ihren Nachnamen ein. *');
define('cformSURNAME_error', 'Der Nachname ist obligatorisch.');
define('cformMAIL', 'E-Mail *');
define('cformMAIL_hint', 'Geben Sie bitte Ihre E-Mail-Adresse ein. *');
define('cformMAIL_error', 'Eine gültige E-Mail-Adresse ist obligatorisch.');
define('cformPHONE', 'Telefon');
define('cformPHONE_hint', 'Geben Sie bitte Ihre Telefonnummer ein (optional).');
define('cformMSG', 'Nachricht');
define('cformMSG_hint', 'Auf Wunsch können Sie eine Textnachricht eingeben (optional).');
define('cformSEND_btn', 'Nachricht senden');
define('cformNOTICE','Diese Felder sind obligatorisch.');
define('cform_POSTerror','Fehler beim Senden des Formulars. Bitte erneut versuchen.');
define('cform_CAPTCHAerror','reCaptcha ungültig oder nicht eingegeben.');
define('cform_SUCCESS','Die Wunschliste wurde korrekt versendet.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean Foto-Shooting: Making-Of');
define('vidmodal_LINKTOMODAL', 'Making Of-Video ansehen');
//--
define('furtherinfo', 'Weitere Informationen erhalten Sie bei Ihrem SEAT-Händler.');
define('selcar', 'Fahrzeug auswählen');
define('prevcars', 'VORGÄNGERMODELLE');
define('newcars', 'AKTUELLE MODELLE');
define('dealer', 'Verfügbarkeit prüfen');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Artikel auswählen');
define('clistREF', 'OT-Nr.');
define('clistPRO', 'Produkt');
define('clistSIZ', 'Größe');
define('clistCOL', 'Farbe');
define('clistPRC', 'Unverbindliche Preisempfehlung');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Die Cookie-Richtlinie von SEAT Zubehör');
define('modal_txt11', 'SEAT Zubehör verwendet Cookies, um Ihnen die Navigation auf unseren Webseiten zu erleichtern. Details zu unserer ');
define('modal_txt12_href', 'Cookie-Richtlinie finden Sie hier');
define('modal_txt13', '.');
define('accept_btn', 'Akzeptieren');
define('policy1', 'Was sind Cookies?');
define('policy2', 'Cookies sind kleine Textdateien, die im Browser des Nutzers gespeichert werden, um dessen Aktivität nachzuverfolgen. Hierzu wird eine anonyme Kennung gesendet und in dem Browser abgelegt mit dem Ziel, die Navigation zu erleichtern. Dies ermöglicht bereits registrierten Nutzern beispielsweise den Zugriff auf Bereiche, Services, Sonderaktionen oder Gewinnspiele, die exklusiv für sie reserviert sind, ohne dass bei jedem Besuch der Webseite eine erneute Registrierung erforderlich ist. Cookies können auch verwendet werden, um die Anzahl der Besucher, das Verkehrsaufkommen, das Navigationsverhalten, die Sitzungszeiten und/oder den Fortschritt sowie die Zahl der Seitenaufrufe nachzuhalten.');
define('policy3', 'SEAT Zubehör stellt jederzeit geeignete Mechanismen bereit, um die Zustimmung des Nutzers für die Installation von Cookies einzuholen, sofern erforderlich. Ungeachtet des Vorstehenden ist zu berücksichtigen, dass laut Gesetz Folgendes gilt: (1) Indem der Nutzer die Konfiguration des Browsers ändert und die Einschränkungen, die eine Nutzung von Cookies verhindern, aufhebt, erklärt er sich mit der Nutzung von Cookies einverstanden; (2) die besagte Zustimmung ist nicht erforderlich für die Installation von Cookies, die ausschließlich für die Erbringung einer Serviceleistung benötigt werden, die der Nutzer ausdrücklich (durch vorherige Registrierung) angefordert hat.');
define('policy4', 'Die Deaktivierung von Cookies kann jedoch die Ausführung von Funktion auf der Website beeinträchtigen. Weitere Informationen entnehmen Sie bitte den Nutzungshinweisen und Handbüchern Ihres Browsers.');
define('policy5', '(1) Wenn Sie Microsoft Internet Explorer verwenden, wählen Sie im Menü „Extras“ die Optionen „Internetoptionen“ und anschließend „Datenschutz“.');
define('policy6', 'Wenn Sie Firefox verwenden, wählen Sie auf einem Mac-PC im Menü „Einstellungen“ die Option „Datenschutz“ und anschließend „Cookies anzeigen“. Auf einem Windows-PC wählen Sie im Menü „Extras“ die Menüpunkte „Optionen“, „Datenschutz und Sicherheit“ und anschließend „Nach benutzerdefinierten Einstellungen anlegen“.');
define('policy7', 'Wenn Sie Safari verwenden, wählen Sie im Menü „Einstellungen“ die Option „Datenschutz“.');
define('policy8', 'Wenn Sie Google Chrome verwenden, wählen Sie die Menüpunkte „Extras“, „Optionen“ („Einstellungen“ in Mac) und „Erweitert“. Rufen Sie dann unter „Datenschutz und Sicherheit“ die Option „Inhaltseinstellungen“ auf und markieren Sie im Dialogfeld „Inhaltseinstellungen“ die Option „Cookies“.');
define('lDIS_1_1', 'Diese Liste ausgewählter Artikel ist nicht verbindlich und stellt keine Verpflichtung zum Kauf der Artikel dar. Sie dient ausschließlich informativen Zwecken und bietet die Möglichkeit zum Abschluss des Kaufs, sofern dies von Ihnen gewünscht wird. Der Kauf wird erst gültig nach entsprechender Vereinbarung mit dem SEAT Händler Ihrer Wahl.');
define('lDIS_1_2', 'In dem Moment, in dem Sie einen SEAT Händler für die Bereitstellung von Informationen und den Kauf sowie die Montage des in der Wunschliste enthaltenen Zubehörs ausgewählt und Ihre persönlichen Daten angegeben haben, erhält der gewählte Händler automatisch eine Benachrichtigung. Darin wird er aufgefordert, mit Ihnen Kontakt aufzunehmen und Sie über alle benötigten Details zu informieren.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Ihre Wunschliste ist leer.');
//--toClient_mail()--
define('mcSubjectForClient', 'Bestätigung Ihrer Wunschlisten-Anfrage');//***
//--toClient_mailContent()--//
define('mcH2', 'Sehr geehrter Kunde/Sehr geehrte Kundin, ');//*
define('mcP1', 'Seat bedankt sich für Ihr Interesse an dem Sortiment von Original-Zubehörteilen, das unsere Marke für die Personalisierung Ihres Fahrzeugs bereitstellt.');//*
define('mcP2', 'In Kürze wird der Leiter der Abteilung für Zubehörartikel des ');//*
define('mcP3_1', ' von Ihnen gewählten Händlers mit Ihnen Kontakt aufnehmen, um die Details bezüglich Kauf und Montage des gewählten Zubehörs zu besprechen.');//*
define('mcP3_2', 'Nachfolgend finden Sie die Kontaktdaten des Händlers für den Fall, dass Sie weitere Fragen haben, sowie eine Übersicht über die ausgewählten Zubehörartikel.');//*
define('mcP4_1', 'Telefon ');
define('mcP4_2', 'Ausgewählte Zubehörartikel') ;
define('mcP5', 'Mit freundlichen Grüßen.');
define('mcP6', 'SEAT Original Zubehör.');
define('mcP7', 'DISCLAIMER: Diese Nachricht enthält proprietäre Informationen, die möglicherweise in Teilen oder vollständig als vertraulich eingestuft und/oder rechtlich geschützt sind. Der Inhalt ist ausschließlich für den vorgesehenen Empfänger bestimmt. Wenn Sie nicht der richtige Empfänger sind oder diese E-Mail irrtümlich erhalten haben, informieren Sie bitte den Absender. Wenn Sie nicht der vorgesehene Empfänger sind, dürfen Sie diese Mitteilung auf keinerlei Art und Weise verwenden, weitergeben, ausdrucken, kopieren oder verbreiten.');
?>	