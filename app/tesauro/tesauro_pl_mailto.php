<?php
//--html--//
define('mlREF', 'Nr katalogowy');
define('mlPRD', 'Produkt');
define('mlPRC', 'Cena');
define('mlUT', 'Czas montażu (U.T.)');
define('mlDIS', '* Podane ceny zawierają podatek VAT ale nie obejmują kosztów montażu. Aby uzyskać więcej informacji, należy skontaktować się z przedstawicielem firmy SEAT.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Moja lista życzeń');
define('mwlNAME', 'Imię');
define('mwlPRC', 'Cena');
define('mwlTOT', 'Razem');
define('mwlUNIT', 'Jednostki');
define('mwlDIS', '* Podane ceny nie obejmują montażu.');
define('mwlDIS_1_1', 'Niniejsza lista wybranych produktów nie wiąże się z jakimkolwiek zobowiązaniem do nabycia ich i ma jedynie charakter informacyjny, dając możliwość dokończenia procesu zakupu, jeżeli użytkownik będzie tym zainteresowany, po uzgodnieniu z wybranym przedstawicielem firmy SEAT.');
define('mwlDIS_1_2', 'W chwili wybrania przedstawiciela marki SEAT, z którym użytkownik chce się skontaktować i u którego chce dokonać zakupu oraz montażu akcesoriów z listy życzeń, po podaniu danych osobowych, zostanie automatycznie wysłana wiadomość do wybranego przedstawiciela, aby skontaktował się z użytkownikiem i poinformował o wszystkich potrzebnych szczegółach.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Została wysłana pusta lista życzeń.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Nowe zamówienie od oryginalnych akcesoriów SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Do kierownika działu części zamiennych i akcesoriów w salonie przedstawiciela ');
define('mP1', 'Za pośrednictwem internetowego katalogu oryginalnych akcesoriów SEAT klient ');
define('mP2', ' zgłosił zainteresowanie zakupem i montażem akcesoriów wymienionych na poniższej liście.');
define('mP3_1', 'Klient oczekuje teraz na kontakt telefoniczny celem ustalenia szczegółów zakupu, dlatego należy skontaktować się z nim jak najszybciej, aby zamknąć transakcję.');
define('mP3_2', 'Poniżej podane są dane kontaktowe klienta:');
define('mP4_1', 'Ufamy, że zamówienie zostanie potwierdzone i że ta nowa opcja w internetowym katalogu akcesoriów generować będzie więcej podobnych transakcji, które bez wątpienia przyczynią się do dalszego rozwoju działalności w branży akcesoriów.');
define('mP4_2', 'Wybrane akcesoria') ;
define('mP5', 'Serdecznie pozdrawiamy.');
define('mP6', 'Oryginalne akcesoria SEAT.');
define('mP7', 'ZASTRZEŻENIE PRAWNE: Niniejsza wiadomość zawiera informacje, których część lub całość może być poufna bądź chroniona prawnie. Adresowana jest wyłącznie do adresata. Jeżeli w wyniku błędnego nadania lub przekazania otrzymali Państwo niniejszą wiadomość nie będąc jej adresatem, uprzejmie prosimy o poinformowanie nadawcy o tym fakcie. Osoby niebędące adresatem niniejszej wiadomości nie mają prawa do wykorzystywania, przekazywania, dystrybuowania, drukowania, kopiowania czy rozpowszechniania jej treści w jakikolwiek sposób.');
?>