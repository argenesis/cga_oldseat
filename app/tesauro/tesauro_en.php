<?php
$_SESSION['lang'] = 'English'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'The product is not distributed in this market.');
define('aut_role', 'You do not have enough privileges to access this service.');
define('model_mrkt', 'The selected model is not sold in this market.');
define('browser', 'The browser does not support this option.');
define('err_wlITEM', 'This field cannot be empty nor zero and must be a positive integer.'); //Wishlist - error on quantity
define('err_videoplayer', 'Your browser does not support playback of this file format.');

//************************ TRANSLATION ****************************//
define('Head1', 'SEAT Accessories General Catalogue');
define('Head2', 'Original Accessories');
define('Head3', 'General Catalogue');
define('Footer1', 'ORIGINAL ACCESSORIES');
define('Footer2', ' SEAT applies a continuous development policy to its products and reserves the right to make changes to specifications.');
define('Footer3', 'TECHNOLOGY FOR ENJOYING');
define('Home1', 'Accessories for you');
define('Home1alt', 'to SEAT Collection home');
define('Home2', 'Accessories for your car');
define('Home2alt', 'to SEAT Vehicles home');
define('Col1alt', 'to SEAT Collection Ateca');
define('Col2alt', 'to SEAT Collection Essentials');
define('Col3alt', 'to SEAT Collection Motorsports');
define('Col4alt', 'to SEAT Collection Mediterranean');
define('cat1col1', 'There are no products for this market.');
//--product view(coll&acc)
define('tabTXT', 'Product');
define('tabDESP', 'Parts');
define('lREF', 'Reference');
define('lNEW', 'New!');
define('lSIZ', 'Size');
define('lCOL', 'Colour');
define('lPRD', 'Product');
define('lPRC', 'RRP');
define('lUT', 'Installation Time (T.U.)');
define('lDIS', '* The prices include VAT and do not include installation costs. For more information please contact your SEAT dealer.');
define('lPRNT', 'Print');
define('lCROSS', 'Also of interest'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Select your product');
define('listREF', 'Reference');
define('listPRO', 'Product');
define('listUT', 'Installation Time (T.U.)');
define('listPRC', 'RRP');
//--searchresults & searchbox
define('search1', 'The product with reference no.');
define('search2', 'could not be found');
define('search3', 'to no model');
define('search4', 'has been found');
define('search5', 'for the following current models');
define('search6', 'to current model');
define('search7', 'for the following previous models');
define('search8', 'to previous model');
define('search9', 'in the SEAT Collection');
define('buscador','Name or reference');
//--wishlist(xxx_WL)
define('add_WL', 'Add to wish list');
define('view_WL', 'View wish list');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'My wish list');
define('wlNAME', 'Name');
define('wlPRC', 'Price');
define('wlTOT', 'Total');
define('wlUPDATE', 'Update list');
define('wlEMPTY', 'Clear list');
define('wlEMPTIED', 'Wish list emptied');
define('wlPRNT', 'Print list');
define('wlUNIT', 'Units');
define('wlADD', 'Product added to the wish list.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Wish list');
define('wlTAB_CONTACTFORM', 'Send list to dealer');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'All');
define('mpTITLE', 'Select your distributor');
define('mpCLOSE_btn', 'Close');
define('mpPROV', 'Select your province');
define('mpSEARCH_btn', 'Search for dealers');
define('mpCHOSE_btn', 'Select and close');
define('cformTITLE', 'Send your wish list to the dealer:');
define('cformSELDEALER_btn', 'Select dealer');
define('cformSELDEALER_error', 'Please select a dealer.');
define('cformDEALERNAME', 'Name of dealer *');
define('cformDEALERNAME_hint', 'Please select a dealer on the map *');
define('cformDEALERNAME_error', 'Dealer name required.');
define('cformDEALERADRESS', 'Dealer address *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Dealer address required *');
define('cformDEALERMAIL', 'Dealer e-mail address *');
define('cformDEALERMAIL_hint', 'Please select a dealer on the map or type its e-mail address *');
define('cformDEALERMAIL_error', 'Dealer e-mail address is required.');
define('cformNAME', 'Name *');
define('cformNAME_hint', 'Please enter your name *');
define('cformNAME_error', 'Name is required.');
define('cformSURNAME', 'Surname *');
define('cformSURNAME_hint', 'Please enter your surname *');
define('cformSURNAME_error', 'Surname is required.');
define('cformMAIL', 'E-mail *');
define('cformMAIL_hint', 'Please enter your e-mail address *');
define('cformMAIL_error', 'Valid e-mail required.');
define('cformPHONE', 'Telephone');
define('cformPHONE_hint', 'Enter your contact number (optional)');
define('cformMSG', 'Message');
define('cformMSG_hint', 'If you like you can enter a text message (optional)');
define('cformSEND_btn', 'Send message');
define('cformNOTICE','These fields are required.');
define('cform_POSTerror','Error sending form. Please try again.');
define('cform_CAPTCHAerror','reCaptcha invalid or not entered.');
define('cform_SUCCESS',' The wish list has been sent successfully.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-of');
define('vidmodal_LINKTOMODAL', 'See the Making-of video');
//--
define('furtherinfo', 'Contact your SEAT dealer for further information');
define('selcar', 'Select your car');
define('prevcars', 'PREVIOUS MODELS');
define('newcars', 'CURRENT MODELS');
define('dealer', 'Where to buy?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Select your product');
define('clistREF', 'Reference');
define('clistPRO', 'Product');
define('clistSIZ', 'Size');
define('clistCOL', 'Colour');
define('clistPRC', 'RRP');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'SEAT Accessories cookies policy');
define('modal_txt11', 'SEAT Accessories uses cookies to improve your experience browsing our websites. You can see our ');
define('modal_txt12_href', 'Cookies policy here');
define('modal_txt13', '.');
define('accept_btn', 'Accept');
define('policy1', 'What are cookies?');
define('policy2', 'Cookies are small text files that are installed in the user\'s browser to register their activity. They send an anonymous identification that is stored there to make browsing easier. For example, it allows users who have already signed up to log in and to access areas, services, promotions or contests reserved exclusively for them without having to sign up with each visit. They can also be used to measure visitors, traffic and browsing metrics, session time and/or monitor progress and number of accesses');
define('policy3', 'SEAT Accessories will always try to establish appropriate mechanisms to obtain the User\'s consent for installing cookies that require it. Notwithstanding the above, it must be taken into account that, according to law, it is understood that (1) the User has given their consent if they modify the browser\'s settings to disable the restrictions preventing the use of cookies and (2) that said consent will not be required for installing cookies that are strictly necessary for providing a service requested expressly by the User (through prior registration).');
define('policy4', 'However, disabling them may alter the functioning of the Website. Please see your browser\'s instruction and manuals for more information.');
define('policy5', '(1) If you use Microsoft Internet Explorer, under the Tools menu select Internet Options and then select Privacy.');
define('policy6', 'If you use Firefox, on Mac: select Privacy under the Preferences menu and open Show Cookies; on Windows: under the Tools menu, select Options, Privacy and select Use a customised configuration for the history.');
define('policy7', 'If you use Safari, under the Preferences menu, select Privacy.');
define('policy8', 'If you use Google Chrome, under the Tools menu, select Options (Preferences on Mac), select Advanced, then select the Content Settings option under the Privacy section, and mark Cookies in the Content Settings dialogue.');
define('lDIS_1_1', 'This list of selected products is not linked to nor does it imply an obligation to purchase. It is for information purposes only with the possibility of completing the operation if you are interested and once you have agreed on it with the selected SEAT dealer.');
define('lDIS_1_2', 'When you select the SEAT dealer that you are interested to contact for information, purchase and installation of the accessories in the wish list, and after providing your personal details, a communication is submitted automatically to the selected dealer so that they can contact you and advise you on the details you need.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'You wish list is empty.');
//--toClient_mail()--
define('mcSubjectForClient', 'Confirmation of wish list request');//***
//--toClient_mailContent()--//
define('mcH2', 'Dear Customer ');//*
define('mcP1', 'At Seat we would like to thank you for your interest in the range of Original Accessories that the brand offers for customising your vehicle.');//*
define('mcP2', 'Based on your interest, the Accessories department representative from the dealer ');//*
define('mcP3_1', ' that you have selected will contact you shortly to finalise the details of the purchase and installation of the selected Accessories.');//*
define('mcP3_2', 'Please find below the contact details of the dealer for any additional queries that you may have, as well as a summary of the selected Accessories.');//*
define('mcP4_1', 'Telephone ');
define('mcP4_2', 'Selected Accessories') ;
define('mcP5', 'Kind Regards,');
define('mcP6', 'SEAT Original Accessories');
define('mcP7', 'DISCLAIMER: This message contains proprietary information, all or part of which may contain confidential or legally protected information. It is exclusively intended for the target user. If you have received this message due to a submission error and are not the recipient, please notify us immediately. If you are not the final recipient of this message you must not use, inform, distribute, print, copy or disclose this message by any means.');
?>	