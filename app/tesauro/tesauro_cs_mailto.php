<?php
//--html--//
define('mlREF', 'Označení');
define('mlPRD', 'Produkt');
define('mlPRC', 'Cena');
define('mlUT', 'Čas potřebný k montáži (časová jednotka)');
define('mlDIS', '* Uvedené ceny zahrnují DPH, nezahrnují náklady na montáž. Potřebujete-li další informace, obraťte se na svého prodejce SEAT.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Můj seznam přání');
define('mwlNAME', 'Jméno');
define('mwlPRC', 'Cena');
define('mwlTOT', 'Celkem');
define('mwlUNIT', 'Počet');
define('mwlDIS', '* Uvedené ceny nezahrnují montáž.');
define('mwlDIS_1_1', 'Tento seznam vybraných produktů není závazný ani nepředpokládá závazek k jejich zakoupení. Má pouze informativní charakter a nabízí možnost uzavřít nákupní operaci, pokud o uzavření máte zájem, a po dohodě s vybraným prodejcem SEAT.');
define('mwlDIS_1_2', 'Ve chvíli, kdy učiníte výběr prodejce SEAT, kterého byste měli zájem kontaktovat za účelem poskytnutí informací, zakoupení a montáže příslušenství, které máte v seznamu přání, a po vyžádání vašich osobních údajů, bude automaticky odesláno sdělení vybranému prodejci, aby vás kontaktoval a informoval o všech podrobnostech, které byste mohli potřebovat.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Byl odeslán prázdný seznam přání.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Nová žádost z: Originální příslušenství SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Pro vedoucího oddělení náhradních dílů a příslušenství SEAT u daného prodejce ');
define('mP1', 'Prostřednictvím online katalogu originálních příslušenství SEAT zákazník ');
define('mP2', ' vyjádřil zájem o nákup a montáž příslušenství, seznam uvádíme níže.');
define('mP3_1', 'Tento zákazník čeká na vaši výzvu k zajištění a domluvení podrobností této transakce, proto je velmi důležité, abyste jej kontaktovali co nejdříve, a tím zajistili uskutečnění této operace.');
define('mP3_2', 'Níže uvádíme kontaktní údaje zákazníka:');
define('mP4_1', 'Doufáme, že tato operace bude potvrzena a že tato nová funkce zahrnutá do online katalogu příslušenství pomůže k uzavření dalších podobných operací, což bezesporu bude mít výhody pro rozvoj obchodu s příslušenstvím.');
define('mP4_2', 'Vybrané příslušenství') ;
define('mP5', 'Se srdečným pozdravem');
define('mP6', 'Originální příslušenství SEAT.');
define('mP7', 'PRÁVNÍ UPOZORNĚNÍ: Tato zpráva obsahuje informace o vlastnických právech, z nichž část nebo všechny mohou obsahovat důvěrné nebo legálně chráněné informace. Je určena výhradně konkrétnímu uživateli. Pokud jste kvůli chybě v odesílání nebo přenosu tuto zprávu obdrželi a nejste jejím adresátem, sdělte, prosím, tuto skutečnost odesílateli. Pokud nejste konečným příjemcem této zprávy, nesmíte tuto zprávu použít, sdělovat, distribuovat, tisknout, kopírovat nebo šířit žádnými prostředky.');

?>