<?php
$_SESSION['lang'] = 'português'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'O produto não é distribuído neste mercado.');
define('aut_role', 'Não tem privilégios suficientes para aceder a este serviço.');
define('model_mrkt', 'O modelo selecionado não é comercializado neste mercado.');
define('browser', 'O navegador não é compatível com esta opção.');
define('err_wlITEM', 'Este campo não pode estar vazio, nem a zero e deve ser um número inteiro positivo.'); //Wishlist - error on quantity
define('err_videoplayer', 'O seu navegador não é compatível com a reprodução deste formato de ficheiro.');

//************************ TRANSLATION ****************************//
define('Head1', 'Catálogo geral de acessórios SEAT');
define('Head2', 'Catálogo geral');
define('Head3', 'de acessórios originais');
define('Footer1', 'ACESSÓRIOS ORIGINAIS');
define('Footer2', ' - A SEAT aplica uma política de desenvolvimento contínuo dos seus produtos e reserva-se o direito de efetuar alterações nas especificações.');
define('Footer3', 'TECHNOLOGY TO ENJOY');
define('Home1', 'Acessórios para si');
define('Home1alt', 'para SEAT Collection home');
define('Home2', 'Acessórios para o seu veículo');
define('Home2alt', 'para SEAT Vehicles home');
define('Col1alt', 'para SEAT Collection Ateca');
define('Col2alt', 'para SEAT Collection Essentials');
define('Col3alt', 'para SEAT Collection Motorsports');
define('Col4alt', 'para SEAT Collection Mediterranean');
define('cat1col1', 'Não existem produtos para este mercado.');
//--product view(coll&acc)
define('tabTXT', 'Produto');
define('tabDESP', 'Peças');
define('lREF', 'Referência');
define('lNEW', 'Novo!');
define('lSIZ', 'Tamanho');
define('lCOL', 'Cor');
define('lPRD', 'Produto');
define('lPRC', 'PVP');
define('lUT', 'Tempo de montagem (U.T.)');
define('lDIS', '* Os preços indicados incluem IVA e não incluem custos de montagem. Para mais informações, contacte o seu concessionário SEAT.');
define('lPRNT', 'Imprimir');
define('lCROSS', 'Pode também interessar-lhe'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Selecione o seu produto');
define('listREF', 'Referência');
define('listPRO', 'Produto');
define('listUT', 'Tempo de montagem (U.T.)');
define('listPRC', 'PVP');
//--searchresults & searchbox
define('search1', 'O produto com referência');
define('search2', 'não foi encontrado');
define('search3', 'para nenhum modelo');
define('search4', 'foi encontrado');
define('search5', 'para os seguintes modelos atuais');
define('search6', 'para o modelo atual');
define('search7', 'para os seguintes modelos anteriores');
define('search8', 'para o modelo anterior');
define('search9', 'na SEAT Collection');
define('buscador','Nome ou referência');
//--wishlist(xxx_WL)
define('add_WL', 'Adicionar à wishlist');
define('view_WL', 'Ver wishlist');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'A minha lista de desejos');
define('wlNAME', 'Nome');
define('wlPRC', 'Preço');
define('wlTOT', 'Total');
define('wlUPDATE', 'Atualizar lista');
define('wlEMPTY', 'Limpar lista');
define('wlEMPTIED', 'Lista de desejos esvaziada');
define('wlPRNT', 'Imprimir lista');
define('wlUNIT', 'Unidades');
define('wlADD', 'Produto adicionado à lista de desejos.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Lista de desejos');
define('wlTAB_CONTACTFORM', 'Enviar lista para concessionário');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Todas');
define('mpTITLE', 'Selecione o seu distribuidor');
define('mpCLOSE_btn', 'Fechar');
define('mpPROV', 'Selecione a sua região');
define('mpSEARCH_btn', 'Procurar concessionário');
define('mpCHOSE_btn', 'Selecionar e fechar');
define('cformTITLE', 'Enviar a sua lista de desejos ao concessionário:');
define('cformSELDEALER_btn', 'Selecionar concessionário');
define('cformSELDEALER_error', 'Deve selecionar um concessionário.');
define('cformDEALERNAME', 'Nome do concessionário *');
define('cformDEALERNAME_hint', 'Selecione um concessionário no mapa*');
define('cformDEALERNAME_error','Nome do concessionário de preenchimento obrigatório.');
define('cformDEALERADRESS', 'Endereço do concessionário *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Endereço do concessionário de preenchimento obrigatório.');
define('cformDEALERMAIL', 'Endereço de e-mail do concessionário *');
define('cformDEALERMAIL_hint', 'Selecione um concessionário no mapa ou escreva o e-mail do mesmo*');
define('cformDEALERMAIL_error', 'E-mail do concessionário de preenchimento obrigatório.');
define('cformNAME', 'Nome *');
define('cformNAME_hint', 'Introduza o seu nome *');
define('cformNAME_error', 'Nome de preenchimento obrigatório.');
define('cformSURNAME', 'Apelidos *');
define('cformSURNAME_hint', 'Introduza os seus apelidos *');
define('cformSURNAME_error', 'Apelidos de preenchimento obrigatório.');
define('cformMAIL', 'E-mail *');
define('cformMAIL_hint', 'Introduza o seu endereço de e-mail *');
define('cformMAIL_error', 'Endereço de e-mail válido de preenchimento obrigatório.');
define('cformPHONE', 'Número de telefone');
define('cformPHONE_hint', 'Introduza o seu número de telefone de contacto (opcional)');
define('cformMSG', 'Mensagem');
define('cformMSG_hint', 'Caso pretenda, introduza uma mensagem de texto (opcional)');
define('cformSEND_btn', 'Enviar mensagem');
define('cformNOTICE','Estes campos são de preenchimento obrigatório.');
define('cform_POSTerror','Erro no envio do formulário. Tente novamente.');
define('cform_CAPTCHAerror','reCaptcha inválido ou não introduzido.');
define('cform_SUCCESS','A lista de desejos foi enviada corretamente.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-off');
define('vidmodal_LINKTOMODAL', 'Ver o vídeo do Making-off');
//--
define('furtherinfo', 'Para mais informações, entre em contacto com o seu concessionário SEAT');
define('selcar', 'Selecione o seu veículo');
define('prevcars', 'MODELOS ANTERIORES');
define('newcars', 'MODELOS ACTUAIS');
define('dealer', 'Onde comprar?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Selecione o seu produto');
define('clistREF', 'Referência');
define('clistPRO', 'Produto');
define('clistSIZ', 'Tamanho');
define('clistCOL', 'Cor');
define('clistPRC', 'PVP');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'A política de Cookies da SEAT Acessórios');
define('modal_txt11', 'A SEAT Acessórios utiliza cookies para melhorar a sua experiência de navegação nos nossos ambientes Web. Pode consultar a nossa ');
define('modal_txt12_href', 'política de Cookies aqui');
define('modal_txt13', '.');
define('accept_btn', 'Aceitar');
define('policy1', 'O que são cookies?');
define('policy2', 'As cookies são pequenos ficheiros de texto instalados no navegador do computador do Utilizador para registar a sua atividade, enviando uma identificação anónima que se armazena no mesmo, com o objetivo de que a navegação seja mais simples, permitindo (por exemplo) o acesso aos Utilizadores que se tenham registado anteriormente, bem como o acesso às áreas, serviços, promoções ou concursos reservados exclusivamente para os referidos Utilizadores sem que tenham de se registar em cada visita. Também podem ser utilizadas para avaliar a audiência, os parâmetros de tráfego e navegação, tempo de sessão e/ou controlar o progresso e o número de entradas.');
define('policy3', 'A SEAT Acessórios irá, constantemente, procurar estabelecer mecanismos adequados para obter o consentimento do Utilizador para a instalação das cookies necessárias. Não obstante a disposição anterior, é necessário que se tenha em conta que, em conformidade com a Lei, entender-se-á que (1) o Utilizador deu o seu consentimento em caso de modificação de configuração do navegador, se desativar as restrições que impeçam a entrada de cookies e (2) que o referido consentimento não será necessário para a instalação das cookies que sejam estritamente necessárias para o fornecimento de um serviço expressamente solicitado pelo Utilizador (mediante registo prévio).');
define('policy4', 'No entanto, a desativação das mesmas poderia modificar o funcionamento do Website. Consulte as instruções e manuais do seu navegador para obter mais informações.');
define('policy5', '(1) Se utilizar o Microsoft Internet Explorer, na opção do menu Ferramentas, selecionado Opções da Internet e acedendo a Privacidade.');
define('policy6', 'Se utilizar o Firefox: para Mac, na opção do menu Preferências, selecionando Privacidade, acedendo à secção Mostrar Cookies; e para Windows, na opção do menu Ferramentas, selecionando Opções, acedendo a Privacidade e, em seguida, Utilizar definições personalizadas para o histórico.');
define('policy7', 'Se utilizar o Safari, na opção do menu Preferências, selecionando Privacidade.');
define('policy8', 'Se utilizar o Google Chrome, na opção do menu Ferramentas, selecionando Definições (Preferências, em Mac), acedendo a Avançadas e, em seguida, à opção Definições de conteúdo da secção Privacidade e segurança, marcando por fim Cookies, na caixa de diálogo Definições de conteúdo.');
define('lDIS_1_1', 'Esta lista de produtos selecionados não é vinculativa nem implica um compromisso de compra dos referidos produtos, tendo um carácter meramente informativo com a possibilidade de encerrar a operação, se tal for do seu interesse, e depois de a ter acordado com o concessionário SEAT selecionado.');
define('lDIS_1_2', 'No momento em que seleciona o concessionário SEAT no qual estaria interessado em contactar para obter informações, efetuar a aquisição e solicitar a montagem dos acessórios incluídos na lista de desejos e no pedido anterior dos seus dados pessoais, é automaticamente emitida uma comunicação ao concessionário selecionado para que este o contacte e o informe sobre todos os dados de que necessita.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'A sua wishlist está vazia.');
//--toClient_mail()--
define('mcSubjectForClient', 'Confirmação de pedido de Wishlist');//***
//--toClient_mailContent()--//
define('mcH2', 'Estimado(a) Cliente ');//*
define('mcP1', 'A SEAT agradece o seu interesse na gama de Acessórios Originais que a nossa marca lhe disponibiliza para a personalização do seu veículo.');//*
define('mcP2', 'Com base no seu interesse, nas próximas horas o responsável do departamento de Acessórios do concessionário ');//*
define('mcP3_1', ' que selecionou, irá contactá-lo para ultimar os detalhes da operação de venda e montagem dos Acessórios selecionados.');//*
define('mcP3_2', 'Em seguida, indicamos-lhe os dados de contacto do concessionário para qualquer questão adicional que pretenda efetuar, bem como um resumo dos Acessórios selecionados.');//*
define('mcP4_1', 'Número de telefone ');
define('mcP4_2', 'Acessórios selecionados') ;
define('mcP5', 'Com os melhores cumprimentos.');
define('mcP6', 'Acessórios Originais SEAT.');
define('mcP7', 'DISCLAIMER: Esta mensagem contém informações de propriedade, nas quais podem existir informações confidenciais ou protegidas legalmente. Está exclusivamente destinado ao utilizador de destino. Se, devido a um erro de envio ou transmissão, tiver recebido esta mensagem e não for o destinatário do mesmo, notifique o remetente de imediato. Caso não seja o destinatário final desta mensagem, não deve utilizar, informar, distribuir, imprimir, copiar ou difundir esta mensagem através de qualquer meio.');
?>