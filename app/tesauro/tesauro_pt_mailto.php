<?php
//--html--//
define('mlREF', 'Referência');
define('mlPRD', 'Produto');
define('mlPRC', 'Preço');
define('mlUT', 'Tempo de montagem (U.T.)');
define('mlDIS', '* Os preços indicados incluem IVA e não incluem custos de montagem. Para mais informações, contacte o seu concessionário SEAT.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'A minha lista de desejos');
define('mwlNAME', 'Nome');
define('mwlPRC', 'Preço');
define('mwlTOT', 'Total');
define('mwlUNIT', 'Unidades');
define('mwlDIS', '* Os preços indicados não incluem a montagem.');
define('mwlDIS_1_1', 'Esta lista de produtos selecionados não é vinculativa nem implica um compromisso de compra dos referidos produtos, tendo um carácter meramente informativo com a possibilidade de encerrar a operação, se tal for do seu interesse, e depois de a ter acordado com o concessionário SEAT selecionado.');
define('mwlDIS_1_2', 'No momento em que seleciona o concessionário SEAT no qual estaria interessado em contactar para obter informações, efetuar a aquisição e solicitar a montagem dos acessórios incluídos na lista de desejos e no pedido anterior dos seus dados pessoais, é automaticamente emitida uma comunicação ao concessionário selecionado para que este o contacte e o informe sobre todos os dados de que necessita.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Foi enviada uma wishlist vazia.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Novo pedido a partir de Acessórios Originais SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Para o responsável do departamento de Peças de Substituição e Acessórios SEAT do concessionário ');
define('mP1', 'Através do Catálogo Online de Acessórios Originais SEAT, o cliente ');
define('mP2', ' mostrou-se interessado na compra e montagem dos Acessórios relacionados na lista que indicamos em seguida.');
define('mP3_1', 'Este cliente aguarda a sua chamada para encerrar e ultimar os detalhes desta venda, pelo que é urgente contactá-lo o mais rapidamente possível para assegurar esta operação.');
define('mP3_2', 'Em seguida, indicar-lhe-emos os dados de contacto do cliente:');
define('mP4_1', 'Esperamos que esta operação se confirme e também que esta novidade incluída no Catálogo de Acessórios Online implique outras operações semelhantes que, sem dúvida, tratarão benefícios ao desenvolvimento do negócio de Acessórios.');
define('mP4_2', 'Acessórios selecionados') ;
define('mP5', 'Com os melhores cumprimentos.');
define('mP6', 'Acessórios Originais SEAT.');
define('mP7', 'DISCLAIMER: Esta mensagem contém informações de propriedade, nas quais podem existir informações confidenciais ou protegidas legalmente. Está exclusivamente destinado ao utilizador de destino. Se, devido a um erro de envio ou transmissão, tiver recebido esta mensagem e não for o destinatário do mesmo, notifique o remetente de imediato. Caso não seja o destinatário final desta mensagem, não deve utilizar, informar, distribuir, imprimir, copiar ou difundir esta mensagem através de qualquer meio.');
?>