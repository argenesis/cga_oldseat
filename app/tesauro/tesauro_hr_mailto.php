<?php
//--html--//
define('mlREF', 'Referenca');
define('mlPRD', 'Proizvod');
define('mlPRC', 'Cijena');
define('mlUT', 'Vrijeme ugradnje (U.T.)');
define('mlDIS', '* U navedene cijene uključen je PDV, ali nisu uključeni troškovi ugradnje. Za više informacija obratite se svojem SEAT koncesionaru.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'Moja lista želja');
define('mwlNAME', 'Ime');
define('mwlPRC', 'Cijena');
define('mwlTOT', 'Ukupno');
define('mwlUNIT', 'Jedinica');
define('mwlDIS', '* Navedene cijene ne uključuju ugradnju.');
define('mwlDIS_1_1', 'Ovaj popis odabranih proizvoda nije povezana s obvezom njihove kupnje niti podrazumijeva njihovu kupnju, već je isključivo informativnog karaktera te uključuje mogućnost kupnje ako vi to želite i ako ste to dogovorili sa svojim odabranim SEAT koncesionarom.');
define('mwlDIS_1_2', 'Kad odaberete SEAT koncesionara kojeg želite kontaktirati kako biste dobili više informacija, kupili ili montirali dodatnu opremu uključenu u listu želja te kad dostavite svoje osobne podatke, odabranom koncesionaru automatski se šalje obavijest da stupi u kontakt s vama i uputi vas u sve pojedinosti koje su vam potrebne.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'Poslali ste praznu Listu želja.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Novi zahtjev iz Odjela za dodatnu opremu SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Za odgovornog zaposlenika odjela koncesionara za zamjenske dijelove i dodatnu opremu SEAT ');
define('mP1', 'Putem internetskog kataloga dodatne opreme SEAT klijent ');
define('mP2', ' je iskazao interes za kupnju i montiranje dodatne opreme navedene na popisu koji se nalazi u nastavku.');
define('mP3_1', 'Klijent očekuje vaš poziv kako biste dogovorili pojedinosti te kupnje pa je važno da što je prije moguće stupite s njim u kontakt kako biste osigurali prodaju.');
define('mP3_2', 'U nastavku su navedeni podaci za kontakt klijenta:');
define('mP4_1', 'Nadamo se da će transakcija biti potvrđena, kao i da će ova novost uključena u internetski katalog dodatne opreme uključivati više sličnih transakcija koje će nedvojbeno dovesti do koristi za razvoj poslovanja odjela dodatne opreme.');
define('mP4_2', 'Odabrana dodatna oprema') ;
define('mP5', 'Srdačan pozdrav');
define('mP6', 'Originalna dodatna oprema SEAT');
define('mP7', 'IZJAVA O ODRICANJU OD ODGOVORNOSTI: Ova poruka sadržava povjerljive informacije koje u cijelosti ili djelomično mogu biti povjerljive ili zaštićene zakonom. Namijenjena je isključivo primatelju navedenom u adresi. Ako ste greškom primili ovu poruku i vi niste primatelj kojem je namijenjena, molimo da o tome obavijestite pošiljatelja. Ako niste primatelj kojem je ova poruka namijenjena, ni na koji je način ne smijete upotrijebiti, objaviti, distribuirati, ispisivati, kopirati ili širiti.');
?>