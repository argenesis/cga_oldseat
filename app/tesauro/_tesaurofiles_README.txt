2017/11/21 - 13:30
@author Argenis Urribarri<argenis@snconsultors.es>

################################################################################
     Configuración de un nuevo idioma - Catálogo General de Accesorios
################################################################################

$_SESSION['lang'] = ;
    Es la ETIQUETA del idioma que se muestra en la barra como idioma activo.
    Debe asignarse un valor en el idioma (ej: el idioma 'español' no debe tener
    'spanish', sino 'español'), y este debe estar en minusculas. La conversion
    a mayusculas ya se realiza donde es necesario.

######## ON ERROR MESSAGE TRANSLATION ########
Estos valores se usan en "/public/js/commonviews.js" para informar de errores.
Sigue una estructura como la siguiente:
    [tipo de error]    [mensaje de error]
    define('prd_mrkt', 'The product it\'s not avaliable for this market.');

######## TRANSLATION ########
Las etiquetas dentro de este apartado GENERALMENTE estan agrupadas por vistas.

    //--product view(coll&acc)
        Etiquetas en las vistas de producto, tanto para ACCESORIOS como COLLECTION.
    //--ctg_list: column labels
        Etiquetas de columnas en vista tipo lista para ACCESORIOS.
    //--searchresults & searchbox
        Etiquetas para vista de resultados de busqueda y el campo de buscador.
    //--wishlist(xxx_WL)
        Etiquetas para los botones relacionados con la wishlist DESDE la vista de
        producto ACCESORIO o COLLECTION.
    //--wishlist WINDOW(wlXXX)
        Etiquetas de la ventana emergente que enseña la wishlist.
    //--mapprovinces(mp_): contactform(cform_)
        Etiquetas relacionadas con el apartado de 'mapa' y 'contacto' de la wishlist.
    //-- Video_modal
        Etiquetas relacionadas con la ventana modal de video.
    //--
        Otras etiquetas. Puede que existan valores que ya no se utilicen en este
        apartado[pendientes de revision].
    ##### TRANSLATION - COLLECTION #####
    //--ctg list: column labels
        Etiquetas de columnas en vista tipo lista para COLLECTION.

######## COOKIES - POLICY ########
Agrupa todos los valores para las etiquetas utilizadas en el apartado de politica
de cookies.

######## MAIL-TO-CLIENT ########
Agrupa todos los valores para las etiquetas utilizadas en el envio de correo electronico
a cliente.
Los comentarios englobados en este apartado son el nombre del metodo que se utiliza
en el modelo mailingModel.php para tener la referencia de donde se utilizan.