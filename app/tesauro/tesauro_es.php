<?php
$_SESSION['lang'] = 'español'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'El producto no se distribuye en este mercado.');
define('aut_role', 'No tiene suficientes privilegios para acceder a este servicio.');
define('model_mrkt', 'El modelo seleccionado no se comercializa en este mercado.');
define('browser', 'El navegador no soporta esta opción.');
define('err_wlITEM', 'Este campo no puede estar vacío ni a cero y debe ser un entero positivo.'); //Wishlist - error on quantity
define('err_videoplayer', 'Su navegador no soporta la reproducción de este formato de archivo.');

//************************ TRANSLATION ****************************//
define('Head1', 'Catálogo General de Accesorios SEAT');
define('Head2', 'Catálogo General');
define('Head3', 'de Accesorios Originales');
define('Footer1', 'ACCESORIOS ORIGINALES');
define('Footer2', ' - SEAT aplica una política de continuo desarrollo de sus productos y se reserva el derecho de realizar cambios en las especificaciones.');
define('Footer3', 'TECNOLOG&Iacute;A PARA DISFRUTAR');
define('Home1', 'Accesorios para ti');
define('Home1alt', 'a SEAT Collection home');
define('Home2', 'Accesorios para tu coche');
define('Home2alt', 'to SEAT Vehicles home');
define('Col1alt', 'to SEAT Collection Ateca');
define('Col2alt', 'to SEAT Collection Essentials');
define('Col3alt', 'to SEAT Collection Motorsports');
define('Col4alt', 'to SEAT Collection Mediterranean');
define('cat1col1', 'No hay productos para este mercado.');
//--product view(coll&acc)
define('tabTXT', 'Producto');
define('tabDESP', 'Despieces');
define('lREF', 'Referencia');
define('lNEW', '¡Nuevo!');
define('lSIZ', 'Talla');
define('lCOL', 'Color');
define('lPRD', 'Producto');
define('lPRC', 'PVP');
define('lUT', 'Tiempo de Montaje (U.T.)');
define('lDIS', '* Los precios indicados incluyen IVA y no incluyen costes de montaje, para más información contacte con su concesionario SEAT.');
define('lPRNT', 'Imprimir');
define('lCROSS', 'También puede interesarte'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Selecciona tu producto');
define('listREF', 'Referencia');
define('listPRO', 'Producto');
define('listUT', 'Tiempo de Montaje (U.T.)');
define('listPRC', 'PVP');
//--searchresults & searchbox
define('search1', 'El producto con referencia');
define('search2', 'no se encontró');
define('search3', 'a ningún modelo');
define('search4', 'se ha encontrado');
define('search5', 'para los siguientes modelos actuales');
define('search6', 'a modelo actual');
define('search7', 'para los siguientes modelos anteriores');
define('search8', 'a modelo anterior');
define('search9', 'en SEAT Collection');
define('buscador','Nombre o referencia');
//--wishlist(xxx_WL)
define('add_WL', 'Añadir a wishlist');
define('view_WL', 'Ver wishlist');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Mi lista de deseos');
define('wlNAME', 'Nombre');
define('wlPRC', 'Precio');
define('wlTOT', 'Total');
define('wlUPDATE', 'Actualizar lista');
define('wlEMPTY', 'Vaciar lista');
define('wlEMPTIED', 'Lista de deseos vaciada');
define('wlPRNT', 'Imprimir lista');
define('wlUNIT', 'Unidades');
define('wlADD', 'Producto añadido a lista de deseos.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Lista de deseos');
define('wlTAB_CONTACTFORM', 'Enviar lista a concesionario');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Todas');
define('mpTITLE', 'Selecciona tu distribuidor');
define('mpCLOSE_btn', 'Cerrar');
define('mpPROV', 'Seleccione su provincia');
define('mpSEARCH_btn', 'Buscar concesionarios');
define('mpCHOSE_btn', 'Seleccionar y cerrar');
define('cformTITLE', 'Enviar su lista de deseos al concesionario:');
define('cformSELDEALER_btn', 'Seleccionar concesionario');
define('cformSELDEALER_error', 'Debe seleccionar un concesionario.');
define('cformDEALERNAME', 'Nombre del concesionario *');
define('cformDEALERNAME_hint', 'Por favor seleccione un concesionario en el mapa*');
define('cformDEALERNAME_error','Nombre del concesionario obligatorio.');
define('cformDEALERADRESS', 'Dirección del concesionario *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Dirección del concesionario obligatoria.');
define('cformDEALERMAIL', 'Dirección de correo del concesionario *');
define('cformDEALERMAIL_hint', 'Por favor seleccione un concesionario en el mapa *');//update:  old."o escriba el mail del mismo"
define('cformDEALERMAIL_error', 'Mail del concesionario obligatorio.');
define('cformNAME', 'Nombre *');
define('cformNAME_hint', 'Por favor, introduzca su nombre *');
define('cformNAME_error', 'Nombre obligatorio.');
define('cformSURNAME', 'Apellidos *');
define('cformSURNAME_hint', 'Por favor, introduzca sus apellidos *');
define('cformSURNAME_error', 'Apellidos obligatorios.');
define('cformMAIL', 'Email *');
define('cformMAIL_hint', 'Por favor, introduzca su dirección de correo *');
define('cformMAIL_error', 'Dirección de correo válida obligatoria.');
define('cformPHONE', 'Teléfono');
define('cformPHONE_hint', 'Introduzca su teléfono de contacto (opcional)');
define('cformMSG', 'Mensaje');
define('cformMSG_hint', 'Si lo desea introduzca un mensaje de texto (opcional)');
define('cformSEND_btn', 'Enviar mensaje');
define('cformNOTICE','Estos campos son obligatorios.');
define('cform_POSTerror','Error en el envío del formulario. Reintente.');
define('cform_CAPTCHAerror','reCaptcha inválido o no introducido.');
define('cform_SUCCESS','Se ha enviado correctamente la Lista de deseos.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-off');
define('vidmodal_LINKTOMODAL', 'Ver el vídeo del Making-off');
//--
define('furtherinfo', 'Para más información contacte con su concesionario SEAT');
define('selcar', 'Selecciona tu coche');
define('prevcars', 'MODELOS ANTERIORES');
define('newcars', 'MODELOS ACTUALES');
define('dealer', '¿Donde comprar?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Seleccione su producto');
define('clistREF', 'Referencia');
define('clistPRO', 'Producto');
define('clistSIZ', 'Talla');
define('clistCOL', 'Color');
define('clistPRC', 'PVP');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'La política de Cookies de SEAT Accesorios');
define('modal_txt11', 'SEAT Accesorios usa cookies para mejorar tu experiencia navegando por nuestros entornos web. Puedes consultar nuestra ');
define('modal_txt12_href', 'política de Cookies aquí');
define('modal_txt13', '.');
define('accept_btn', 'Aceptar');
define('policy1', '¿Qué son las cookies?');
define('policy2', 'Las cookies son pequeños archivos de texto que se instalan en el navegador del ordenador del Usuario para registrar su actividad, enviando una identificación anónima que se almacena en el mismo, con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los Usuarios que se hayan registrado previamente y el acceso a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se pueden utilizar también para medir la audiencia, parámetros del tráfico y navegación, tiempo de sesión, y/o controlar el progreso y el número de entradas.');
define('policy3', 'SEAT Accesorios procurará en todo momento establecer mecanismos adecuados para obtener el consentimiento del Usuario para la instalación de cookies que lo requieran. No obstante lo anterior, deberá tenerse en cuenta que, de conformidad con la Ley, se entenderá que (1) el Usuario ha dado su consentimiento si modifica la configuración del navegador deshabilitando las restricciones que impiden la entrada de cookies y (2) que el referido consentimiento no será preciso para la instalación de aquellas cookies que sean estrictamente necesarias para la prestación de un servicio expresamente solicitado por el Usuario (mediante registro previo).');
define('policy4', 'No obstante, la deshabilitación de las mismas podría modificar el funcionamiento del sitio Web. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información.');
define('policy5', '(1) Si utiliza Microsoft Internet Explorer, en la opción de menú Herramientas, seleccionando Opciones de Internet y accediendo a Privacidad.');
define('policy6', 'Si utiliza Firefox, para Mac en la opción de menú Preferencias, seleccionando Privacidad, accediendo al apartado Mostrar Cookies, y para Windows en la opción de menú Herramientas, seleccionando Opciones, accediendo a Privacidad y luego a Usar una configuración personalizada para el historial.');
define('policy7', 'Si utiliza Safari, en la opción de menú Preferencias, seleccionando Privacidad.');
define('policy8', 'Si utiliza Google Chrome, en la opción de menú Herramientas, seleccionando Opciones (Preferencias en Mac), accediendo a Avanzadas y luego en la opción Configuración Contenido de la sección Privacidad, y finalmente marcando Cookies en el diálogo Configuración de contenido.');
define('lDIS_1_1', 'Esta lista de productos seleccionados no está vinculada ni supone compromiso de compra de los mismos, siendo únicamente de carácter informativo con la posibilidad de cerrar la operación si es de su interés  y una vez usted la haya acordado con el concesionario SEAT seleccionado.');
define('lDIS_1_2', 'En el momento que usted realiza la selección del concesionario SEAT donde estaría interesado en contactar para la información, adquisición y montaje de los accesorios incluídos en la lista de deseos,  y previa solicitud de sus datos personales, se emite automáticamente una comunicación al concesionario seleccionado para que contacte con usted y le informe sobre todos los detalles que necesite.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Su wishlist esta vacia.');
//--toClient_mail()--
define('mcSubjectForClient', 'Confirmación de petición de Wishlist');//***
//--toClient_mailContent()--//
define('mcH2', 'Estimado(a) Cliente ');//*
define('mcP1', 'Desde Seat le agradecemos su interés en la gama de Accesorios Originales que nuestra Marca pone a su disposición para la personalización de su vehículo.');//*
define('mcP2', 'En base a su interés , en las próximas horas el responsable del departamento de Accesorios del concesionario ');//*
define('mcP3_1', ' que usted ha seleccionado le contactará para ultimar los detalles de la operación de venta y montaje de los Accesorios seleccionados.');//*
define('mcP3_2', 'A continuación le indicamos los datos de contacto del concesionario para cualquier cuestión adicional que quisiera consultar, así como un resumen los Accesorios seleccionados.');//*
define('mcP4_1', 'Teléfono ');
define('mcP4_2', 'Accesorios Seleccionados') ;
define('mcP5', 'Saludos cordiales.');
define('mcP6', 'Accesorios Originales SEAT.');
define('mcP7', 'DISCLAIMER: Este mensaje contiene información propietaria de la cual parte o toda puede contener información confidencial o protegida legalmente. Esta exclusivamente destinado al usuario de destino. Si, por un error de envio o transmisión, ha recibido este mensaje y usted no es el destinatario del mismo, por favor, notifique de este hecho al remitente. Si no es el destinatario final de este mensaje no debe usar, informar, distribuir, imprimir, copiar o difundir este mensaje bajo ningún medio.');
?>	