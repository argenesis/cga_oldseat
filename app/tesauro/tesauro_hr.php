<?php
$_SESSION['lang'] = 'hrvatski'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Proizvod se ne distribuira na ovom tržištu.');
define('aut_role', 'Nemate dostatna prava za pristup ovoj usluzi.');
define('model_mrkt', 'Odabrani model ne prodaje se na ovom tržištu.');
define('browser', 'Preglednik ne podržava ovu opciju.');
define('err_wlITEM', 'Ovo polje ne može biti prazno ni nula te mora biti cijeli pozitivan broj.'); //Wishlist - error on quantity
define('err_videoplayer', 'Vaš preglednik ne podržava reprodukciju ovog formata datoteke.');

//************************ TRANSLATION ****************************//
define('Head1', 'Opći katalog dodataka SEAT');
define('Head2', 'Opći katalog');
define('Head3', 'originalnih dodataka');
define('Footer1', 'ORIGINALNI DODACI');
define('Footer2', ' - SEAT primjenjuje politiku neprekidnog razvoja svojih proizvoda i pridržava pravo izmjena u specifikacijama.');
define('Footer3', 'TEHNOLOGIJA ZA UŽITAK');
define('Home1', 'Dodaci za tebe');
define('Home1alt', 'idi na početnu stranicu SEAT Collection');
define('Home2', 'Dodaci za tvoj automobil');
define('Home2alt', 'idi na početnu stranicu SEAT Vehicles');
define('Col1alt', 'idi na SEAT Collection Ateca');
define('Col2alt', 'idi na SEAT Collection Essentials');
define('Col3alt', 'idi na SEAT Collection Motorsports');
define('Col4alt', 'idi na SEAT Collection Mediterranean');
define('cat1col1', 'Nema proizvoda za ovo tržište.');
//--product view(coll&acc)
define('tabTXT', 'Proizvod');
define('tabDESP', 'Dijelovi');
define('lREF', 'Referenca');
define('lNEW', 'Novo!');
define('lSIZ', 'Veličina');
define('lCOL', 'Boja');
define('lPRD', 'Proizvod');
define('lPRC', 'MPC');
define('lUT', 'Vrijeme ugradnje (U.T.)');
define('lDIS', '* U navedene cijene uključen je PDV, ali nisu uključeni troškovi ugradnje. Za više informacija obratite se svojem SEAT koncesionaru.');
define('lPRNT', 'Ispis');
define('lCROSS', 'Moglo bi vas zanimati'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Odaberi proizvod');
define('listREF', 'Referenca');
define('listPRO', 'Proizvod');
define('listUT', 'Vrijeme ugradnje (U.T.)');
define('listPRC', 'MPC');
//--searchresults & searchbox
define('search1', 'Proizvod s referencom');
define('search2', 'nije pronađen');
define('search3', 'nijedan model');
define('search4', 'pronađen');
define('search5', 'za sljedeće nove modele');
define('search6', 'idi na novi model');
define('search7', 'za sljedeće prethodne modele');
define('search8', 'idi na prethodni model');
define('search9', 'u SEAT Collection');
define('buscador','Naziv ili referentni broj');
//--wishlist(xxx_WL)
define('add_WL', 'Dodati na listu želja');
define('view_WL', 'Vidi listu želja');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Moja lista želja');
define('wlNAME', 'Ime');
define('wlPRC', 'Cijena');
define('wlTOT', 'Ukupno');
define('wlUPDATE', 'Ažuriraj listu');
define('wlEMPTY', 'Isprazni listu');
define('wlEMPTIED', 'Lista želja ispraznila');
define('wlPRNT', 'Ispiši listu');
define('wlUNIT', 'Jedinica');
define('wlADD', 'Proizvod dodan na listu želja.');
define('wlDIS', '* Navedene cijene ne uključuju ugradnju.');
define('wlTAB_WISHLIST', 'Lista želja');
define('wlTAB_CONTACTFORM', 'Pošalji listu koncesionaru');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Sve');
define('mpTITLE', 'Odaberi svojeg distributera');
define('mpCLOSE_btn', 'Zatvori');
define('mpPROV', 'Odaberite svoju regiju');
define('mpSEARCH_btn', 'Pronađi koncesionare');
define('mpCHOSE_btn', 'Odaberi i zatvori');
define('cformTITLE', 'Pošaljite svoju listu želja koncesionaru:');
define('cformSELDEALER_btn', 'Odaberi koncesionara');
define('cformSELDEALER_error', 'Morate odabrati koncesionara.');
define('cformDEALERNAME', 'Naziv koncesionara *');
define('cformDEALERNAME_hint', 'Molimo odaberite koncesionara na zemljovidu*');
define('cformDEALERNAME_error','Obvezan unos naziva koncesionara.');
define('cformDEALERADRESS', 'Adresa koncesionara*');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Adresa koncesionara je obvezna*');
define('cformDEALERMAIL', 'Adresa pošte koncesionara*');
define('cformDEALERMAIL_hint', 'Molimo odaberite koncesionara na karti ili upišite njegovu e-poštu*');
define('cformDEALERMAIL_error', 'Adresa e-pošte koncesionara je obvezna.');
define('cformNAME', 'Ime *');
define('cformNAME_hint', 'Molimo unesite svoje ime *');
define('cformNAME_error', 'Ime je obvezno.');
define('cformSURNAME', 'Prezime *');
define('cformSURNAME_hint', 'Molimo unesite svoje prezime *');
define('cformSURNAME_error', 'Prezime je obvezno.');
define('cformMAIL', 'E-pošta *');
define('cformMAIL_hint', 'Molimo unesite svoju poštansku adresu * ');
define('cformMAIL_error', 'Obvezno se navodi valjana poštanska adresa.');
define('cformPHONE', 'Telefon');
define('cformPHONE_hint', 'Unesite svoj telefon za kontakt (neobvezno)');
define('cformMSG', 'Poruka');
define('cformMSG_hint', 'Ako želite unesite tekstualnu poruku (neobvezno)');
define('cformSEND_btn', 'Pošalji poruku');
define('cformNOTICE','Obvezno popuniti ova polja.');
define('cform_POSTerror','Pogreška pri slanju formulara. Pokušajte ponovno.');
define('cform_CAPTCHAerror','Unijeli ste pogrešan reCaptcha unos ili ga niste unijeli.');
define('cform_SUCCESS','Lista želja uspješno je poslana.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-of');
define('vidmodal_LINKTOMODAL', 'Pogledaj video Making-ofa');
//--
define('furtherinfo', 'Za više informacije obratite se svojem SEAT koncesionaru');
define('selcar', 'Odaberi automobil');
define('prevcars', 'PRETHODNI MODELI');
define('newcars', 'NOVI MODELI');
define('dealer', 'Gdje kupiti?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Odaberite proizvod');
define('clistREF', 'Referenca');
define('clistPRO', 'Proizvod');
define('clistSIZ', 'Veličina');
define('clistCOL', 'Boja');
define('clistPRC', 'MPC');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Politika o kolačićima na stranici SEAT dodataka');
define('modal_txt11', 'SEAT Dodaci koriste se kolačićima radi poboljšanja iskustva pregledavanja naše internetske stranice. Možete pročitati našu ');
define('modal_txt12_href', 'politiku o Kolačićima ovdje');
define('modal_txt13', '.');
define('accept_btn', 'Prihvati');
define('policy1', 'Što su kolačići?');
define('policy2', 'Kolačići su malene tekstualne datoteke koje se ugrađuju u preglednik računala korisnika s pomoću kojih se registrira njegova aktivnosti slanjem anonimne identifikacije koja se pohranjuje na računalu u svrhu pojednostavljenja pretraživanja. Time se, na primjer, Korisnicima koji su se prethodno registrirali omogućuje pristup područjima, uslugama, promidžbenim akcijama ili nagradnim igrama rezerviranima isključivo za njih, a da se pritom ne moraju registrirati pri svakom posjetu. Mogu se upotrebljavati i za mjerenje gledanosti, parametara prometa i pregledavanja, vremena trajanja sesije i/ili kontrole napretka te broja ulazaka na stranicu.');
define('policy3', 'Dodaci SEAT trudit će se u svakom trenutku raspolagati odgovarajućim mehanizmima za dobivanje pristanka Korisnika za ugradnju kolačića koji to zahtijevaju. Neovisno o prethodno navedenom, treba imati na umu da se, u skladu sa Zakonom, smatra da (1) je Korisnik dao svoj pristanak ako je izmijenio postavke preglednika tako da je deaktivirao ograničenja kojima se zabranjuje ulazak kolačića i (2) da spomenuti pristanak nije potreban za ugradnju kolačića koji nisu nužno potrebni za pružanje usluge koju je izričito zatražio Korisnik (temeljem prethodne registracije).');
define('policy4', 'Međutim, njihovom deaktivacijom mogao bi se promijeniti rad web mjesta. Za više informacija pročitajte upute i priručnike preglednika kojim se služite.');
define('policy5', '(1) Ako upotrebljavate Microsoft Internet Explorer, u opciji izbornika Alati odaberite Internetske opcije i pristupite Privatnosti.');
define('policy6', 'Ako upotrebljavate Firefox, za Mac otiđite u opciju izbornika Postavke, odaberite Privatnost i pristupite odjeljku Prikaži kolačiće, a za Windowse, otiđite u opciju izbornika Alati, odaberite Opcije, pristupite opciji Privatnost, a zatim Koristi prilagođene postavke za povijest.');
define('policy7', 'Ako upotrebljavate Safari, u opciji izbornika Postavke odaberite Privatnost.');
define('policy8', 'Ako upotrebljavate Google Chrome, u opciji izbornika Alati odaberite Opcije (Postavke na Macu) i pristupite opciji Napredne, a zatim opciji Postavi sadržaj u odjeljku Privatnosti te na kraju označite opciju Kolačići u dijaloškom okviru Postavke sadržaja.');
define('lDIS_1_1', 'Ovaj popis odabranih proizvoda nije povezana s obvezom njihove kupnje niti podrazumijeva njihovu kupnju, već je isključivo informativnog karaktera te uključuje mogućnost kupnje ako vi to želite i ako ste to dogovorili sa svojim odabranim SEAT koncesionarom.');
define('lDIS_1_2', 'Kad odaberete SEAT koncesionara kojeg želite kontaktirati kako biste dobili više informacija, kupili ili montirali dodatnu opremu uključenu u listu želja te kad dostavite svoje osobne podatke, odabranom koncesionaru automatski se šalje obavijest da stupi u kontakt s vama i uputi vas u sve pojedinosti koje su vam potrebne.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Vaša je lista želja prazna.');
//--toClient_mail()--
define('mcSubjectForClient', 'Potvrda zahtjeva za Listu želja');//***
//--toClient_mailContent()--//
define('mcH2', 'Poštovani, ');//*
define('mcP1', 'u ime društva SEAT želimo vam zahvaliti na tome što ste iskazali interes za dodatnu opremu koju vam naša marka nudi za personalizaciju vašeg vozila.');//*
define('mcP2', 'Na temelju vašeg interesa, uskoro će vas kontaktirati zaposlenik odjela za dodatnu opremu koncesionara ');//*
define('mcP3_1', ' kojeg ste odabrali da vas kontaktira kako biste dogovorili pojedinosti postupka kupnje i ugradnje odabrane dodatne opreme.');//*
define('mcP3_2', 'U nastavku navodimo podatke za kontakt koncesionara u slučaju bilo kakvih dodatnih pitanja, kao i sažetak odabrane dodatne opreme.');//*
define('mcP4_1', 'Telefon ');
define('mcP4_2', 'Odabrana dodatna oprema') ;
define('mcP5', 'Srdačan pozdrav');
define('mcP6', 'Originalna dodatna oprema SEAT');
define('mcP7', 'IZJAVA O ODRICANJU OD ODGOVORNOSTI: Ova poruka sadržava povjerljive informacije koje u cijelosti ili djelomično mogu biti povjerljive ili zaštićene zakonom. Namijenjena je isključivo primatelju navedenom u adresi. Ako ste greškom primili ovu poruku i vi niste primatelj kojem je namijenjena, molimo da o tome obavijestite pošiljatelja. Ako niste primatelj kojem je ova poruka namijenjena, ni na koji je način ne smijete upotrijebiti, objaviti, distribuirati, ispisivati, kopirati ili širiti.');
?>	