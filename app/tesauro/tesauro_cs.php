<?php
$_SESSION['lang'] = 'čeština'; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', 'Produkt není k dispozici na tomto trhu.');
define('aut_role', 'Pro přístup k této službě nemáte dostatečná oprávnění.');
define('model_mrkt', 'Zvolený model není k dispozici na tomto trhu.');
define('browser', 'Prohlížeč nepodporuje tuto volbu');
define('err_wlITEM', 'Toto pole nesmí zůstat prázdné ani mít nulovou hodnotu, musí obsahovat celé kladné číslo.'); //Wishlist - error on quantity
define('err_videoplayer', 'Váš prohlížeč nepodporuje zobrazení tohoto formátu souboru.');

//************************ TRANSLATION ****************************//
define('Head1', 'Obecný katalog příslušenství SEAT');
define('Head2', 'Obecný katalog');
define('Head3', 'originálního příslušenství');
define('Footer1', 'ORIGINÁLNÍ PŘÍSLUŠENSTVÍ');
define('Footer2', ' - SEAT neustále vyvíjí své produkty a vyhrazuje si právo provádět změny ve specifikacích.');
define('Footer3', 'UŽÍVEJTE SI TECHNOLOGII');
define('Home1', 'Doplňky pro vás');
define('Home1alt', 'na SEAT Collection home');
define('Home2', 'Doplňky pro váš vůz');
define('Home2alt', 'na SEAT Vehicles home');
define('Col1alt', 'na SEAT Collection Ateca');
define('Col2alt', 'na SEAT Collection Essentials');
define('Col3alt', 'na SEAT Collection Motorsports');
define('Col4alt', 'na SEAT Collection Mediterranean');
define('cat1col1', 'Pro tento trh nejsou dostupné žádné produkty.');
//--product view(coll&acc)
define('tabTXT', 'Produkt');
define('tabDESP', 'Rozložení na díly');
define('lREF', 'Označení');
define('lNEW', 'Novinka!');
define('lSIZ', 'Velikost');
define('lCOL', 'Barva');
define('lPRD', 'Produkt');
define('lPRC', 'Prodejní cena');
define('lUT', 'Čas potřebný k montáži (časová jednotka)');
define('lDIS', '* Uvedené ceny zahrnují DPH, nezahrnují náklady na montáž. Potřebujete-li další informace, obraťte se na svého prodejce SEAT.');
define('lPRNT', 'Vytisknout');
define('lCROSS', 'Také by vás mohlo zajímat'); //cross-selling
//--ctg_list: column labels
define('listTITLE', 'Vyberte si produkt');
define('listREF', 'Označení');
define('listPRO', 'Produkt');
define('listUT', 'Čas potřebný k montáži (časová jednotka)');
define('listPRC', 'Prodejní cena');
//--searchresults & searchbox
define('search1', 'Produkt s označením');
define('search2', 'nebyl nalezen');
define('search3', 'pro žádný model');
define('search4', 'byl nalezen');
define('search5', 'pro následující aktuální modely');
define('search6', 'na aktuální model');
define('search7', 'pro následující předchozí modely');
define('search8', 'na předchozí model');
define('search9', 'v kolekci SEAT');
define('buscador','Název nebo označení');
//--wishlist(xxx_WL)
define('add_WL', 'Přidat do seznamu přání');
define('view_WL', 'Zobrazit seznam přání');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', 'Můj seznam přání');
define('wlNAME', 'Jméno');
define('wlPRC', 'Cena');
define('wlTOT', 'Celkem');
define('wlUPDATE', 'Aktualizovat seznam');
define('wlEMPTY', 'Vymazat seznam');
define('wlEMPTIED', 'Seznam přání vymazán');
define('wlPRNT', 'Vytisknout seznam');
define('wlUNIT', 'Počet');
define('wlADD', 'Produkt přidán do Seznamu přání.');
define('wlDIS', lDIS);//***'lDIS_1_1'|'lDIS_1_2'
define('wlTAB_WISHLIST', 'Seznam přání');
define('wlTAB_CONTACTFORM', 'Odeslat seznam prodejci');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', 'Vše');
define('mpTITLE', 'Vyberte svého prodejce');
define('mpCLOSE_btn', 'Zavřít');
define('mpPROV', 'Vyberte svůj kraj');
define('mpSEARCH_btn', 'Hledat prodejce');
define('mpCHOSE_btn', 'Vybrat a zavřít');
define('cformTITLE', 'Odeslat Seznam přání prodejci:');
define('cformSELDEALER_btn', 'Vybrat prodejce');
define('cformSELDEALER_error', 'Musíte vybrat prodejce.');
define('cformDEALERNAME', 'Jméno prodejce *');
define('cformDEALERNAME_hint', 'Vyberte prodejce na mapě*');
define('cformDEALERNAME_error','Jméno prodejce je povinné.');
define('cformDEALERADRESS', 'Adresa prodejce *');
define('cformDEALERADRESS_hint', cformDEALERNAME_hint);
define('cformDEALERADRESS_error', 'Adresa prodejce je povinná.');
define('cformDEALERMAIL', 'E-mail prodejce *');
define('cformDEALERMAIL_hint', 'Vyberte prodejce na mapě nebo napište jeho e-mail*');
define('cformDEALERMAIL_error', 'E-mail prodejce je povinný.');
define('cformNAME', 'Jméno *');
define('cformNAME_hint', 'Zadejte jméno *');
define('cformNAME_error', 'Jméno je povinné.');
define('cformSURNAME', 'Příjmení *');
define('cformSURNAME_hint', 'Zadejte příjmení *');
define('cformSURNAME_error', 'Příjmení je povinné.');
define('cformMAIL', 'E-mail *');
define('cformMAIL_hint', 'Zadejte e-mailovou adresu *');
define('cformMAIL_error', 'Zadání platného e-mailu je povinné.');
define('cformPHONE', 'Telefon');
define('cformPHONE_hint', 'Zadejte kontaktní telefonní číslo (volitelné)');
define('cformMSG', 'Zpráva');
define('cformMSG_hint', 'Sem můžete zadat textovou zprávu (volitelné)');
define('cformSEND_btn', 'Odeslat zprávu');
define('cformNOTICE','Tato pole jsou povinná.');
define('cform_POSTerror','Chyba při odesílání formuláře. Zkuste to znovu.');
define('cform_CAPTCHAerror','reCaptcha neplatné nebo nebylo zadáno.');
define('cform_SUCCESS','Seznam přání byl úspěšně odeslán.');
//-- Video_modal
define('vidmodal_TITLE', 'Mediterranean photo shooting Making-off');
define('vidmodal_LINKTOMODAL', 'Přehrát video Making-off');
//--
define('furtherinfo', 'Potřebujete-li další informace, obraťte se na prodejce SEAT');
define('selcar', 'Vyberte své auto');
define('prevcars', 'PŘEDCHOZÍ MODELY');
define('newcars', 'AKTUÁLNÍ MODELY');
define('dealer', 'Kde nakupovat?');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', 'Vyberte si produkt');
define('clistREF', 'Označení');
define('clistPRO', 'Produkt');
define('clistSIZ', 'Velikost');
define('clistCOL', 'Barva');
define('clistPRC', 'Prodejní cena');
//************************ COOKIES - POLICY ************************************//
define('modal_title', 'Zásady používání Cookies SEAT příslušenství');
define('modal_txt11', 'SEAT příslušenství používá cookies za účelem zlepšit zážitek při procházení webového prostředí. Naše Zásady používání cookies ');
define('modal_txt12_href', 'si můžete zobrazit zde');
define('modal_txt13', '.');
define('accept_btn', 'Souhlasím');
define('policy1', 'Co jsou to cookies?');
define('policy2', 'Cookies jsou malé textové soubory, které jsou nainstalovány v prohlížeči počítače uživatele za účelem registrovat jeho činnost. Zasílají anonymní identifikaci, která se ukládá do počítače, tak například umožňují přístup uživatelům, kteří se již zaregistrovali, a přístup k oblastem služeb, promo akcím a konkurzům, které jsou vyhrazené exkluzivně pro ně, aniž by se museli při každé návštěvě znovu registrovat. Také je lze použít k měření návštěvnosti, parametrů provozu a procházení, času stráveného v přihlášení a/nebo kontrole průběhu a počtu přístupů.');
define('policy3', 'Společnost SEAT Accesorios se vynasnaží vždy vytvářet vhodné mechanizmy k získání souhlasu uživatele pro instalaci cookies, které takový souhlas budou vyžadovat. Bez ohledu na výše uvedené je třeba poznamenat, že v souladu se zákonem bude považováno (1) za vyjádření souhlasu, pokud uživatel změní nastavení prohlížeče a deaktivuje omezení, která zabraňují vstupu souborů cookie a (2) za nepotřebné poskytnutí souhlasu pro instalace takových souborů cookie, které jsou přísně vyžadovány k poskytování služby výslovně vyžadované uživatelem (prostřednictvím předchozího zaregistrování).');
define('policy4', 'Nicméně deaktivace použití souborů cookie může změnit fungování webové stránky. Další informace k tomuto tématu naleznete v pokynech a návodech svého prohlížeče.');
define('policy5', '(1) Pokud používáte Microsoft Internet Explorer, v nabídce Nástroje zvolte Možnosti internetu a vstupte do oblasti Soukromí.');
define('policy6', 'Pokud používáte Firefox: v případě Macu v nabídce Preference zvolte Soukromí a vstupte do sekce Zobrazit Cookies, v případě Windows v nabídce Nástroje zvolte Možnosti a vstupte do sekce Soukromí a poté do Použít personalizované nastavení historie.');
define('policy7', 'Pokud používáte Safari, v nabídce Preference zvolte Soukromí.');
define('policy8', 'Pokud používáte Google Chrome, v nabídce Nástroje zvolte Možnosti (Preference u Macu) a vstupte do volby Pokročilé, poté do volby Nastavení obsahu v sekci Soukromí a nakonec označte Cookies v dialogu Nastavení obsahu.');
define('lDIS_1_1', 'Tento seznam vybraných produktů není závazný ani nepředpokládá závazek k jejich zakoupení. Má pouze informativní charakter a nabízí možnost uzavřít nákupní operaci, pokud o uzavření máte zájem, a po dohodě s vybraným prodejcem SEAT.');
define('lDIS_1_2', 'Ve chvíli, kdy učiníte výběr prodejce SEAT, kterého byste měli zájem kontaktovat za účelem poskytnutí informací, zakoupení a montáže příslušenství, které máte v seznamu přání, a po vyžádání vašich osobních údajů, bude automaticky odesláno sdělení vybranému prodejci, aby vás kontaktoval a informoval o všech podrobnostech, které byste mohli potřebovat.');

//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', 'Váš seznam přání je prázdný.');
//--toClient_mail()--
define('mcSubjectForClient', 'Potvrzení žádosti ze Seznamu přání');//***
//--toClient_mailContent()--//
define('mcH2', 'Vážná zákaznice / Vážený zákazníku, ');//*
define('mcP1', 've společnosti Seat si vážíme Vašeho zájmu o nabídku originálního příslušenství, které Vám naše značka nabízí pro personalizaci Vašeho vozu.');//*
define('mcP2', 'Na základě zájmu o příslušenství, který jste vyjádřil/a, Vás v příštích hodinách kontaktuje vedoucí oddělení příslušenství prodejce, ');//*
define('mcP3_1', ' kterého jste zvolil/a, aby s Vámi dohodl podrobné údaje ohledně prodeje a montáže vybraného příslušenství.');//*
define('mcP3_2', 'Níže uvádíme kontaktní údaje prodejce, na kterého se můžete obrátit k vyřešení jakýchkoli dalších otázek, a také přehled vybraného příslušenství.');//*
define('mcP4_1', 'Telefon ');
define('mcP4_2', 'Vybrané příslušenství') ;
define('mcP5', 'Se srdečným pozdravem');
define('mcP6', 'Originální příslušenství SEAT.');
define('mcP7', 'PRÁVNÍ UPOZORNĚNÍ: Tato zpráva obsahuje informace o vlastnických právech, z nichž část nebo všechny mohou obsahovat důvěrné nebo legálně chráněné informace. Je určena výhradně konkrétnímu uživateli. Pokud jste kvůli chybě v odesílání nebo přenosu tuto zprávu obdrželi a nejste jejím adresátem, sdělte, prosím, tuto skutečnost odesílateli. Pokud nejste konečným příjemcem této zprávy, nesmíte tuto zprávu použít, sdělovat, distribuovat, tisknout, kopírovat nebo šířit žádnými prostředky.');
?>