<?php
//--html--//
define('mlREF', 'Codice');
define('mlPRD', 'Prodotto');
define('mlPRC', 'Prezzo');
define('mlUT', 'Tempo di montaggio (U.T.)');
define('mlDIS', '* I prezzi sono IVA inclusa, non comprendono il montaggio. Per maggiori informazioni rivolgersi al proprio rivenditore SEAT di fiducia.');
//--wishlist WINDOW(wlXXX)--//
define('mwlTITLE', 'La mia Lista dei desideri');
define('mwlNAME', 'Nome');
define('mwlPRC', 'Prezzo');
define('mwlTOT', 'Totale');
define('mwlUNIT', 'Unità');
define('mwlDIS', '* I prezzi non comprendono il montaggio.');
define('mwlDIS_1_1', 'Questa lista di prodotti non è vincolante e non implica nessun obbligo di acquisto, è solamente di carattere informativo. Se si desidera, è possibile chiudere l’operazione dopo averla concordata con il distributore SEAT selezionato.');
define('mwlDIS_1_2', 'Al momento di selezionare il distributore SEAT dal quale si desidera ricevere informazioni sull’acquisto e montaggio degli accessori aggiunti alla Lista dei desideri e, dopo aver inserito i propri dati personali, viene inviato automaticamente un messaggio al rivenditore selezionato il quale vi contatterà per informarvi di tutti i dettagli necessari.');

//************************ MAIL-TO-DEALER ****************************//
//--verifyWlContent()--//
define('mdEmptyWL', 'È stata inviata una Lista dei desideri vuota.');
//--toConcessionaire_mail()--
define('mSubjectForDealer', 'Nuova richiesta da Accessori Originali SEAT');//***
//--toConcessionaire_mailContent()--//
define('mH2', 'Per il responsabile del dipartimento Ricambi e Accessori SEAT del rivenditore ');
define('mP1', 'Tramite il catalogo Accessori Originali SEAT Online, il cliente ');
define('mP2', ' ha mostrato interesse nell’acquisto e il montaggio degli Accessori che si indicano nella Lista dei desideri fornita qui di seguito.');
define('mP3_1', 'Il cliente è in attesa di una vostra chiamata per ultimare i dettagli della vendita, pertanto è importante contattarlo il prima possibile per garantire il buon esito di quest’operazione.');
define('mP3_2', 'A continuazione sono forniti i dati di contatto del cliente:');
define('mP4_1', 'Ci auguriamo che l’operazione sia confermata e che questa nuova funzione di cui dispone il catalogo Accessori Online permetta di realizzare un maggior numero di operazioni simili, le quali, siamo certi, apporteranno importanti beneficio all’area di Accessori.');
define('mP4_2', 'Accessori Selezionati') ;
define('mP5', 'Cordiali saluti');
define('mP6', 'Accessori Originali SEAT.');
define('mP7', 'DISCLAIMER: Questo messaggio contiene informazione riservate che, in toto o in parte, possono essere di carattere confidenziale e protette legalmente. Questo messaggio è destinato esclusivamente al destinatario. Qualora fosse stato ricevuto per errore si prega di informare tempestivamente il mittente. Sono strettamente proibite la copia, la comunicazione, la trasmissione, la diffusione o la riproduzione in qualsiasi modo eseguite, dei contenuti di questo messaggio.');

?>