<?php //
$_SESSION['lang'] = ''; //for ul_lang_selector
//******************* ON ERROR MESSAGE TRANSLATION *********************//
define('prd_mrkt', '');
define('aut_role', '');
define('model_mrkt', '');
define('browser', '');
define('err_wlITEM', '');
define('err_videoplayer', '');

//************************ TRANSLATION ****************************//
define('Head1', '');
define('Head2', '');
define('Head3', '');
define('Footer1', '');
define('Footer2', '');
define('Footer3', '');
define('Home1', '');
define('Home1alt', '');
define('Home2', '');
define('Home2alt', '');
define('Col1alt', '');
define('Col2alt', '');
define('Col3alt', '');
define('Col4alt', '');
define('cat1col1', '');
//--product view(coll&acc)
define('tabTXT', '');
define('tabDESP', '');
define('lREF', '');
define('lNEW', '');
define('lSIZ', '');
define('lCOL', '');
define('lPRD', '');
define('lPRC', '');
define('lUT', '');
define('lDIS', '');
define('lPRNT', '');
define('lCROSS', ''); //cross-selling
//--ctg_list: column labels
define('listTITLE', '');
define('listREF', '');
define('listPRO', '');
define('listUT', '');
define('listPRC', '');
//--searchresults & searchbox
define('search1', '');
define('search2', '');
define('search3', '');
define('search4', '');
define('search5', '');
define('search6', '');
define('search7', '');
define('search8', '');
define('search9', '');
define('buscador', '');
//--wishlist(xxx_WL)
define('add_WL', '');
define('view_WL', '');
//--wishlist WINDOW(wlXXX)
define('wlTITLE', '');
define('wlNAME', '');
define('wlPRC', '');
define('wlTOT', '');
define('wlUPDATE', '');
define('wlEMPTY', '');
define('wlEMPTIED', '');
define('wlPRNT', '');
define('wlUNIT', '');
define('wlADD', '');
define('wlDIS', lDIS);
define('lDIS_1_1', '');//[2017-11-14]
define('lDIS_1_2', '');//[2017-11-14]
define('wlTAB_WISHLIST', '');
define('wlTAB_CONTACTFORM', '');
//--mapprovinces(mp_): contactform(cform_)
define('mpALLPROVINCES', '');
define('mpTITLE', '');
define('mpCLOSE_btn', '');
define('mpPROV', '');
define('mpSEARCH_btn', '');
define('mpCHOSE_btn', '');
define('cformTITLE', '');
define('cformSELDEALER_btn', '');
define('cformSELDEALER_error', '');
define('cformDEALERNAME', '');
define('cformDEALERNAME_hint', '');
define('cformDEALERNAME_error', '');
define('cformDEALERADRESS', '');
define('cformDEALERADRESS_hint', '');
define('cformDEALERADRESS_error', '');
define('cformDEALERMAIL', '');
define('cformDEALERMAIL_hint', '');
define('cformDEALERMAIL_error', '');
define('cformNAME', '');
define('cformNAME_hint', '');
define('cformNAME_error', '');
define('cformSURNAME', '');
define('cformSURNAME_hint', '');
define('cformSURNAME_error', '');
define('cformMAIL', '');
define('cformMAIL_hint', '');
define('cformMAIL_error', '');
define('cformPHONE', '');
define('cformPHONE_hint', '');
define('cformMSG', '');
define('cformMSG_hint', '');
define('cformSEND_btn', '');
define('cformNOTICE', '');
define('cform_POSTerror','');
define('cform_CAPTCHAerror','');
define('cform_SUCCESS','');
//-- Video_modal
define('vidmodal_TITLE', '');
define('vidmodal_LINKTOMODAL', '');
//--
define('furtherinfo', '');
define('selcar', '');
define('prevcars', '');
define('newcars', '');
define('dealer', '');
//************************ TRANSLATION - COLLECTION ****************************//
//--ctg list: column labels
define('clistTITLE', '');
define('clistREF', '');
define('clistPRO', '');
define('clistSIZ', '');
define('clistCOL', '');
define('clistPRC', '');
//************************ COOKIES - POLICY ************************************//
define('modal_title', '');
define('modal_txt11', '');
define('modal_txt12_href', '');
define('modal_txt13', '.');
define('accept_btn', '');
define('policy1', '');
define('policy2', '');
define('policy3', '');
define('policy4', '');
define('policy5', '');
define('policy6', '');
define('policy7', '');
define('policy8', '');
//************************ MAIL-TO-CLIENT ****************************//
//--verifyWlContent()--//
define('mEmptyWL', '');
//--toClient_mail()--
define('mcSubjectForClient', '');//[2017-11-14]
//--toClient_mailContent()--//
define('mcH2', '');//[2017-11-14]
define('mcP1', '');//[2017-11-14]
define('mcP2', '');//[2017-11-14]
define('mcP3_1', '');//[2017-11-14]
define('mcP3_2', '');//[2017-11-14]
define('mcP4_1', '');//[2017-11-14]
define('mcP4_2', '') ;//[2017-11-14]
define('mcP5', '');//[2017-11-14]
define('mcP6', '');//[2017-11-14]
define('mcP7', '');//[2017-11-14]

?>	